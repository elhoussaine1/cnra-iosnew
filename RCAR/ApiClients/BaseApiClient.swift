//
//  BaseApiClient.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 03/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import Alamofire

typealias URLRequestParams = [String: Any]

class BaseApiClient {
    
}

extension BaseApiClient {
    enum Router: URLRequestConvertible {
    
        //static let domainName  = "https://www.rcar.ma"
        static let domainName  = "https://www.rcar.ma/"
        //static let domainName  = "https://www.rcar.ma/apimobile/oauth/token"
        
        static let apiPathName = ""
        static let baseURLString = "\(domainName)/\(apiPathName)"
    
        //MARK: - Routes
        case login(data: URLRequestParams)
        case signup(data: URLRequestParams)
        case forgotPassword(data: URLRequestParams)
        case SaveDevice(identifiant: String,DeviceToken: String)
        
        //Appointment Routes
        case existAppointment(identifiant: String)
        case addAppointment(identifiant: String,data: URLRequestParams)
        case cancelAppointment(identifiant: String,data: URLRequestParams)
        case updateAppointment(data: URLRequestParams)
        
        //Complaint
        case addComplaintBeneficiary(data: URLRequestParams)
        case FollowUpComplaint(identifiant: String,profil: String)
        case Complaintdetails(identifiant: String,complaintId: String)
        case DisplayComplaintDoc(documentId: String)
        
        //FollowUpFolder
        case FollowUpFolder(identifiant: String,profil: String)
        case FolderDetails(identifiant: String,numero: String)
      
        //Affilialted
        case getAffilie(identifiant: String)
        case getLifeControl(data: URLRequestParams)
        case simulation(identifiant: String,components : URLComponents)
        case AffiliateInformations(identifiant: String,profil: String)
        case addAffiliatedInformations(profil: String,data: URLRequestParams)
 
        //NewsHelp
        case News(identifiant: String,components : URLComponents)
        
        //Beneficiary
        case getBeneficiryAffiliatedCor(profil : String , identifiant: String)
        case BeneficiaryInformations(identifiant: String,profil: String)
        case addBeneficiryInformations(profil: String,identifiant: String,data: URLRequestParams)

        //Accountsituation
        case AccountsituationRCDtos(identifiant: String)
        case AccountsituationRGDtos(identifiant: String)
        case subscriptiondetails(identifiant: String,year: String)
        
        //Appointmeny
        case HoursList(components : URLComponents)
        case CheckCodeActivation(produit: String,numClient: String,codeActivation:String,token:String)
        
        //Certification
        case addCertification(data: URLRequestParams)
        case ActivateFacePrint(data: URLRequestParams)
        case ActivatePassPortFacePrint(data: URLRequestParams)
        case getcertificationType(data: URLRequestParams)
        case getNextSeq(data: URLRequestParams)
        
        //PaymentSituation
        case getpaymentSituation(identifiant: String)
        case detailSituation(identifiant : String,moisEche : String,anneeEche : String)
        case printpaymentSituation(identifiant: String,components : URLComponents)
        
        //MARK: - Methods
        var method: HTTPMethod {
            switch self {
            case .login, .signup, .forgotPassword,
                 .addAppointment, .cancelAppointment, .updateAppointment, .addComplaintBeneficiary,.addCertification,.SaveDevice, .addAffiliatedInformations, .addBeneficiryInformations, .ActivateFacePrint, .getLifeControl, .ActivatePassPortFacePrint:
                return .post
                
            default:
                return .get
            }
        }
        
        // MARK: - Paths
        var path: String {
            switch self {
            case .login(_):
                return "apimobile/oauth/token"
            case .signup(_):                                                return "commun/inscription/signUp"
            case .forgotPassword(_):                                        return "password/reset"
            case .SaveDevice(let identifiant,let DeviceToken):
                return "commun/user/\(identifiant)/\(DeviceToken)"
            case .existAppointment(let identifiant):                            return "commun/rendez-vous/\(identifiant)"
            case .addAppointment(let identifiant,_):                            return "commun/rendez-vous/\(identifiant)"
            case .cancelAppointment(let identifiant,_):                         return "commun/rendez-vous/\(identifiant)/annuler"
            case .updateAppointment(_):                                     return "rendez-vous"
            case .getAffilie(let identifiant):
                return "rcar/situationCompte/affilie/\(identifiant)/RC"
            case .simulation(let identifiant,_):
                return "rcar/affilie/\(identifiant)/simulation-affilie"
            case .News(let identifiant,_):
                return "commun/wcm/actualites"
            case .addComplaintBeneficiary(_):                               return "commun/reclamation/beneficiaire"
             case .addAffiliatedInformations(let profil,_):
                return "rcar/\(profil)/coordonnee"
            case .addBeneficiryInformations(let profil,let identifiant,_):
                return "rcar/\(profil)/coordonnee/\(identifiant)"
            case .getBeneficiryAffiliatedCor(let profil,let identifiant):
                return "rcar/libre-service/reclamation/\(profil)/\(identifiant)"
            case .FollowUpComplaint(let identifiant,let profil):
    
                if(profil.lowercased() == "affilie"){
                return "rcar/\(profil.lowercased())/reclamation/\(identifiant)"
                }else{
                return "rcar/beneficiaire/reclamation/\(identifiant)"
                }
                
            case .Complaintdetails(let identifiant,let complaintId):
                
                return "rcar/beneficiaire/detail-reclamation/\(identifiant)/\(complaintId)"
                
            case .DisplayComplaintDoc(let documentID):
                return "rcar/declaration/document-id/\(documentID)"
               
            case .FollowUpFolder(let identifiant,let profil):
                return "commun/suiviDossier/\(identifiant)/\(profil)"
            case .AffiliateInformations(let identifiant,let profil):
                return "rcar/\(profil)/\(identifiant)/coordonnee/"
            case .BeneficiaryInformations(let identifiant,let profil):
                return "rcar/\(profil)/coordonnee/\(identifiant)"
                
            case .FolderDetails(let identifiant,let numero):
                return "commun/suiviDossier/\(identifiant)/numero/\(numero)"
        
            case .AccountsituationRCDtos(let identifiant):
                return "rcar/situationCompte/affilie/\(identifiant)/RC"
                
            case .subscriptiondetails(let identifiant,let year):
                return "rcar/situationCompte/situation-compte-rg-detail/\(identifiant)/\(year)"
                
            case .AccountsituationRGDtos(let identifiant):
                return "rcar/situationCompte/affilie/\(identifiant)/RG"
           
            case .HoursList(_):
                return "commun/rendez-vous/listHeuresByJour"
            case .addCertification(_):
                return "commun/attestations/demande-attestation"
            case .getLifeControl(_):
                return "apimobile/mobile-service/face-print/controle-vie"
            case .ActivatePassPortFacePrint(_):
                return "apimobile/mobile-service/face-print/passeport/activate"
            case .ActivateFacePrint(_):
                return "apimobile/mobile-service/face-print/activate"
            case .CheckCodeActivation(let produit,let numClient,let codeActivation,let token):
                return "apimobile/mobile-service/face-print/\(produit)/\(numClient)/\(codeActivation)"
            case .getcertificationType(_):
                return "commun/attestations/types"
            case .getNextSeq(_):
                return "commun/attestations/next"
            case .getpaymentSituation(let identifiant):
                return "rcar/beneficiaire/\(identifiant)"
            case .detailSituation(let identifiant,let moisEche,let anneeEche):
                return "rcar/beneficiaire/detail-situation/\(identifiant)/\(moisEche)/\(anneeEche)"
            case .printpaymentSituation(let identifiant,_):
                return "rcar/beneficiaire/\(identifiant)/situation/imprimer"
            }
        }
        
        // MARK: - components
        var components: URLComponents? {
            var compons: URLComponents?
            
            switch self {
            case .simulation(_, let data):             compons = data
            case .News(_, let data)      :             compons = data
            case .HoursList(let data)    :             compons = data
            case .printpaymentSituation(_, let data):  compons = data
                
            default:                                   compons = nil
            }
            
            return compons
        }
        
        // MARK: - Parameters
        var parameters: URLRequestParams? {
            var params: URLRequestParams?
            
            switch self {
                
            case .login(let data):                      params = data
            case .signup(let data):                     params = data
            case .forgotPassword(let data):             params = data
            case .addAppointment(_,let data):           params = data
            case .cancelAppointment(_,let data):        params = data
            case .updateAppointment(let data):          params = data
            case .addComplaintBeneficiary(let data):    params = data
            case .addAffiliatedInformations(_,let data): params = data
            case .addBeneficiryInformations(_,_,let data): params = data
            case .ActivateFacePrint(let data):        params = data
            case .ActivatePassPortFacePrint(let data):        params = data
            case .getLifeControl(let data):          params = data
            case .addCertification(let data):           params = data
        
            default:                                    params = nil
            }
            return params
        }
        

        func asURLRequest() throws -> URLRequest {
            
            let url = try Router.baseURLString.asURL()
            var urlRequest = URLRequest(url: url.appendingPathComponent(path))
            urlRequest.httpMethod = method.rawValue
            urlRequest.timeoutInterval = 20
            
            switch self {
              
            case .login(_):

                if let parameters = parameters {
                    urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
                }
                
                let user = "service-rcar"
                let password = "123456"
                let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
                let base64Credentials = credentialData.base64EncodedString(options: [])
                urlRequest.setValue("Basic \(base64Credentials)", forHTTPHeaderField:"Authorization")
            case .simulation( _,_) , .HoursList(_), .printpaymentSituation( _,_):
                urlRequest = URLRequest(url: (components?.url!)!)
                
                let access_token = Profil.currentProfile().access_token as String
                let tokenString = "Bearer \(access_token)"
                urlRequest.setValue(tokenString, forHTTPHeaderField: "Authorization")
                urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
                urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")

            case .News( _,_) :
                urlRequest = URLRequest(url: (components?.url!)!)
                urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
                urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")

            default:
                
                if((parameters) != nil){
                    do{
                        urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters as! Dictionary<String, String> , options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
                    }catch let error{
                        print(error.localizedDescription)
                    }
                }
                
                if(Profil.currentProfile().access_token != nil){
                    let access_token = Profil.currentProfile().access_token as String
                    let tokenString = "Bearer \(access_token)"
                    urlRequest.setValue(tokenString, forHTTPHeaderField: "Authorization")
                }
                urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
                urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
            }

            return urlRequest
        }
        
    }
}
