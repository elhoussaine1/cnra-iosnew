//
//  FolderApiClient.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 05/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON


class FolderApiClient: BaseApiClient {
    
    static func FollowUpFolder(identifiant : String,profil : String, success: @escaping ([Folder])-> Void , failure: @escaping (String) -> Void ){
        var FolderArray = [Folder]()
        let session = URLSession.shared
        let task = session.dataTask(with: Router.FollowUpFolder(identifiant : identifiant,profil : profil).urlRequest!, completionHandler: {data,response,error in
            
//             let result = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
//            print("resultfollowUPFOlder\(result)")
            
            if let httpResponse = response as? HTTPURLResponse {
                if(httpResponse.statusCode == 200 || httpResponse.statusCode == 400){
                    let json = JSON(data: data!)
                    for i in 0..<json.count {
                        let folderitem = Folder(fromJSON: json[i])
                        FolderArray.append(folderitem)
                    }
                    success(FolderArray)
                }else{
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                }
            }else{
                failure("\(String(describing: error?.localizedDescription))")
            }
        })
        task.resume()
    }
    
    static func getFolderDetails(identifiant : String,numero : String, success: @escaping (JSON)-> Void , failure: @escaping (String) -> Void ){
        var FolderArray = [Folder]()
        let session = URLSession.shared
        let task = session.dataTask(with: Router.FolderDetails(identifiant : identifiant,numero : numero).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
                let result = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
                
                print("folderDetails\(result)")
                
                if(httpResponse.statusCode == 200 || httpResponse.statusCode == 400){
                    
                    
                    let json = JSON(data: data!)
                    
                    // let folderitem = Folder(fromJSON: json)
                    
                    success(json)
                    
                    
                }else{
                    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                    
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }
            
            
        })
        task.resume()
    }
}
