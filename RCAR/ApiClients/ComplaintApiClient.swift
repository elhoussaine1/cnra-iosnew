//
//  ComplaintApiClient.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 16/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ComplaintApiClient: BaseApiClient {
    
    static func addBeneficiryInformations(_ data: URLRequestParams,profil : String,identifiant : String, completionHandler: @escaping (String) -> (), failure: @escaping (String) -> Void ){
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.addBeneficiryInformations(profil: "beneficiaire",identifiant:identifiant, data: data).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
               
                switch httpResponse.statusCode {
                case 200 :
                    
                    let result = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
                    completionHandler(result)
                    
                default:
                    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                }
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
            }
            
        })
        task.resume()
    }
    
    static func getBeneficiryInformations(identifiant : String,profil : String, success: @escaping (AffiliatedBeneficiaryCor)-> Void , failure: @escaping (String) -> Void ){
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.BeneficiaryInformations(identifiant : identifiant,profil : profil.lowercased()).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
                if(httpResponse.statusCode == 200 || httpResponse.statusCode == 400){
                    let json = JSON(data: data!)
                    let affiliatedBeneficiaryCor = AffiliatedBeneficiaryCor(fromJSON: json)
                    success(affiliatedBeneficiaryCor)
                }else{
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                }
            }else{
                failure("\(String(describing: error?.localizedDescription))")
            }
        })
        task.resume()
    }
    
   
    static func getBeneficiryAffiliatedCor(profil : String,identifiant : String, success: @escaping (AffiliatedBeneficiaryCor) -> Void, failure: @escaping (String) -> Void ){
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.getBeneficiryAffiliatedCor(profil : profil ,identifiant : identifiant).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
                switch httpResponse.statusCode {
                case 200 :
                    
                    let json = JSON(data: data!)
                    success(AffiliatedBeneficiaryCor(fromJSON: json))
                    
                default:
                    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }
            
        })
        task.resume()
    }
    
    
    static func addComplaint(_ data: URLRequestParams, completionHandler: @escaping (String) -> (), failure: @escaping (String) -> Void ){
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.addComplaintBeneficiary(data: data).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
                
                switch httpResponse.statusCode {
                case 200 :
                    
                    let result = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
                    completionHandler(result)
                 
                    
                default:
                    
                   failure("Problème de connexion au serveur. Veuillez réessayer")
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }

        })
        task.resume()
    }
    
    static func FillingComplaint(identifiant : String,profil : String, success: @escaping ([Complaint])-> Void , failure: @escaping (String) -> Void ){
        var ComplaintArray = [Complaint]()
        let session = URLSession.shared
        let task = session.dataTask(with: Router.FollowUpComplaint(identifiant : identifiant,profil : profil).urlRequest!, completionHandler: {data,response,error in
            
            
            if let httpResponse = response as? HTTPURLResponse {
                
                switch httpResponse.statusCode {
                case 200 :
                    
                    // Convert the data to JSON
                    let json = JSON(data: data!)
                    for i in 0..<json.count {
                        let complaintitem = Complaint(fromJSON: json[i])
                        ComplaintArray.append(complaintitem)
                    }
                    
                    success(ComplaintArray)
                default:
                    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }
            
        })
        task.resume()
    }
   

    
    static func FollowUpComplaint(identifiant : String,profil : String, success: @escaping ([Complaint])-> Void , failure: @escaping (String) -> Void ){
        var ComplaintArray = [Complaint]()
        let session = URLSession.shared
        let task = session.dataTask(with: Router.FollowUpComplaint(identifiant : identifiant,profil : profil).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {

                if(httpResponse.statusCode == 200 || httpResponse.statusCode == 400){
                    
                    let json = JSON(data: data!)
                    for i in 0..<json.count {
                        let complaintitem = Complaint(fromJSON: json[i])
                        ComplaintArray.append(complaintitem)
                    }
                    success(ComplaintArray)
                    
                    
                }else{
                    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                    
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }
            
        })
        task.resume()
    }
    

    static func Complaintdetails(identifiant : String,complaintId : String, success: @escaping (String)  -> Void  , failure: @escaping (String) -> Void ){
       
        let session = URLSession.shared
        let task = session.dataTask(with: Router.Complaintdetails(identifiant : identifiant,complaintId : complaintId).urlRequest!, completionHandler: {data,response,error in
            
         
            
            if let httpResponse = response as? HTTPURLResponse {
                
                if(httpResponse.statusCode == 200 || httpResponse.statusCode == 400){
               
                    let result = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
                    success(result)
                    
                }else{
                    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                    
                }
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }
            
        })
        task.resume()
    }
    
    static func DisplayComplaintDoc(documentId : String, success: @escaping (Data)  -> Void  , failure: @escaping (String) -> Void ){
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.DisplayComplaintDoc(documentId : documentId).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
                
                if(httpResponse.statusCode == 200 || httpResponse.statusCode == 400){

                    success(data!)
                    
                }else{
                    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                    
                }
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }
            
        })
        task.resume()
    }
}
