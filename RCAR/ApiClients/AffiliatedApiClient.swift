//
//  AffiliatedApiClient.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 03/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON


class AffiliatedApiClient: BaseApiClient {
    
    static func addAffiliatedInformations(_ data: URLRequestParams,profil : String,identifiant : String, completionHandler: @escaping (String) -> (), failure: @escaping (String) -> Void ){
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.addAffiliatedInformations(profil: profil.lowercased(), data: data).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
                switch httpResponse.statusCode {
                case 200 :
                    
                 let result = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
                print("resultresult\(result)")
                    completionHandler(result)
                    
                default:
                    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                }
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
            }
            
        })
        task.resume()
    }
    
    static func getAffiliateInformations(identifiant : String,profil : String, success: @escaping (AffiliatedCoordinate)-> Void , failure: @escaping (String) -> Void ){
     
        let session = URLSession.shared
        let task = session.dataTask(with: Router.AffiliateInformations(identifiant : identifiant,profil : profil.lowercased()).urlRequest!, completionHandler: {data,response,error in
     
            if let httpResponse = response as? HTTPURLResponse {
                if(httpResponse.statusCode == 200 || httpResponse.statusCode == 400){
                    let json = JSON(data: data!)
                    let affiliatedCoordinate = AffiliatedCoordinate(fromJSON: json)
                    success(affiliatedCoordinate)
                }else{
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                }
            }else{
                failure("\(String(describing: error?.localizedDescription))")
            }
        })
        task.resume()
    }
    
    static func getAffilie(identifiant : String, success: @escaping (Affiliated) -> Void, failure: @escaping (String) -> Void ){

    let session = URLSession.shared
        let task = session.dataTask(with: Router.getAffilie(identifiant : identifiant).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
                

                if(httpResponse.statusCode == 200 || httpResponse.statusCode == 400){
                    // Convert the data to JSON
                    let json = JSON(data: data!)
                    success(Affiliated(fromJSON: json))
                    
                }else{
                    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                    
                }

                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }

    })
    task.resume()
}
    
    static func simulation(identifiant : String, dateEffet : String, success: @escaping (Simulation) -> Void, failure: @escaping (String) -> Void ){
        
        var components = URLComponents(string: (Router.simulation(identifiant : identifiant,components : URLComponents(string :"https://www.rcar.ma/rcar/affilie/\(identifiant)/simulation-affilie")!).urlRequest?.url?.absoluteString)!)!
        components.queryItems = [
            URLQueryItem(name: "dateEffet", value: dateEffet),
            URLQueryItem(name: "session", value: nil)
        ]
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.simulation(identifiant : identifiant,components : components).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
                
                switch httpResponse.statusCode {
                case 200 :
                    // Convert the data to JSON
                    let json = JSON(data: data!)
                    success(Simulation(fromJSON: json))
                default:
                    
                    failure("\(String(describing: error?.localizedDescription))")
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }
        })
        task.resume()
}
}

