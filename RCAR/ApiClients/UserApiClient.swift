//
//  UserApiClient.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 10/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class UserApiClient: BaseApiClient {
    
    static func login(_ data: URLRequestParams, success: @escaping (Profil) -> (), failure: @escaping (String) -> Void ){
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.login(data: data).urlRequest!, completionHandler: {data,response,error in
            if let httpResponse = response as? HTTPURLResponse {
                if(httpResponse.statusCode == 200 || httpResponse.statusCode == 400){
                  
                    let json = JSON(data: data!)
                    success(Profil(fromJSON: json))
                }else{
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                }
            }else{
                failure("\(String(describing: error?.localizedDescription))")
            }
        })
        task.resume()
    }
    
    static func signup(_ data: URLRequestParams, success: @escaping (JSON) -> Void, failure: @escaping (String) -> Void){
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.signup(data: data).urlRequest!, completionHandler: {data,response,error in
            if let httpResponse = response as? HTTPURLResponse {
                let result = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
                print("inscribeform\(result)")
                switch httpResponse.statusCode {
                case 200 :
                    // Convert the data to JSON
                    let json = JSON(data: data!)
                    success(json)
                default:
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                }
            }else{
                failure("\(String(describing: error?.localizedDescription))")
            }

        })
        task.resume()
    
    }
    
    static func SaveDevice(identifiant : String,deviceToken : String, success: @escaping ([Folder])-> Void , failure: @escaping (String) -> Void ){
    
        let session = URLSession.shared
        let task = session.dataTask(with: Router.SaveDevice(identifiant : identifiant,DeviceToken : deviceToken).urlRequest!, completionHandler: {data,response,error in
            
            
            if let httpResponse = response as? HTTPURLResponse {
                
               
//                if(httpResponse.statusCode == 200 || httpResponse.statusCode == 400){
//
//                    let json = JSON(data: data!)
//                    for i in 0..<json.count {
//                        let folderitem = Folder(fromJSON: json[i])
//                        FolderArray.append(folderitem)
//                    }
//                    //success(FolderArray)
//
//
//                }else{
//
//                    failure("Problème de connexion au serveur. Veuillez réessayer")
//
//                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }
            
            
        })
        task.resume()
    }

}
