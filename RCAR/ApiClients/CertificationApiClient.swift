//
//  CertificationApiClient.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 15/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire


class CertificationApiClient: BaseApiClient {
    

    static func addCertification(_ data: URLRequestParams, success: @escaping (String) ->Void, failure: @escaping (String) -> Void ){
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.addCertification(data: data).urlRequest!, completionHandler: {data,response,error in
            
            
            if let httpResponse = response as? HTTPURLResponse {
                
                switch httpResponse.statusCode {
                case 200 :
                    
                    let result = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
                    success(result)
                    
                default:
                    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }

        })
        task.resume()
    }
    
    static func getcertificationType(_ data: URLRequestParams, success: @escaping ([CertificationType]) ->Void, failure: @escaping (String) -> Void ){
        var CertificationTypeArray = [CertificationType]()
        let session = URLSession.shared
        let task = session.dataTask(with: Router.getcertificationType(data: data).urlRequest!, completionHandler: {data,response,error in
            if let httpResponse = response as? HTTPURLResponse {
                
                switch httpResponse.statusCode {
                case 200 :
                    
                    // Convert the data to JSON
                    let json = JSON(data: data!)
                    for i in 0..<json.count {
                        let certificationTypeitem = CertificationType(fromJSON: json[i])
                        CertificationTypeArray.append(certificationTypeitem)
                    }
                    success(CertificationTypeArray)
                    
                default:
                    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }
            
       
            
     
        })
        task.resume()
    }
    
    static func getNextSeq(_ data: URLRequestParams, success: @escaping (String) ->Void, failure: @escaping (String) -> Void ){
     
        let session = URLSession.shared
        let task = session.dataTask(with: Router.getNextSeq(data: data).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
                
                switch httpResponse.statusCode {
                case 200 :
                    
                    let result = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
                    success(result)
                    
                default:
                    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }

   
        })
        task.resume()
    }
    
}
