//
//  paymentSituationApiClient.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 15/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON


class paymentSituationApiClient: BaseApiClient {
    
    static func getpaymentSituation(identifiant : String, success: @escaping (PaymentSituation) -> Void, failure: @escaping (String) -> Void ){
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.getpaymentSituation(identifiant : identifiant).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
                
                switch httpResponse.statusCode {
                case 200 :
                    
                    let json = JSON(data: data!)
                    success(PaymentSituation(fromJSON: json))
                    
                default:
                    
                     failure("Problème de connexion au serveur. Veuillez réessayer")
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }

        })
        task.resume()
    }
    
    
    static func detailSituation(identifiant : String,moisEche : String,anneeEche : String, success: @escaping ([SituationDetail]) -> Void, failure: @escaping (String) -> Void ){
        
      
        var SituationDetailArray = [SituationDetail]()
        let session = URLSession.shared
        let task = session.dataTask(with: Router.detailSituation(identifiant : identifiant,moisEche : moisEche,anneeEche : anneeEche).urlRequest!, completionHandler: {data,response,error in
            if let httpResponse = response as? HTTPURLResponse {
                
                switch httpResponse.statusCode {
                case 200 :
                    
                    let json = JSON(data: data!)
                    for i in 0..<json.count {
                        let situationDetailitem = SituationDetail(fromJSON: json[i])
                        SituationDetailArray.append(situationDetailitem)
                    }
                    success(SituationDetailArray)
                    
                default:
                    
                      failure("Problème de connexion au serveur. Veuillez réessayer")
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }
        })
        task.resume()
    }
    
    static func printpaymentSituation(identifiant : String,nom : String, success: @escaping (Data) -> Void, failure: @escaping (String) -> Void ){
        
        var components = URLComponents(string: (Router.printpaymentSituation(identifiant : identifiant,components : URLComponents(string :"https://www.rcar.ma/rcar/beneficiaire/\(identifiant)/situation/imprimer")!).urlRequest?.url?.absoluteString)!)!
        components.queryItems = [URLQueryItem(name: "nom", value: nom)]
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.printpaymentSituation(identifiant : identifiant,components : components).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
                
                switch httpResponse.statusCode {
                case 200 :
                    
                    success(data!)
                    
                default:
                    
                      failure("Problème de connexion au serveur. Veuillez réessayer")
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
            }
        })
        task.resume()
    }
    
    
  
    


}

