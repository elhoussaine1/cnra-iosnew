//
//  NewsApiClient.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 07/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class NewsApiClient: BaseApiClient {
    

    static func getNews(identifiant: String, success: @escaping ([News]) -> Void, failure: @escaping (String) -> Void ){
        
         var NewsArray = [News]()
        
        var components = URLComponents(string: (Router.News(identifiant : identifiant,components : URLComponents(string :"https://www.rcar.ma/commun/wcm/actualites/")!).urlRequest?.url?.absoluteString)!)!
        components.queryItems = [
            URLQueryItem(name: "organisme", value: "RCAR"),
            URLQueryItem(name: "all", value: "1"),
            URLQueryItem(name: "langue", value: "Français")
        ]
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.News(identifiant : identifiant,components : components).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
                

                switch httpResponse.statusCode {
                case 200 :
                    
                    // Convert the data to JSON
                    let json = JSON(data: data!)
                    for i in 0..<json.count {
                        let newsitem = News(fromJSON: json[i])
                        NewsArray.append(newsitem)
                    }
                    success(NewsArray)
                    
                default:
                    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }
            
            
            
        })
        task.resume()
    }
    
    
}
