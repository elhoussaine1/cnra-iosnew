//
//  AppointmentApiClient.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 13/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON


class AppointmentApiClient: BaseApiClient {
    
    
    static func HoursList(date : String,type : String, success: @escaping (Data) -> Void, failure: @escaping (String) -> Void ){
        
        var components = URLComponents(string: (Router.HoursList(components : URLComponents(string :"https://www.rcar.ma/commun/rendez-vous/listHeuresByJour")!).urlRequest?.url?.absoluteString)!)!
        components.queryItems = [
            URLQueryItem(name: "date", value: date),
            URLQueryItem(name: "type", value: type)
        ]
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.HoursList(components : components).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
                
                switch httpResponse.statusCode {
                case 200 :
                    
                    success(data!)
                default:
                    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }

        })
        task.resume()
    }
    
    
    static func existAppointment(identifiant : String, success: @escaping (Appointment) -> Void, failure: @escaping (String) -> Void ){
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.existAppointment(identifiant : identifiant).urlRequest!, completionHandler: {data,response,error in
             // let result = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
            
            if let httpResponse = response as? HTTPURLResponse {
                
                switch httpResponse.statusCode {
                case 200 :
                    
                    // Convert the data to JSON
                    let json = JSON(data: data!)
                    success(Appointment(fromJSON: json))
                default:
                    
                   failure("Problème de connexion au serveur. Veuillez réessayer")
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }
  
        })
        task.resume()
    }
  
    
    static func addAppointment(identifiant : String,_ data: URLRequestParams, success: @escaping (String) ->Void, failure: @escaping (String) -> Void ){
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.addAppointment(identifiant : identifiant,data: data).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
                
                switch httpResponse.statusCode {
                case 200 :
                  
                    let result = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
                   success(result)
                default:
              
                   failure("Problème de connexion au serveur. Veuillez réessayer")
                }
                
            }else{
          
                failure("\(String(describing: error?.localizedDescription))")
                
            }
            
         

        })
        task.resume()
    }
    

    
    static func cancelAppointment(identifiant : String,_ data: URLRequestParams, success: @escaping (String) ->Void, failure: @escaping (String) -> Void ){
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.cancelAppointment(identifiant : identifiant,data: data).urlRequest!, completionHandler: {data,response,error in
         
            if let httpResponse = response as? HTTPURLResponse {
                
                switch httpResponse.statusCode {
                case 200 :
               
                    let result = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
              
                    success(result)
                    
                default:
                    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }

        })
        task.resume()
    }
    
}
