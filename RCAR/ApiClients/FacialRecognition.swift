//
//  FacialRecognition.swift
//  RCAR
//
//  Created by El houssaine el gamouz on 19/10/2020.
//  Copyright © 2020 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON

class FacialRecognition: BaseApiClient {

static func lifeControl(_ data: URLRequestParams, success: @escaping (String) -> Void, failure: @escaping (String) -> Void ){
    
    
    let session = URLSession.shared
    let task = session.dataTask(with: Router.getLifeControl(data: data).urlRequest!, completionHandler: {data,response,error in
        
        if let httpResponse = response as? HTTPURLResponse {
            switch httpResponse.statusCode {
            case 200 :
        
                let result = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
                success(result)
                
            default:
                failure("Problème de connexion au serveur. Veuillez réessayer")
            }
        }else{
            failure("\(String(describing: error?.localizedDescription))")
        }
    })
    task.resume()
  
}
    
    static func ActivatePassPortFacePrint(_ data: URLRequestParams, success: @escaping (String) ->Void, failure: @escaping (String) -> Void ){
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.ActivatePassPortFacePrint(data: data).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
                switch httpResponse.statusCode {
                case 200 :
            
                    let result = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
                    success(result)
                    
                default:
    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                }
            }else{
                failure("\(String(describing: error?.localizedDescription))")
            }
        })
        task.resume()
    }
    
    static func ActivateFacePrint(_ data: URLRequestParams, success: @escaping (String) ->Void, failure: @escaping (String) -> Void ){
        
        let session = URLSession.shared
    let task = session.dataTask(with: Router.ActivateFacePrint(data: data).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
                
                switch httpResponse.statusCode {
                case 200 :
            
                    let result = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
                    success(result)
                    
                default:
    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                }
            }else{
                failure("\(String(describing: error?.localizedDescription))")
            }
        })
        task.resume()
    }
        
    static func CheckCodeActivation(produit: String,numClient: String,codeActivation: String,token:String, success: @escaping (String) -> Void, failure: @escaping (String) -> Void ){
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.CheckCodeActivation(produit: produit, numClient: numClient, codeActivation: codeActivation,token:token).urlRequest!, completionHandler: {data,response,error in

            if let httpResponse = response as? HTTPURLResponse {

                switch httpResponse.statusCode {
                case 200 :
                    let result = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
                    success(result)
                default:
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                }

            }else{
                failure("\(String(describing: error?.localizedDescription))")
            }

        })
        task.resume()
    }
    
 
}
