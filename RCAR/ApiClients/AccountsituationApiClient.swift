//
//  AccountsituationApiClient.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 06/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire


class AccountsituationApiClient: BaseApiClient {
    
    
    
    static func AccountsituationRCDtos(identifiant : String, success: @escaping (AccountsituationRC) -> Void, failure: @escaping (String) -> Void ){
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.AccountsituationRCDtos(identifiant : identifiant).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
                
                if(httpResponse.statusCode == 200 || httpResponse.statusCode == 400){
                    // Convert the data to JSON
                    let json = JSON(data: data!)
                    success(AccountsituationRC(fromJSON: json))
                    
                }else{
                    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                    
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }
            
 
        })
        task.resume()
    }
    
 
    static func AccountsituationRGDtos(identifiant : String, success: @escaping (AccountsituationRG) -> Void, failure: @escaping (String) -> Void ){
        
        let session = URLSession.shared
        let task = session.dataTask(with: Router.AccountsituationRGDtos(identifiant : identifiant).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {

                if(httpResponse.statusCode == 200 || httpResponse.statusCode == 400){
                    // Convert the data to JSON
                    let json = JSON(data: data!)
                    success(AccountsituationRG(fromJSON: json))
                    
                }else{
                    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                    
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }
 
        })
        task.resume()
    }
   
    static func subscriptiondetails(identifiant : String,year : String, success: @escaping ([contributionRGDtos]) -> Void, failure: @escaping (String) -> Void ){
         var contributionRGDtosArray = [contributionRGDtos]()
        let session = URLSession.shared
        let task = session.dataTask(with: Router.subscriptiondetails(identifiant : identifiant,year :year).urlRequest!, completionHandler: {data,response,error in
            
            if let httpResponse = response as? HTTPURLResponse {
                
                if(httpResponse.statusCode == 200 || httpResponse.statusCode == 400){
                    // Convert the data to JSON
                    let json = JSON(data: data!)
                    for i in 0..<json.count {
                        let contributionRGDtositem = contributionRGDtos(fromJSON: json[i])
                        contributionRGDtosArray.append(contributionRGDtositem)
                    }
                    success(contributionRGDtosArray)
                    
                }else{
                    
                    failure("Problème de connexion au serveur. Veuillez réessayer")
                    
                }
                
            }else{
                
                failure("\(String(describing: error?.localizedDescription))")
                
            }
            
        })
        task.resume()
    }
}
