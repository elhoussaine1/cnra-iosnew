//
//  AppDelegate.swift
//  MaPlage
//
//  Created by lotfi on 17/08/2017.
//  Copyright © 2017 Aramobile. All rights reserved.
//
import UIKit
import AVFoundation
import IQKeyboardManagerSwift
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import SFaceCompare
import FirebaseRemoteConfig
import CoreNFC
import PushKit
import Foundation


//5114158201
//867478721
//retourne  vers activation de code
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UIAlertViewDelegate, UNUserNotificationCenterDelegate, MessagingDelegate   {
    
    var window: UIWindow?
    var mainNC: RCARNavigationController?
    var mainVC: HomeViewController?
    var sideMenuViewController: MMDrawerController?
    var isActif         : Bool! = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        // Loads important additional data
        UserDefaults.standard.set(true, forKey: "ClCheckBox")
        SFaceCompare.prepareData()
        Profil.logout()
        //change status bar color
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        //Create the notificationCenter
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        self.window!.backgroundColor = UIColor.white
        
        self.loadnAppController()
      
        //configure keyboard
        IQKeyboardManager.shared.enable = true
    
        self.window?.makeKeyAndVisible()
        return true
    }

    func loadnAppController(){
        
        let curentController : UIViewController = HomeViewController() as HomeViewController
        let navigation : RCARNavigationController = RCARNavigationController(rootViewController: curentController) as RCARNavigationController
        navigation.delegate = navigation
        
        let menuViewController : MenuViewController = MenuViewController()
        navigation.navigationBar.tintColor = UIColor.black//UIColor(red: 0, green: 187.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        navigation.navigationBar.barTintColor = UIColor.clear
            //Utils.color(forHex: "648d2f")
       // navigation.navigationBar.titleTextAttributes = [NSAttributedSNSAttributedString.KeytringKey.foregroundColor: UIColor.black]
        navigation.navigationBar.barStyle = UIBarStyle.black
        navigation.navigationBar.isTranslucent = false
     
        self.sideMenuViewController = MMDrawerController(center: navigation, leftDrawerViewController: menuViewController)
    self.sideMenuViewController!.setDrawerVisualStateBlock(MMDrawerVisualState.slideVisualStateBlock()!)
        self.sideMenuViewController!.maximumLeftDrawerWidth = self.window!.bounds.size.width - 50
        self.sideMenuViewController!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.all
        self.sideMenuViewController!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.all
        self.window!.rootViewController = sideMenuViewController
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        Messaging.messaging().apnsToken = deviceToken
        Messaging.messaging().subscribe(toTopic: "/topics/general")
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                UserDefaults.standard.removeObject(forKey: "DeviceToken")
                UserDefaults.standard.set(result.token, forKey: "DeviceToken")
                UserDefaults.standard.synchronize() 
            }
        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        UserDefaults.standard.removeObject(forKey: "DeviceToken")
        UserDefaults.standard.set(fcmToken, forKey: "DeviceToken")
        UserDefaults.standard.synchronize()
    }

    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        //print("Notification: Unable to register for remote notifications: \(error.localizedDescription)")
    }
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        self.isActif = false;
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
         Profil.logout()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    


    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        //logout whene application terminate
        Profil.logout()
    }
    
 
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
        
        // custom code to handle push while app is in the foreground
        let dict = notification.request.content.userInfo["aps"] as! NSDictionary
        let d : [String : Any] = dict["alert"] as! [String : Any]
        let title : String = d["title"] as! String
        let body : String = d["body"] as! String
        let message : String = "message"

        // Create the alert controller
        let alertController = UIAlertController(title: title, message: body, preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: "Annuler", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }

        // Add the actions
        alertController.addAction(cancelAction)

        // Present the controller
        window?.rootViewController?.present(alertController, animated: false, completion: nil)
    }
    
    //Register for push notification.
    func registerForPushNotifications(application: UIApplication) {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.alert,.sound]) { (granted, error) in
                if error == nil{
                    DispatchQueue.main.async(execute: {
                        application.registerForRemoteNotifications()
                    })
                }
            }
        }
        else {
            
            let settings = UIUserNotificationSettings(types: [.alert,.sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
        
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
    }
    
    @objc func tokenRefreshNotification(_ notification: Notification) {
        print(#function)
        //        if let refreshedToken = InstanceID.instanceID().token() {
        //            NSLog("Notification: refresh token from FCM -> \(refreshedToken)")
        //
        //        }
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                
                guard result.token != nil else {
                    NSLog("FCM: Token does not exist.")
                    return
                }
            }
        }
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    func connectToFcm() {
        // Won't connect since there is no token
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                
                guard result.token != nil else {
                    NSLog("FCM: Token does not exist.")
                    return
                }
            }
        }
        
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    // iOS9, called when presenting notification in foreground
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    }
    
  

}

