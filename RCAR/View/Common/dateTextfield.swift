//
//  dateTextfield.swift
//  Switch Eat
//
//  Created by mac on 13/10/2017.
//  Copyright © 2017 Le Diet. All rights reserved.
//

//import UIKit


// MARK: - Protocols


@IBDesignable class dateTextfield: XibView, UITextFieldDelegate
{
    // MARK: - Properties
    //fileprivate var placeholderText             =   ""
    fileprivate var shouldSetReturnKeyToNext    =   false
    fileprivate var containerView               :   UIView?
    {
        var viewToMove  :   UIView?
        
        if let view = (delegate as? UIView)
        {
            viewToMove  =   view
        }
        
        if let view = (delegate as? UIViewController)?.view
        {
            viewToMove  =   view
        }
        
        return viewToMove
    }
    var moveHeight  :   CGFloat             =   50
    var delegate    :   TextFieldDelegate?
    // MARK: - Inspectable attributes
  
   /* @IBInspectable var placeholder: String
    {
        get
        {
            return self.placeholderText
        }
        set
        {
            self.placeholderText        =   newValue
            self.txtField.placeholder   =   newValue
        }
    }*/
    
    @IBInspectable var hasNext: Bool
    {
        get
        {
            return self.shouldSetReturnKeyToNext
        }
        set
        {
            self.shouldSetReturnKeyToNext   =   newValue
            self.txtField.returnKeyType     =   newValue    ?   .next   :   .done
        }
    }
    
    @IBInspectable var isPassword: Bool
    {
        get
        {
            return self.txtField.isSecureTextEntry
        }
        set
        {
            self.txtField.isSecureTextEntry   =   newValue
        }
    }
    @IBInspectable var hideImage: Bool
        {
        get
        {
            return self.image_txtField.isHidden
        }
        set
        {
            self.image_txtField.isHidden   =   newValue
            self.txtField.returnKeyType     =   newValue    ?   .next   :   .done
        }
    }
    
    // MARK: - Outlets
    @IBOutlet weak var txtField         : UITextField!
    @IBOutlet weak var image_txtField   : UIImageView!
    // MARK: - Autolayout constraints
    
    
    // MARK: - Initialization
    override func initView()
    {
        UITextField.appearance().tintColor = UIColor.blue
        self.view.layoutIfNeeded()
    }
    
    
    // MARK: - Methods
  
    
    fileprivate func moveSuperViewTo(_ yPosition: CGFloat)
    {
        UIView.animate(withDuration: 0.3)
        {
            self.containerView?.changeYPosition(yPosition)
        }
    }
    
    
    // MARK: - Txtfield delegate methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.moveSuperViewTo(0)

        if self.hasNext
        {
         //   self.delegate?.textfieldDidReturn(self)
        }
        else
        {
            self.txtField.resignFirstResponder()
        }
        
        return true
    }
    
    // MARK: - Actions
    @IBAction func actionTextBeginEditing(_ sender: UITextField)
    {
        self.moveSuperViewTo(-self.moveHeight)
    }
    @IBAction func actionTextChanged(_ sender: UITextField)
    {
    }
}
