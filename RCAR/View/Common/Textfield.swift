//
//  Textfield.swift
//  Switch Eat
//
//  Created by mac on 13/10/2017.
//  Copyright © 2017 Le Diet. All rights reserved.
//

//import UIKit


// MARK: - Protocols
protocol TextFieldDelegate
{
    func textfieldDidReturn(_ textfield: Textfield)
}


@IBDesignable class Textfield: XibView, UITextFieldDelegate
{
    // MARK: - Properties
    var iconImage                   =   UIImage()
    //fileprivate var placeholderText             =   ""
    fileprivate var shouldSetReturnKeyToNext    =   false
    fileprivate var containerView               :   UIView?
    {
        var viewToMove  :   UIView?
        
        if let view = (delegate as? UIView)
        {
            viewToMove  =   view
        }
        
        if let view = (delegate as? UIViewController)?.view
        {
            viewToMove  =   view
        }
        
        return viewToMove
    }
    var moveHeight  :   CGFloat             =   50
    var delegate    :   TextFieldDelegate?
    // MARK: - Inspectable attributes
    @IBInspectable var icon: UIImage
        {
        get
        {
            return self.iconImage
        }
        set
        {
            self.iconImage      =   newValue
            self.imgIcon.image  =   newValue
        }
    }
    
    /* @IBInspectable var placeholder: String
     {
     get
     {
     return self.placeholderText
     }
     set
     {
     self.placeholderText        =   newValue
     self.txtField.placeholder   =   newValue
     }
     }*/
    
    @IBInspectable var hasNext: Bool
        {
        get
        {
            return self.shouldSetReturnKeyToNext
        }
        set
        {
            self.shouldSetReturnKeyToNext   =   newValue
            self.txtField.returnKeyType     =   newValue    ?   .next   :   .done
        }
    }
    
    @IBInspectable var isPassword: Bool
        {
        get
        {
            return self.txtField.isSecureTextEntry
        }
        set
        {
            self.txtField.isSecureTextEntry   =   newValue
        }
    }
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var rightIcon: UIImageView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var lbldiscription: UILabel!
    
    
    
    // MARK: - Autolayout constraints
    @IBOutlet weak var layLblErrorHeight: NSLayoutConstraint!
    
    
    // MARK: - Initialization
    override func initView()
    {
        self.layLblErrorHeight.constant     =   0
        self.imgIcon.alpha = 0
        self.rightIcon.isHidden = true
        self.lblError.setNeedsLayout()
        UITextField.appearance().tintColor = UIColor.blue
        self.view.layoutIfNeeded()
    }
    
    
    // MARK: - Methods
    func showError(_ text: String)
    {
        self.lblError.text                  =   text
        self.layLblErrorHeight.constant     =   16
        self.lblError.setNeedsLayout()
        
        UIView.animate(withDuration: 0.3)
        {
            self.imgIcon.alpha = 1
            self.view.borderColor           =   #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            self.view.layoutIfNeeded()
        }
    }
    
    fileprivate func moveSuperViewTo(_ yPosition: CGFloat)
    {
        UIView.animate(withDuration: 0.3)
        {
            self.containerView?.changeYPosition(yPosition)
        }
    }
    
    
    // MARK: - Txtfield delegate methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.moveSuperViewTo(0)
        
        if self.hasNext
        {
            self.delegate?.textfieldDidReturn(self)
        }
        else
        {
            self.txtField.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDisabling(_ Enabled: Bool)
    {
        self.txtField.isUserInteractionEnabled = Enabled
        
    }
    
    func rightIconHidding(_ hidden: Bool,iconName: String)
    {

        self.rightIcon.isHidden = hidden
        self.rightIcon.image =  UIImage(named: iconName as String)!
        
    }
    
    // MARK: - Actions
    @IBAction func actionTextBeginEditing(_ sender: UITextField)
    {
        self.moveSuperViewTo(-self.moveHeight)
    }
    
    @IBAction func actionTextChanged(_ sender: UITextField)
    {
        self.layLblErrorHeight.constant     =   0
        self.lblError.setNeedsLayout()
        
        UIView.animate(withDuration: 0.3)
        {
            self.imgIcon.alpha = 0
            self.view.borderColor           =   #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            self.view.layoutIfNeeded()
        }
    }
}
