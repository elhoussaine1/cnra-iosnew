//
//  Tools.swift
//
//
//  Created by mac on 02/02/2017.
//  Copyright © 2017. All rights reserved.
//

import UIKit
import SystemConfiguration


// MARK: - Constants
let IS_IPHONE_4_OR_LESS     =   SCREEN_MAX_LENGTH < 568.0
let IS_IPHONE_5             =   SCREEN_MAX_LENGTH == 568.0
let IS_IPHONE_6             =   SCREEN_MAX_LENGTH == 667.0
let IS_IPHONE_6P            =   SCREEN_MAX_LENGTH == 736.0

let SCREEN_WIDTH            =   UIScreen.main.bounds.size.width
let SCREEN_HEIGHT           =   UIScreen.main.bounds.size.height
let SCREEN_MAX_LENGTH       =   max(SCREEN_WIDTH, SCREEN_HEIGHT)
let SCREEN_MIN_LENGTH       =   min(SCREEN_WIDTH, SCREEN_HEIGHT)


open class Reachability
{
    class func isConnectedToNetwork() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress)
        {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) { zeroSockAddress in SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress) }
        }
        
        var flags = SCNetworkReachabilityFlags()
        
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) { return false }
        
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection)
    }
}

public extension UIDevice {
    
   static var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}

private var AssociatedObjectHandle: UInt8 = 0

extension UINavigationBar
{
    var height: CGFloat
        {
        get { if let h = objc_getAssociatedObject(self, &AssociatedObjectHandle) as? CGFloat { return h }; return 0 }
        set { objc_setAssociatedObject(self, &AssociatedObjectHandle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }
    
    override open func sizeThatFits(_ size: CGSize) -> CGSize
    {
        if self.height > 0 { return CGSize(width: self.superview!.bounds.size.width, height: self.height) }
        return super.sizeThatFits(size)
    }
}

//  SwiftHEXColors.swift
//
// Copyright (c) 2014 SwiftHEXColors contributors

#if os(iOS) || os(tvOS)
    import UIKit
    typealias SWColor = UIColor
#else
    import Cocoa
    typealias SWColor = NSColor
#endif

private extension Int {
    func duplicate4bits() -> Int {
        return (self << 4) + self
    }
}

/// An extension of UIColor (on iOS) or NSColor (on OSX) providing HEX color handling.
public extension SWColor {
    /**
     Create non-autoreleased color with in the given hex string. Alpha will be set as 1 by default.
     - parameter hexString: The hex string, with or without the hash character.
     - returns: A color with the given hex string.
     */
    public convenience init?(_ hexString: String) {
        self.init(hexString, alpha: 1.0)
    }
    
    fileprivate convenience init?(hex3: Int, alpha: Float) {
        self.init(red:   CGFloat( ((hex3 & 0xF00) >> 8).duplicate4bits() ) / 255.0,
                  green: CGFloat( ((hex3 & 0x0F0) >> 4).duplicate4bits() ) / 255.0,
                  blue:  CGFloat( ((hex3 & 0x00F) >> 0).duplicate4bits() ) / 255.0,
                  alpha: CGFloat(alpha))
    }
    
    fileprivate convenience init?(hex6: Int, alpha: Float) {
        self.init(red:   CGFloat( (hex6 & 0xFF0000) >> 16 ) / 255.0,
                  green: CGFloat( (hex6 & 0x00FF00) >> 8 ) / 255.0,
                  blue:  CGFloat( (hex6 & 0x0000FF) >> 0 ) / 255.0, alpha: CGFloat(alpha))
    }
    
    /**
     Create non-autoreleased color with in the given hex string and alpha.
     - parameter hexString: The hex string, with or without the hash character.
     - parameter alpha: The alpha value, a floating value between 0 and 1.
     - returns: A color with the given hex string and alpha.
     */
    public convenience init?(_ hexString: String, alpha: Float) {
        var hex = hexString
        
        // Check for hash and remove the hash
        if hex.hasPrefix("#") { hex = String(hex.characters.dropFirst()) }
        
        guard let hexVal = Int(hex, radix: 16) else {
            self.init()
            return nil
        }
        
        switch hex.characters.count {
        case 3:
            self.init(hex3: hexVal, alpha: alpha)
        case 6:
            self.init(hex6: hexVal, alpha: alpha)
        default:
            // Note:
            // The swift 1.1 compiler is currently unable to destroy partially initialized classes in all cases,
            // so it disallows formation of a situation where it would have to.  We consider this a bug to be fixed
            // in future releases, not a feature. -- Apple Forum
            self.init()
            return nil
        }
    }
    
    /**
     Create non-autoreleased color with in the given hex value. Alpha will be set as 1 by default.
     - parameter hex: The hex value. For example: 0xff8942 (no quotation).
     - returns: A color with the given hex value
     */
    public convenience init?(hex: Int) {
        self.init(hex: hex, alpha: 1.0)
    }
    
    /**
     Create non-autoreleased color with in the given hex value and alpha
     - parameter hex: The hex value. For example: 0xff8942 (no quotation).
     - parameter alpha: The alpha value, a floating value between 0 and 1.
     - returns: color with the given hex value and alpha
     */
    public convenience init?(hex: Int, alpha: Float) {
        if (0x000000 ... 0xFFFFFF) ~= hex {
            self.init(hex6: hex, alpha: alpha)
        } else {
            self.init()
            return nil
        }
    }
}

extension UIView
{
    func setFrame(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) { self.frame = CGRect(x: x, y: y, width: width, height: height) }
    
    func changeXPosition(_ to: CGFloat) { frame = CGRect(x: to, y: frame.origin.y, width: frame.width, height: frame.height) }
    
    func changeYPosition(_ to: CGFloat) { frame = CGRect(x: frame.origin.x, y: to, width: frame.width, height: frame.height) }
    
    func changeWidth(_ to: CGFloat) { frame = CGRect(x: frame.origin.x, y: frame.origin.y, width: to, height: frame.height) }
    
    func changeHeight(_ to: CGFloat) { frame = CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.width, height: to) }
    
    func alterXPosition(_ by: CGFloat) { changeXPosition(frame.origin.x+by) }
    
    func alterYPosition(_ by: CGFloat) { changeYPosition(frame.origin.y+by) }
    
    func alterWidth(_ by: CGFloat) { changeWidth(frame.width+by) }
    
    func alterHeight(_ by: CGFloat) { changeHeight(frame.height+by) }
    
    func addConstraintsWithFormat(_ format: String, views: UIView...)
    {
        var viewsDictionary = [String: UIView]()
        
        for (index, view) in views.enumerated()
        {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    func setConstraints(_ constraint: String, subviews: UIView...) { for (_, subview) in subviews.enumerated() { addConstraintsWithFormat(constraint, views: subview) } }
    
    func addSubviews(_ subviews: UIView...) { for (_, subview) in subviews.enumerated()
    { addSubview(subview) } }
    
    func removeSubview(_ subview: UIView) { subview.removeFromSuperview() }
    
    func removeSubviews(_ subviews: UIView...) { for (_, subview) in subviews.enumerated() { subview.removeFromSuperview() } }
    
    @IBInspectable var cornerRadius: CGFloat { get { return layer.cornerRadius } set { layer.cornerRadius = newValue; layer.masksToBounds = newValue > 0 } }
    
    @IBInspectable var borderWidth: CGFloat { get { return layer.borderWidth } set { layer.borderWidth = newValue } }
    
    @IBInspectable var borderColor: UIColor? { get { return UIColor(cgColor: layer.borderColor!) } set { layer.borderColor = newValue?.cgColor } }
    
    @IBInspectable var leftBorderWidth: CGFloat
        {
        get { return 0 /* Just to satisfy property */ }
        set
        {
            let line = UIView(frame: CGRect(x: 0.0, y: 0.0, width: newValue, height: bounds.height))
            line.translatesAutoresizingMaskIntoConstraints = false
            line.backgroundColor = UIColor(cgColor: layer.borderColor!)
            self.addSubview(line)
            
            let views = ["line": line]
            let metrics = ["lineWidth": newValue]
            
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[line(==lineWidth)]", options: [], metrics: metrics, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[line]|", options: [], metrics: nil, views: views))
        }
    }
    
    @IBInspectable var topBorderWidth: CGFloat
        {
        get { return 0 /* Just to satisfy property */ }
        set
        {
            let line = UIView(frame: CGRect(x: 0.0, y: 0.0, width: bounds.width, height: newValue))
            line.translatesAutoresizingMaskIntoConstraints = false
            line.backgroundColor = borderColor
            self.addSubview(line)
            
            let views = ["line": line]
            let metrics = ["lineWidth": newValue]
            
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[line]|", options: [], metrics: nil, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[line(==lineWidth)]", options: [], metrics: metrics, views: views))
        }
    }
    
    @IBInspectable var rightBorderWidth: CGFloat
        {
        get { return 0 /* Just to satisfy property */ }
        set
        {
            let line = UIView(frame: CGRect(x: bounds.width, y: 0.0, width: newValue, height: bounds.height))
            line.translatesAutoresizingMaskIntoConstraints = false
            line.backgroundColor = borderColor
            self.addSubview(line)
            
            let views = ["line": line]
            let metrics = ["lineWidth": newValue]
            
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "[line(==lineWidth)]|", options: [], metrics: metrics, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[line]|", options: [], metrics: nil, views: views))
        }
    }
    
    @IBInspectable var bottomBorderWidth: CGFloat
        {
        get { return 0 /* Just to satisfy property */ }
        set
        {
            let line = UIView(frame: CGRect(x: 0.0, y: bounds.height, width: bounds.width, height: newValue))
            line.translatesAutoresizingMaskIntoConstraints = false
            line.backgroundColor = borderColor
            self.addSubview(line)
            
            let views = ["line": line]
            let metrics = ["lineWidth": newValue]
            
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[line]|", options: [], metrics: nil, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[line(==lineWidth)]|", options: [], metrics: metrics, views: views))
        }
    }
    
    func setBorder(_ color: UIColor, _ width: (top: CGFloat, left: CGFloat, right: CGFloat, bottom: CGFloat))
    {
        self.borderColor = color
        self.topBorderWidth = width.top
        self.leftBorderWidth = width.left
        self.rightBorderWidth = width.right
        self.bottomBorderWidth = width.bottom
    }
    
    func setCornersRadius(_ corners: UIRectCorner, radius: CGFloat)
    {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        layer.mask = maskLayer
    }
    
    // Empty action to provide to a selector
    func noAction(){}
    
    //    func glowX() { borderWidth = 2; borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1) }
    
    func x() -> CGFloat { return frame.origin.x }; func y() -> CGFloat { return frame.origin.y }
    
    func copy<T: UIView>() -> T { return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self)) as! T }
    
    var bottom: CGPoint
    {
        return CGPoint(x:(self.frame.size.width-self.frame.origin.x)/2, y: self.frame.origin.y+self.frame.size.height)
    }
}

extension UISegmentedControl
{
    func changeTitleFont(_ newFontName:String?, newFontSize:CGFloat?)
    {
        let lineattribute : [NSAttributedString.Key : Any] = [
            .font : UIFont(name: newFontName!, size: newFontSize!)]
        setTitleTextAttributes(lineattribute, for: UIControl.State())
    }
}

extension Int
{
    func durationString() -> String
    {
        let hoursText = String(format: "%02d", self / 3600)
        let minutesText = String(format: "%02d", self / 60)
        let secondsText = String(format: "%02d", self % 60)
        let durationText = "\(hoursText):\(minutesText):\(secondsText)"
        
        return durationText
    }
}

extension Float
{
    func durationString() -> String
    {
        let hoursText = String(format: "%02d", Int(self) / 3600)
        let minutesText = String(format: "%02d", Int(self) / 60)
        let secondsText = String(format: "%02d", Int(self) % 60)
        let durationText = "\(hoursText):\(minutesText):\(secondsText)"
        
        return durationText
    }
}

extension Double
{
    func durationString() -> String
    {
        let hoursText = String(format: "%02d", Int(self) / 3600)
        let minutesText = String(format: "%02d", Int(self) / 60)
        let secondsText = String(format: "%02d", Int(self) % 60)
        let durationText = "\(hoursText):\(minutesText):\(secondsText)"
        
        return durationText
    }
}

extension String
{
    func capitalizingFirstLetter() -> String
    {
        
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        
        return first + other
    }
    
    mutating func capitalizeFirstLetter() { self = self.capitalizingFirstLetter() }
    
    func intArray(_ separatedBy: String) -> [Int] { if self != "" { return self.components(separatedBy: separatedBy).map{Int($0)!} } else { return [] } }
    
    func isAValidEmailAddress() -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: self)
    }
}

extension NSObject
{
    func printFonts()
    {
        let fontFamilyNames = UIFont.familyNames
        
        for familyName in fontFamilyNames
        {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName)
            print("Font Names = [\(names)]")
        }
    }
}

extension UIViewController
{    
    // Empty action to provide to a selector
    func noAction(){}
}


extension UITextField
{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[kCTForegroundColorAttributeName as NSAttributedString.Key: newValue!])
        }
    }
}


/*
extension JSON
{
    public var dateValue: Date? {
        get {
            if let str = self.string {
                return JSON.jsonDateFormatter.date(from: str)
            }
            return nil
        }
    }
    
    private static let jsonDateFormatter: DateFormatter =
    {
        let fmt = DateFormatter()
        fmt.dateFormat = "yyyy-MM-dd HH:mm:ss"
        fmt.timeZone = TimeZone(secondsFromGMT: 0)
        return fmt
    }()
}

 */
extension Date
{
    func tempsJusquaMaintenant() -> String
    {
        let date = NSDate()
        let dateFormatter = DateFormatter()
        let timeZone = NSTimeZone(name: "GMT")
        dateFormatter.timeZone = timeZone as TimeZone!
        
        let secondsAgo = Int(date.timeIntervalSince(self))
        
        let minute = 60
        let hour = 60 * minute
        let day = 24 * hour
        let week = 7 * day
        let month = 4 * week
        let year = 12 * month
        
        if secondsAgo < minute { return "Il y'a \(secondsAgo) seconde\( secondsAgo > 1 ? "s" : "")" }
        else if secondsAgo < hour { return "Il y'a \(secondsAgo/minute) minute\( (secondsAgo/minute) > 1 ? "s" : "")" }
        else if secondsAgo < day { return "Il y'a \(secondsAgo/hour) heure\( (secondsAgo/hour) > 1 ? "s" : "")" }
        else if secondsAgo < week { return "Il y'a \(secondsAgo/day) jour\( (secondsAgo/day) > 1 ? "s" : "")" }
        else if secondsAgo < month { return "Il y'a \(secondsAgo/week) semaine\( (secondsAgo/week) > 1 ? "s" : "")" }
        else if secondsAgo < year { return "Il y'a \(secondsAgo/month) mois" }
        else { return "Il y'a \(secondsAgo/year) an\( (secondsAgo/year) > 1 ? "s" : "")" }
    }
}

extension Data
{
    var hexString: String { return map { String(format: "%02.2hhx", arguments: [$0]) }.joined() }
}

// Cute degree operator extension
postfix operator °

protocol IntegerInitializable: ExpressibleByIntegerLiteral
{
    init (_: Int)
}

extension Int: IntegerInitializable
{
    postfix public static func °(lhs: Int) -> CGFloat
    {
        return CGFloat(lhs) * .pi / 180
    }
}
