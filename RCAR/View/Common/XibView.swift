//
//  XibView.swift
//  Switch Eat
//
//  Created by mac on 02/02/2017.
//  Copyright © 2017 Le Diet. All rights reserved.
//

import UIKit


class XibView: UIView
{
    // MARK: - Properties
    var view = UIView()
    
    
    // MARK: - Initialization
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    
    // MARK: - View states
    override func draw(_ rect: CGRect)
    {
        super.draw(rect)
        updateView()
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        updateView()
    }
    
    override func prepareForInterfaceBuilder()
    {
        super.prepareForInterfaceBuilder()
        updateView()
    }
    
    
    // MARK: - Methods
    func initView()
    {
        // This is the place where the view's initial setup must be
    }
    
    func updateView()
    {
        // This is the place where the view's updates must be, ibinspectable changes will happen too
    }
    
    // Setting up the container view
    func xibSetup()
    {
        view = loadViewFromNib()
        
        view.frame = bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(view)
        
        initView()
    }
    
    // Loading the view from the xib file
    func loadViewFromNib() -> UIView!
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
}
