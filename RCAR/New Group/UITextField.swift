//
//  UITextField.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 09/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

extension UITextField {
    
    //add right icon
    func rightIcon(iconImage: String)  {
        self.rightViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let image = UIImage(named: iconImage)
        imageView.image = image
        self.rightView = imageView
    }
}
