//
//  CGRect+Scaled.swift
//  RCAR
//
//  Created by El houssaine el gamouz on 10/11/2020.
//  Copyright © 2020 Aramobile. All rights reserved.
//

import Foundation
import UIKit

extension CGRect {
    func scaled(to size: CGSize) -> CGRect {
        return CGRect(
            x: self.origin.x * size.width,
            y: self.origin.y * size.height,
            width: self.size.width * size.width,
            height: self.size.height * size.height
        )
    }
}

