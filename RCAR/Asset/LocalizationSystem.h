//
//  LocalizationSystem.h
//
//  Created by Brahim on 26/03/14.
//  Copyright (c) 2014 Brahim. All rights reserved.
//
#import <Foundation/Foundation.h>


@interface LocalizationSystem : NSObject {
	NSString *language;
    NSString *lang;
}

@property (nonatomic, retain) NSString *lang;

// you really shouldn't care about this functions and use the MACROS
+ (LocalizationSystem *)sharedLocalSystem;

//gets the string localized
- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)comment;

//sets the language
- (void) setLanguage:(NSString*) language;

//gets the current language
- (NSString*) getLanguage;

//resets this system.
- (void) resetLocalization;

@end
