//
//  Utils.h
//  Alyaoum24
//
//  Created by lotfi on 27/02/2015.
//  Copyright (c) 2015 Alyaoum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "JGProgressHUD.h"
#import "JGProgressHUDFadeZoomAnimation.h"

@interface Utils : NSObject

+ (UIColor *)colorForHex:(NSString *)hexColor;
+ (NSURL *) urlForRessource:(NSString *)relativePath;
+ (NSString *)isEndDateIsSmallerThanCurrent:(NSString *)checkEndDate;
+ (NSString *)isEndDateIsSmallerThanCurrent:(NSString *)checkEndDate dateFormat:(NSString *)dateFormat;

+ (NSString *) dateFormat:(NSString *)dateString;
+ (NSString *) dateFormat:(NSString *)dateString dateFormat:(NSString *)dateFormat ;
+ (JGProgressHUD *)prototypeHUD;
+ (JGProgressHUD *)getHUDText:(NSString *)errorText;
+ (JGProgressHUD *)getHUDIndeterminate;
+ (BOOL)isSmallerThanCurrent:(NSString *)checkEndDate dateFormat:(NSString *)dateFormat;
+ (NSString *)clashFormatDate:(NSString *)checkEndDate :(BOOL) isNotification;
+ (NSURL *) urlForPicture:(NSString *)relativePath;
+(NSString *) getTraduc:(NSString *) tag;


@end
