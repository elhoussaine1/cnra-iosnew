//
//  Utils.m
//  Alyaoum24
//
//  Created by lotfi on 27/02/2015.
//  Copyright (c) 2015 Alyaoum. All rights reserved.
//
#define SERVER_BASE_URL @"http://maplage.aramobile.com/api/"
#define IMAGE_BASE_URL @"http://maplage.aramobile.com/"
#define IS_OS_9_0    ([[[UIDevice currentDevice] systemVersion] floatValue] < 9.0)
#define IS_OS_9_2    ([[[UIDevice currentDevice] systemVersion] floatValue] > 9.3)

#import "Utils.h"
#import "JSON.h"
#import "LocalizationSystem.h"

@implementation Utils

+ (NSURL *) urlForRessource:(NSString *)relativePath {
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", SERVER_BASE_URL, relativePath]];
}
+ (NSURL *) urlForPicture:(NSString *)relativePath {
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", IMAGE_BASE_URL, relativePath]];
}

+ (UIColor *)colorForHex:(NSString *)hexColor {

    hexColor = [[hexColor stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    if ([hexColor length] < 6) return [UIColor blackColor];
    
    if ([hexColor hasPrefix:@"#"]) hexColor = [hexColor substringFromIndex:1];
    
    if ([hexColor length] != 6) return [UIColor blackColor];
    
    NSRange range;
    
    range.length = 2;
    
    range.location = 0;
    
    NSString *rString = [hexColor substringWithRange:range];
    
    range.location = 2;
    
    NSString *gString = [hexColor substringWithRange:range];
    
    range.location = 4;
    
    NSString *bString = [hexColor substringWithRange:range];
    
    unsigned int r, g, b;
    
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f];
    
}


+ (NSString *)formatDate:(NSString *)checkEndDate {
    return [Utils formatDate:checkEndDate dateFormat:@"yyyy-MM-dd HH:mm:ss"];
}

+ (NSString *)formatDate:(NSString *)checkEndDate dateFormat:(NSString *)dateFormat
{
    // checkEndDate = @"2014-01-24 08:58:00";
    // date format
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:usLocale];
    [usLocale release];
    
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:dateFormat];
    NSDate *date = [dateFormatter dateFromString:checkEndDate];
    [dateFormatter release];
    NSTimeInterval seconds = [date timeIntervalSince1970];
    ////
    NSDate * myDate = [NSDate dateWithTimeIntervalSince1970:seconds];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *weekdayComponents =
    [gregorian components: NSMonthCalendarUnit|NSDayCalendarUnit|NSYearCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:myDate];
    [gregorian release];
    int year = [weekdayComponents year];
    int day = [weekdayComponents day];
    int month = [weekdayComponents month];
    //   int hour = [weekdayComponents hour];
    //  int minute = [weekdayComponents minute];
    // test hour minute
    ///:
    
    NSDate* enddate = myDate;
    NSDate* currentdate = [NSDate date];
     NSDate  *newDate = [currentdate dateByAddingTimeInterval:-60*60*24];
    NSTimeInterval distanceBetweenDates = [enddate timeIntervalSinceDate:currentdate];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    
    if (secondsBetweenDates > -107){
        // la date du match
       // NSString *dateString = [NSString stringWithFormat:@"%@ %@ %@", ]
        NSArray *monthArray = [[NSArray alloc]initWithObjects:NSLocalizedString(@"JANVIER", @""),NSLocalizedString(@"FEVRIER", @""),NSLocalizedString(@"MARS", @""),NSLocalizedString(@"AVRIL", @""),NSLocalizedString(@"MAI", @""),NSLocalizedString(@"JUIN", @""),NSLocalizedString(@"JUIELET", @""),NSLocalizedString(@"AOUT", @""),NSLocalizedString(@"SEPTEMBRE", @""),NSLocalizedString(@"OCTOBRE", @""),NSLocalizedString(@"NOVEMBRE", @""),NSLocalizedString(@"DECEMBRE", @""),nil];
        
        NSArray *dayArray = [[NSArray alloc]initWithObjects: NSLocalizedString(@"LUNDI", @""), NSLocalizedString(@"MARDI", @""),NSLocalizedString(@"MERCREDI", @""),NSLocalizedString(@"JEUDI", @""),NSLocalizedString(@"VENDREDI", @""),NSLocalizedString(@"SAMEDI", @""),NSLocalizedString(@"DIMANCHE", @""),nil];
        int jour = [self dayOfWeek:myDate];
            return [NSString stringWithFormat:@" %@ %2d %@ %2d",[dayArray objectAtIndex:jour],day,[monthArray objectAtIndex:month - 1],year];

//        return @"Match en cours";
    } else{
        return @"Match terminé";
    }
    /*   if (secondsBetweenDates > -3)
        return @"Match terminé";
    else
        if (secondsBetweenDates > -10)     //@"moins d une heure";
            return [NSString stringWithFormat:@"مند %ld دقائق",(long)-secondsBetweenDates];
        else
            if (secondsBetweenDates > -60)     //@"moins d une heure";
                return [NSString stringWithFormat:@"مند %ld دقيقة",(long)-secondsBetweenDates];
            else if(secondsBetweenDates > -60)
                return @"منذ ساعة";
            else if(secondsBetweenDates > -60*2)
                return @"منذ ساعتين";
            else if(secondsBetweenDates > -60*24)
                return [NSString stringWithFormat:@"مند %d ساعات",-secondsBetweenDates/60];
            else if(secondsBetweenDates > -60*48)
                return @"مند يوم";
            else if(secondsBetweenDates > -60*72)
                return @"مند يومين";
    //@"moins d une journée";
            else if(secondsBetweenDates > -60*24*10)
                return [NSString stringWithFormat:@"مند %d أيام",-secondsBetweenDates/(60*24)];
            else if(secondsBetweenDates > -60*24*31)
                return [NSString stringWithFormat:@"مند %d يوم",-secondsBetweenDates/(60*24)];
            else if(secondsBetweenDates > -60*24*31*3)
                return @"منذ شهرين";
            else if(secondsBetweenDates > -60*24*31*12)
                return [NSString stringWithFormat:@"مند %d أشهر",-secondsBetweenDates/(60*24*31)];
    
            else
                return [NSString stringWithFormat:@"%2d/%2d/%2d",day,month,year];
   */
    /*  else if (secondsBetweenDates < 0)
     return [NSString stringWithFormat:@" %2d/%2d/%2d // %2d:%2d",day,month,year, hour, minute];
     
     else
     return [NSString stringWithFormat:@"%2d:%2d", hour, minute];*/
}

+(long) dayOfWeek:(NSDate *)anyDate{
    //calculate number of days since sunday reference date
    NSTimeInterval interval = [anyDate timeIntervalSinceReferenceDate]/(60.0*60.0*24.0);
    //mod 7 the number of days to identify day
    long dayix=((long)interval) % 7;
    return dayix;
}
+ (NSString *) dateFormat:(NSString *)dateString{
    return [self dateFormat:dateString dateFormat:@"yyyy-MM-dd HH:mm"];
}

+ (NSString *) dateFormat:(NSString *)dateString dateFormat:(NSString *)dateFormat {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:usLocale];
    [usLocale release];
    
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:dateFormat];
    NSDate *date = [dateFormatter dateFromString:dateString];
    [dateFormatter release];
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"EEE dd MMM yyyy - HH:mm"]; // changed line in your code
    NSLocale *arabeLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"ar_MA"];
    [dateFormatter2 setLocale:arabeLocale];
    [arabeLocale release];
    
    NSString *dateText = [[dateFormatter2 stringFromDate:date] capitalizedString];
    [dateFormatter2 release];
    
    if(dateText == nil)
        return dateString;
    
    return dateText;
}


+ (BOOL)isSmallerThanCurrent:(NSString *)checkEndDate dateFormat:(NSString *)dateFormat
{
    // checkEndDate = @"2014-01-24 08:58:00";
    // date format
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"GTM"];
   // [dateFormatter setLocale:usLocale];
    //[usLocale release];
    
    //[dateFormatter setDateStyle:NSDateFormatterLongStyle];
    //[dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:dateFormat];
    NSDate *date = [dateFormatter dateFromString:checkEndDate];
    [dateFormatter release];
    
    NSTimeInterval seconds = [date timeIntervalSince1970];
    ////
    //////////////////////////
    NSDate* currentdate = [NSDate date];
    //NSTimeInterval first = [currentdate timeIntervalSince1970];
    //NSTimeInterval second = [date timeIntervalSince1970];
    NSInteger secondsDates = [currentdate timeIntervalSinceDate:date];
    if (secondsDates > 6)
        return YES;
    else
        return false;

    //////////////////////////
    
    NSDate * myDate = [NSDate dateWithTimeIntervalSince1970:seconds];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *weekdayComponents =
    [gregorian components: NSMonthCalendarUnit|NSDayCalendarUnit|NSYearCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:myDate];
    [gregorian release];
   // int year = [weekdayComponents year];
   // int day = [weekdayComponents day];
    //int month = [weekdayComponents month];
    //   int hour = [weekdayComponents hour];
    //  int minute = [weekdayComponents minute];
    // test hour minute
    NSString *dateString;
    ///:
    NSDate* enddate = myDate;
     // NSTimeInterval second = [currentdate timeIntervalSince1970];
    ///NSInteger secondsDates = second - seconds;

    NSDate  *newDate = [currentdate dateByAddingTimeInterval:-60*60*24];
    NSTimeInterval distanceBetweenDates = [enddate timeIntervalSinceDate:currentdate];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    
    if (secondsDates > 5)
        return YES;
    else
        return false;
}

#pragma mark lang

+(NSString *)getTraduc:(NSString *) tag {
    return [[LocalizationSystem sharedLocalSystem] localizedStringForKey:tag value: @""];
}

#pragma mark plage date
+ (NSString *)clashFormatDate:(NSString *)checkEndDate:(BOOL) isNotification {
    return [Utils clashOldDate:checkEndDate dateFormat:@"yyyy-MM-dd HH:mm:ss" : isNotification];
}

+ (NSString *)clashOldDate:(NSString *)checkEndDate dateFormat:(NSString *)dateFormat : (BOOL) isNotification
{
    // checkEndDate = @"2014-01-24 08:58:00";
    // date format
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"fr-FR"];
    [dateFormatter setLocale:usLocale];
    [usLocale release];
    
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:dateFormat];
    NSDate *date = [dateFormatter dateFromString:checkEndDate];
    [dateFormatter release];
    NSTimeInterval seconds = [date timeIntervalSince1970];
    ////
    NSDate * myDate = [NSDate dateWithTimeIntervalSince1970:seconds];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *weekdayComponents =
    [gregorian components: NSMonthCalendarUnit|NSDayCalendarUnit|NSYearCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:myDate];
    [gregorian release];
    int year = [weekdayComponents year];
    int day = [weekdayComponents day];
    int month = [weekdayComponents month];
    //   int hour = [weekdayComponents hour];
    //  int minute = [weekdayComponents minute];
    // test hour minute
    NSString *dateString;
    ///:
    NSDate* enddate = myDate;
    NSDate* currentdate = [NSDate date];
    // NSDate  *newDate = [currentdate dateByAddingTimeInterval:-60*60*24];
    NSTimeInterval distanceBetweenDates = [enddate timeIntervalSinceDate:currentdate];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    NSString *ilYa = NSLocalizedString(@"ILYA", @"");
    
    if (isNotification) {
        if (secondsBetweenDates > -61)     //@"moins d une heure";
            return [NSString stringWithFormat:@"%@ %ld %@",ilYa,(secondsBetweenDates),NSLocalizedString(@"MINUTE", @"")];
        else if(secondsBetweenDates > -60*24)
            return [NSString stringWithFormat:@"%@ %ld %@",ilYa, (-1 * secondsBetweenDates)/60,NSLocalizedString(@"HEUR", @"")];
        else {// la date abrege
            NSArray *monthArray = [[NSArray alloc]initWithObjects:NSLocalizedString(@"JANVIER", @""),NSLocalizedString(@"FEVRIER", @""),NSLocalizedString(@"MARS", @""),NSLocalizedString(@"AVRIL", @""),NSLocalizedString(@"MAI", @""),NSLocalizedString(@"JUIN", @""),NSLocalizedString(@"JUIELET", @""),NSLocalizedString(@"AOUT", @""),NSLocalizedString(@"SEPTEMBRE", @""),NSLocalizedString(@"OCTOBRE", @""),NSLocalizedString(@"NOVEMBRE", @""),NSLocalizedString(@"DECEMBRE", @""),nil];
            
            return [NSString stringWithFormat:@"%2d.%2@.%2d",day,[monthArray objectAtIndex:month - 1],year];
    }
    }
        if (secondsBetweenDates > -61)     //@"moins d une heure";
            return [NSString stringWithFormat:@"%@ %ld %@",ilYa,(secondsBetweenDates),NSLocalizedString(@"MINUTE", @"")];
            else if(secondsBetweenDates > -60*24)
                return [NSString stringWithFormat:@"%@ %ld %@",ilYa, (-1 * secondsBetweenDates)/60,NSLocalizedString(@"HEUR", @"")];
            else if(secondsBetweenDates > -60*48)
                return NSLocalizedString(@"JOUR", @"");
            else if(secondsBetweenDates > -60*24*31)
                return [NSString stringWithFormat:@"%@ %ld %@",ilYa, (-1 * secondsBetweenDates)/(60*24),NSLocalizedString(@"JOURS", @"")];
            else {// la date abrege
            NSArray *monthArray = [[NSArray alloc]initWithObjects:NSLocalizedString(@"JANVIER", @""),NSLocalizedString(@"FEVRIER", @""),NSLocalizedString(@"MARS", @""),NSLocalizedString(@"AVRIL", @""),NSLocalizedString(@"MAI", @""),NSLocalizedString(@"JUIN", @""),NSLocalizedString(@"JUIELET", @""),NSLocalizedString(@"AOUT", @""),NSLocalizedString(@"SEPTEMBRE", @""),NSLocalizedString(@"OCTOBRE", @""),NSLocalizedString(@"NOVEMBRE", @""),NSLocalizedString(@"DECEMBRE", @""),nil];

                return [NSString stringWithFormat:@"%2d.%2@.%2d",day,[monthArray objectAtIndex:month - 1],year];
            }
    /*  else if (secondsBetweenDates < 0)
     return [NSString stringWithFormat:@" %2d/%2d/%2d // %2d:%2d",day,month,year, hour, minute];
     
     else
     return [NSString stringWithFormat:@"%2d:%2d", hour, minute];*/
}
+ (JGProgressHUD *)getHUDText:(NSString *)errorText {
    
    JGProgressHUD *HUD = [[JGProgressHUD alloc] initWithStyle:JGProgressHUDStyleExtraLight];
    HUD.interactionType = JGProgressHUDInteractionTypeBlockTouchesOnHUDView;
    JGProgressHUDFadeZoomAnimation *an = [JGProgressHUDFadeZoomAnimation animation];
    HUD.animation = an;
    HUD.HUDView.layer.shadowColor = [UIColor blueColor].CGColor;
    HUD.HUDView.layer.shadowOffset = CGSizeZero;
    HUD.HUDView.layer.shadowOpacity = 0.4f;
    HUD.HUDView.layer.shadowRadius = 1.0f;
    
    HUD.indicatorView = nil;
    
    HUD.textLabel.text = errorText;
    HUD.position = JGProgressHUDPositionBottomCenter;
    HUD.marginInsets = (UIEdgeInsets) {
        .top = 0.0f,
        .bottom = 20.0f,
        .left = 0.0f,
        .right = 0.0f,
    };
    return HUD;
}

 + (JGProgressHUD *) getHUDIndeterminate{
    JGProgressHUD *HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
     HUD.interactionType = JGProgressHUDInteractionTypeBlockTouchesOnHUDView;
     HUD.textLabel.text = @"Chargement";
 
    return HUD;
}
@end
