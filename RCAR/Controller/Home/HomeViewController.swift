//
//  HomePagerViewController.swift
//  ClashFoot
//
//  Created by lotfi on 26/07/2016.
//  Copyright © 2016 Aramobile. All rights reserved.
//

extension Config {
    public static let cgdConfiguration = Config(
        headerColor: "E7F3E9",
        botBubbleColor: "#F7F7F7",
        botBubbleTextColor: "#1F1F24",
        userBubbleColor: "#4D8231",
        userBubbleTextColor: "#FFFFFF"
    )
}

let successResponse: (String) -> Void = {
    print($0)
}

let failureResponse: (String) -> Void = {
    print($0)
}

func certifcateData(_ json: [String: Any]) -> URLRequestParams {
    var data = URLRequestParams()
    
    let date = Date()
    let Yearformatter = DateFormatter()
    Yearformatter.dateFormat = "yy"
    let setAnnee = Yearformatter.string(from: date)
    let Dateformatter = DateFormatter()
    Dateformatter.dateFormat = "dd/MM/yyyy"
    
    
    let mail = json["mail"] as? String ?? ""
    let adresse = json["adresse"] as? String ?? ""
    let idTypeAttestation = json["idTypeAttestation"] as? String ?? ""
    
    let setDateDemande = Dateformatter.string(from: date)
    data["nomDemandeur"] = Profil.currentProfile().name.trimmingCharacters(in: .whitespaces)
    data["mail"] = mail
    data["adresse"] = adresse
    data["idTypeAttestation"] = idTypeAttestation
    data["profil"] = "REC"
    data["typeDemande"] = "D"
    data["numeroClient"] = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
    data["produit"] = "RCAR"
    data["canal"] = "MOBILE"
    if(UserDefaults.standard.object(forKey: "NextSeq") != nil){
        let setSequence = UserDefaults.standard.object(forKey: "NextSeq")
        data["sequence"] = setSequence
    }
    data["annee"] = setAnnee

    let DateDemandeformatter = DateFormatter()
    DateDemandeformatter.dateFormat = "yyyy"

    let numeroDemande = "rcar\(DateDemandeformatter.string(from: date))web\(randomString(length: 10))"
    return data
}


func claimData(_ json: [String: Any]) -> URLRequestParams {
    
    
    let mail = json["mail"] as? String ?? ""
    let adresse = json["adresse"] as? String ?? ""
    let ville = json["ville"] as? String ?? ""
    let pays = json["pays"] as? String ?? ""
    let telephone = json["telephone"] as? String ?? ""
    let gsm = json["gsm"] as? String ?? ""
    let reclamation = json["reclamation"] as? String ?? ""
    
    
    var data = URLRequestParams()
    data["nom"] = Profil.currentProfile().name
    data["mail"] = mail
    data["adresse"] = adresse
    data["ville"] = ville
    data["pays"] = pays
    data["telephone"] = telephone
    data["gsm"] = gsm
    data["identifiant"] = Profil.currentProfile().identifiant
    data["reclamation"] = reclamation
    data["profil"] = Profil.currentProfile().profiltype
    data["produit"] = "RCAR"
    data["canal"] = "MOBILE"
    return data
}


func appointementData(_ json: [String: Any]) -> URLRequestParams {
    var data = URLRequestParams()
        
    let adresseMail = json["adresseMail"] as? String ?? ""
    let objet = json["objet"] as? String ?? ""
    let tel = json["tel"] as? String ?? ""
    let fax = json["fax"] as? String ?? ""
    let heure = json["heure"] as? String ?? ""
    let jour = json["jour"] as? String ?? ""
    
    data["adresseMail"] = adresseMail
    data["objet"] = objet
    data["tel"] = tel
    data["fax"] = fax
    data["nomPrenom"] = Profil.currentProfile().name
    data["heure"] = heure
    data["jour"] = jour
    data["identifiant"] = Profil.currentProfile().identifiant
    data["profil"] = Profil.currentProfile().profiltype

    return data
}

import UIKit
import MessageKit

class HomeViewController: BaseViewController  , UINavigationControllerDelegate , UICollectionViewDelegate , UICollectionViewDataSource , MosaicLayoutDelegate {
    
    // MARK: Properties
    var Homeheadings = [UIViewController]()
    var StartAppointmentViewController : UIViewController = AppointmentViewController()
    var elementsArray :NSMutableArray! = NSMutableArray()

    // Mark: - Outlets
    @IBOutlet weak var HelpSupportContainer: UIView!
    @IBOutlet var homeCollectionView  : UICollectionView!
    
    // Mark: - Actions
    @IBAction func HelpSupportButtonACtion(_ sender: UIButton) {
        
        let profile = Profil.currentProfile()
                
                let config: Config  = .cgdConfiguration
                
                Current = .live(
                    config: config,
                    user: User(
                        id: profile.identifiant,
                        name: profile.name,
                        type: profile.profiltype,
                        token: profile.access_token
                    ),
                    simulation: AffiliatedApiClient.simulation(identifiant:dateEffet:success:failure:)
                )
                
                let vc = ChatViewController()
                vc.actions =  { action in
                    switch action {
                    case .endActivity:
                        DispatchQueue
                            .main
                            .asyncAfter(deadline: .now() + 4.0) {
                                vc.dismiss(
                                    animated: true,
                                    completion: nil
                                )
                            }
                    case let .sendAppointement(responseData):
                        guard let object = jsonObject(responseData) else { return }
                        AppointmentApiClient.addAppointment(
                            identifiant: profile.identifiant,
                            appointementData(object),
                            success: successResponse,
                            failure: failureResponse
                        )
                    case let .requestCertificate(responseData):
                        guard let object = jsonObject(responseData) else { return }
                        CertificationApiClient.addCertification(
                            certifcateData(object),
                            success: successResponse,
                            failure: failureResponse
                        )
                    case let .sendClaim(responseData):
                        guard let object = jsonObject(responseData) else { return }
                        ComplaintApiClient.addComplaint(
                            claimData(object),
                            completionHandler: successResponse,
                            failure: failureResponse)
                    case .simulation:
                        break
                    }
                }
                vc.modalPresentationStyle = .fullScreen
                self.showDetailViewController(vc, sender: nil)

    }
    
    init() {
        super.init(nibName: "HomeViewController", bundle: nil)
    }
    required   init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let currentCellView = "HomeCollectionViewCell"
        let nib                         = UINib(nibName: currentCellView, bundle: nil)
        homeCollectionView.register(nib, forCellWithReuseIdentifier: currentCellView)
        (homeCollectionView.collectionViewLayout as? MosaicLayout)?.delegate = self
        HelpSupportContainer.layer.cornerRadius = 28
        HelpSupportContainer.clipsToBounds = true
        HelpSupportContainer.dropShadow(color: .gray, opacity: 0.8, offSet: CGSize(width: 0.0, height: 0.0), radius: 25, scale: true)
        
        if (Profil.isLoggedIn() == true && Profil.currentProfile().profiltype.trimmingCharacters(in: .whitespaces) != "AFFILIE") {
            if(Profil.currentProfile().facePrintMenuActive == false){

                DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                    let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
                    let currentViewController = FacialRecognitionPopUpViewController()
                    currentViewController.backgroundIImage = self.takeScreenshot()
                    currentViewController.modalPresentationStyle = .fullScreen
                    NavigationController.present(currentViewController, animated: false)
                }

            }else{
                if(Profil.currentProfile().pieceReclame == "CVI"){
                    if(UserDefaults.standard.bool(forKey: "ClCheckBox")){
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                            let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
                            let currentViewController = OnlineLifeCertificatePopupViewController()
                            currentViewController.backgroundIImage = self.takeScreenshot()
                            currentViewController.modalPresentationStyle = .fullScreen
                            NavigationController.present(currentViewController, animated: false)
                        }
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
         self.navigationItem.setHidesBackButton(true, animated:true);
        
        if Profil.isLoggedIn() == false {
            let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
            let curentController  = PagerLoginViewController() as PagerLoginViewController
            NavigationController.pushViewController(curentController, animated: true)
            
        }else{
            self.getHomeElement()
            self.existAppointment()
            self.homeCollectionView.reloadData()
            if(Profil.currentProfile().profiltype.trimmingCharacters(in: .whitespaces) != "AFFILIE"){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    var image :UIImage?
                    let currentLayer = UIApplication.shared.keyWindow!.layer
                    let currentScale = UIScreen.main.scale
                    UIGraphicsBeginImageContextWithOptions(currentLayer.frame.size, false, currentScale);
                    guard let currentContext = UIGraphicsGetCurrentContext() else {return}
                    currentLayer.render(in: currentContext)
                    image = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                }
            }
        }
    }
    
    //MARK: GET home element
    func getHomeElement(){
        
        //refresh elementsArray
        elementsArray.removeAllObjects()
        Homeheadings.removeAll()
        if(Profil.currentProfile().profiltype.trimmingCharacters(in: .whitespaces) == "AFFILIE"){
            
            self.elementsArray.add("UpdateSituation")
            self.elementsArray.add("Appointment")
            self.elementsArray.add("AccountSituation")
            self.elementsArray.add("Simulator")
            self.elementsArray.add("Complaint")
            self.elementsArray.add("FollowUpComp")
            self.elementsArray.add("FollowUpFolder")
            
            Homeheadings.append(UpdateSituationViewController())
            Homeheadings.append(StartAppointmentViewController)
            Homeheadings.append(SituationViewController())
            Homeheadings.append(SimulatorViewController())
            Homeheadings.append(ComplaintViewController())
            Homeheadings.append(FollowUpComViewController())
            Homeheadings.append(FollowUpFolderViewController())
            
        }else{
            self.elementsArray.add("UpdateSituation")
            self.elementsArray.add("Appointment")
            self.elementsArray.add("PaymentSituation")
            self.elementsArray.add("CertificateRequest")
            self.elementsArray.add("Complaint")
            self.elementsArray.add("FollowUpComp")
            self.elementsArray.add("FollowUpFolder")
            self.elementsArray.add("CertificateLife")
        
            Homeheadings.append(UpdateSituationViewController())
            Homeheadings.append(StartAppointmentViewController)
            Homeheadings.append(paymentSituationViewController())
            Homeheadings.append(CertificationViewController())
            Homeheadings.append(ComplaintViewController())
            Homeheadings.append(FollowUpComViewController())
            Homeheadings.append(FollowUpFolderViewController())
            Homeheadings.append(OnlineLifeCertificateViewController())
        }
        
    }
    // MARK: UIcollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.elementsArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let actu  = self.elementsArray.object(at: indexPath.row) as! NSString
        let cellIdentifier  = "HomeCollectionViewCell"
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? HomeCollectionViewCell
        cell!.contentCellView.layer.cornerRadius    = 2
        cell!.contentCellView.layer.masksToBounds   = true
        
        cell?.titleCell.text                        = Utils.getTraduc(actu as String?) as String
        let icon = (actu as String) + ".png"
        cell?.iconCell.image = UIImage(named: icon)
        
        if(Profil.currentProfile().profiltype.trimmingCharacters(in: .whitespaces) != "AFFILIE"){
        if(Profil.currentProfile().facePrintMenuActive == true && Profil.currentProfile().pieceReclame != "CVI" && indexPath.row == self.elementsArray.count-1){
            cell?.iconCell.image = cell?.iconCell.image?.withRenderingMode(.alwaysTemplate)
            cell?.iconCell.tintColor = UIColor.gray
        }}
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if Reachability.isConnectedToNetwork(){
            let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
            
            if(Profil.currentProfile().profiltype.trimmingCharacters(in: .whitespaces) != "AFFILIE" && indexPath.row == self.elementsArray.count-1 ){
                
                if(Profil.currentProfile().facePrintMenuActive == false){

                    NavigationController.pushViewController(ActivationChoiceViewController(), animated: true)

                }else{

                    if(Profil.currentProfile().pieceReclame == "CVI"){
                        if(self.getSavedImage() != nil){
                            NavigationController.pushViewController(Homeheadings[indexPath.row], animated: true)
                        }else{
                            NavigationController.pushViewController(ActivationChoiceViewController(), animated: true)
                        }
                    }else{
                        
                        self.view.makeToast("Le service de reconnaissance faciale est activé. Aucun contrôle de vie n'est demandé pour votre compte", duration: 2.0, position: .center)
                    }
                }
            
            }else{
                NavigationController.pushViewController(Homeheadings[indexPath.row], animated: true)
            }
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
        
    }
    // MARK: MosaicCollectionView
    func collectionView(_ collectionView: UICollectionView, relativeHeightForItemAt indexPath: IndexPath) -> Float {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, isDoubleColumnAt indexPath: IndexPath) -> Bool {
        return false
    }
    public func numberOfColumns(in collectionView: UICollectionView!) -> UInt {
        //  Set the quantity of columns according of the device and interface orientation
        return 2
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

private extension HomeViewController {
    
    func existAppointment() {
            let loader = Utils.getHUDIndeterminate()
            loader?.show(in: appDelegate.window)
            
            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
            AppointmentApiClient.existAppointment(identifiant: identifiant, success: { appointment in
                DispatchQueue.main.async {
                    self.StartAppointmentViewController = UIViewController ()
                    if(appointment.adresseMail != ""){
                        let appointment :Appointment!    = appointment
                        Appointment.currentAppointment().remove()
                        appointment.save()
                        self.StartAppointmentViewController =  UpdateCancelAppointmentViewController()
                    }else{
                        self.StartAppointmentViewController =  AppointmentViewController()
                    }
                    self.getHomeElement()
                    self.homeCollectionView.reloadData()
                    loader?.dismiss()
                }
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    loader?.dismiss()
                }
            })
    }
}
