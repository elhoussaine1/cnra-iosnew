//
//  NewVersionViewController.swift
//  RCAR
//
//  Created by El houssaine EL gamouz on 01/07/2019.
//  Copyright © 2019 Aramobile. All rights reserved.
//

class NewVersionViewController: UIViewController {
    
    var version_message:String!
    var screenshot:UIImage!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var popMessage: UITextView!
    
    @IBAction func openNewApp(_ sender: UIButton) {

        if let url = URL(string: "itms-apps://itunes.apple.com/app/id654689522"),
            UIApplication.shared.canOpenURL(url){
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func cancelButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.containerView.layer.cornerRadius = 2
        self.containerView.addshadowColor(shadowColor: UIColor.black)
        
        if(screenshot != nil){
            view.backgroundColor = UIColor(patternImage: screenshot)
        }
        popMessage.text = version_message as String
    }

}
