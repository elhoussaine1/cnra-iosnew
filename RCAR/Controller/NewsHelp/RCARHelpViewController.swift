//
//  RCARHelpViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 10/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftSoup

class RCARHelpViewController: BaseViewController {
    
    // MARK: Properties
    var titleArray = [String]()
    var bodyArray = [String]()
    var RCARHelp = String()
    // Mark: - Outlets
    @IBOutlet weak var RCARHelpExpyTableView: ExpyTableView!
    // Mark: - Actions
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RCARHelpExpyTableView.register(UINib(nibName: "RCarTitleTableViewCell", bundle: nil), forCellReuseIdentifier: "RCarTitleTableViewCell")
        RCARHelpExpyTableView.register(UINib(nibName: "detailTableViewCell", bundle: nil), forCellReuseIdentifier: "detailTableViewCell")
        
        loadContentView()
        loadHtml()
    }
    
    func loadHtml(){
        
        do {
            let myHTMLString = try String(contentsOf: URL(string: RCARHelp)!, encoding: .utf8)
            let doc: Document = try SwiftSoup.parse(myHTMLString)
            let titlecount = try doc.select("h6").select("span").array().count
            
            for i in 0 ..<  titlecount {
                titleArray.append(try doc.select("h6").select("span").get(i).text())
                bodyArray.append(try doc.getElementsByClass("card-body").get(i).html())
            }
            self.RCARHelpExpyTableView.reloadData()
            
        } catch {
            print("")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadContentView() {
        
        RCARHelpExpyTableView.dataSource = self
        RCARHelpExpyTableView.delegate = self
        
        RCARHelpExpyTableView.rowHeight = UITableView.automaticDimension
        RCARHelpExpyTableView.estimatedRowHeight = 54
        RCARHelpExpyTableView.tableFooterView = UIView()
        
        //Delete lines between UITableViewCells
        RCARHelpExpyTableView.separatorStyle = .none
    }
    
    
    
}

//MARK: ExpyTableViewDataSourceMethods
extension RCARHelpViewController: ExpyTableViewDataSource {
    
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: RCarTitleTableViewCell.self)) as! RCarTitleTableViewCell
        cell.selectionStyle = .none
        cell.title.text = titleArray[section]
        cell.layoutMargins = UIEdgeInsets.zero
        
        return cell
    }
}


//MARK: ExpyTableView delegate methods
extension RCARHelpViewController: ExpyTableViewDelegate {
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
    }
}



extension RCARHelpViewController {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK: UITableView Data Source Methods
extension RCARHelpViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: detailTableViewCell.self)) as! detailTableViewCell
            cell.layoutMargins = UIEdgeInsets.zero

            let modifiedFont = String(format:"<span style=\"font-family: '-apple-system', 'RobotoCondensed-Regular'; font-size: 12 \">%@</span>", bodyArray[indexPath.section])
            
            cell.detailUILabel.attributedText = try? NSAttributedString(data: modifiedFont.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            return cell
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ExpandedCell.self)) as! ExpandedCell
            cell.layoutMargins = UIEdgeInsets.zero

            return cell
        }
    }
    
    
    
  
}

