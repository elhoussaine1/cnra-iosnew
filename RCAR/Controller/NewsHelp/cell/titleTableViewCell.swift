//
//  titleTableViewCell.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 07/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class titleTableViewCell: UITableViewCell {

   
    @IBOutlet weak var newsItemImage: UIImageView!
    @IBOutlet weak var titleUILabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
