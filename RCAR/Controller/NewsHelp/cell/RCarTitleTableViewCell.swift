//
//  RCarTitleTableViewCell.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 02/07/2019.
//  Copyright © 2019 Aramobile. All rights reserved.
//

import UIKit

class RCarTitleTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var title: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
