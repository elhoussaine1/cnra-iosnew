//
//  newsViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 07/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class newsViewController: BaseViewController {


    // MARK: Properties
    var NewsArray = [News]()
    // Mark: - Outlets
    @IBOutlet weak var emptyResult: UILabel!

    @IBOutlet weak var newsExpyTableView: UITableView!
    // Mark: - Actions
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        
        newsExpyTableView.register(UINib(nibName: "titleTableViewCell", bundle: nil), forCellReuseIdentifier: "titleTableViewCell")
//        newsExpyTableView.register(UINib(nibName: "detailTableViewCell", bundle: nil), forCellReuseIdentifier: "detailTableViewCell")
         loadContentView()
         getNews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func requestData() -> URLRequestParams {
        var data = URLRequestParams()
        return data
    }
    
    func loadContentView() {
        
        newsExpyTableView.dataSource = self
        newsExpyTableView.delegate = self
        
        newsExpyTableView.rowHeight = UITableView.automaticDimension
        newsExpyTableView.estimatedRowHeight = 270
        
        //Alter the animations as you want
        // followUpFolderUITableView.expandingAnimation = .fade
        //followUpFolderUITableView.collapsingAnimation = .fade
        
        //newsExpyTableView.tableFooterView = UIView()
        
        //Delete lines between UITableViewCells
        newsExpyTableView.separatorStyle = .none
        emptyResult.isHidden = true
    }
}
private extension newsViewController {
    
    func getNews() {
        if Reachability.isConnectedToNetwork(){
         
            loader?.show(in: appDelegate.window)
            let identifiant = Profil.currentProfile().identifiant
            NewsApiClient.getNews(identifiant: "1882928",success: { newsArray in
                DispatchQueue.main.async {

                    if (newsArray.count == 0){
                        self.emptyResult.isHidden = false
                    }else{
                        self.emptyResult.isHidden = true
                        self.NewsArray = newsArray
                    }
                     self.newsExpyTableView.reloadData()
                    self.loader?.dismiss()
                }
                
            }, failure: { apiError in
                
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                }
                
                //self.showApiErrorAlert(apiError)
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
}

//MARK: ExpyTableViewDataSourceMethods
extension newsViewController:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: titleTableViewCell.self)) as! titleTableViewCell
        cell.selectionStyle = .none
        cell.titleUILabel.text = NewsArray[indexPath.row].titre
        
        let Imageurl = "https://www.rcar.ma\(NewsArray[indexPath.row].image.replacingOccurrences(of: "\\", with: "/") as String)"
        cell.newsItemImage.sd_setImage(with: NSURL(string: Imageurl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!) as URL?, placeholderImage: UIImage(named: "default_image.png"))
        
        cell.layoutMargins = UIEdgeInsets.zero
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
        let controller : newsDetailsViewController = newsDetailsViewController()
        controller.NewsItem = NewsArray[indexPath.row]
        NavigationController.pushViewController(controller, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NewsArray.count
    }
    
    
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        //return 270.0;//Choose your custom row height
        return UITableView.automaticDimension
    }
}








