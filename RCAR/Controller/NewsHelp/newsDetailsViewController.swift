//
//  newsDetailsViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 24/01/2019.
//  Copyright © 2019 Aramobile. All rights reserved.
//

import UIKit

class newsDetailsViewController: BaseViewController {

    @IBOutlet weak var descNewsdetails: UILabel!
    @IBOutlet weak var imageNewsdetails: UIImageView!
    @IBOutlet weak var titleNewsDetails: UILabel!
 

    var NewsItem:News!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        titleNewsDetails.text = NewsItem.titre
       
        let modifiedFont = String(format:"<span style=\"font-family: '-apple-system', 'RobotoCondensed-Regular'; font-size: 12 \">%@</span>", NewsItem.detail)
        
        descNewsdetails.attributedText = try? NSAttributedString(data: modifiedFont.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)

        let Imageurl = "https://www.rcar.ma\(NewsItem.image.replacingOccurrences(of: "\\", with: "/") as String)"

        imageNewsdetails.sd_setImage(with: NSURL(string: Imageurl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!) as URL?, placeholderImage: UIImage(named: "default_image.png"))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
