//
//  AppointmentPopUp.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 14/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol SideSelectionDelegate {
    func SelectedDateTime(time:String,date:String)
}
class AppointmentPopUp: BaseViewController ,EPCalendarPickerDelegate , UINavigationControllerDelegate , UICollectionViewDelegate , UICollectionViewDataSource , MosaicLayoutDelegate{
    
    let hasWeaponOfMassDescrtruction : Bool = true
    var selectionDelegate: SideSelectionDelegate!
    var elementsArray = [String]()
    var  resultDate :String = "00/00/0000"
    var resultTime :String = "00:00"
    
    @IBOutlet weak var selectdateUIButton: UIButton!
    @IBOutlet weak var selecthourUICollectionView: UICollectionView!
    
    @IBAction func cancelUIButton(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func submitUIButton(_ sender: Any) {
       // storeSelectedDateTime(time:resultTime,date:resultDate)
        
        selectionDelegate.SelectedDateTime(time:resultTime,date:resultDate)
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var messageUILabel: UILabel!
    @IBAction func selectdateUIButton(_ sender: Any) {
        
        
        let date = Date()
        let calendarPicker = EPCalendarPicker(startYear: date.year(), endYear: date.year(), multiSelection: false, selectedDates: [])
        
        calendarPicker.calendarDelegate = self
        calendarPicker.startDate = Date()
        calendarPicker.hightlightsToday = true
        calendarPicker.showsTodaysButton = true
        calendarPicker.hideDaysFromOtherMonth = true
        calendarPicker.tintColor = Utils.color(forHex: "000000")
        calendarPicker.weekdayTintColor = Utils.color(forHex: "FFFFFF")
        calendarPicker.dateSelectionColor = Utils.color(forHex: "5F8D38")
        calendarPicker.todayTintColor = Utils.color(forHex: "5F8D38")
        calendarPicker.dayDisabledTintColor = UIColor.gray
        calendarPicker.title = "Séléctionner une date"
        calendarPicker.backgroundColor = Utils.color(forHex: "424A50")
        let navigationController = UINavigationController(rootViewController: calendarPicker)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    
    init() {
        super.init(nibName: "AppointmentPopUp", bundle: nil)
    }
    required   init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let currentCellView = "selectHourCollectionViewCell"
        let nib                         = UINib(nibName: currentCellView, bundle: nil)
        selecthourUICollectionView.register(nib, forCellWithReuseIdentifier: currentCellView)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let defaultdate = formatter.string(from: Date())
        selectdateUIButton.setTitle(defaultdate, for: .normal)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UIcollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return elementsArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellIdentifier  = "selectHourCollectionViewCell"
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? selectHourCollectionViewCell
        
        cell?.hourUILabel.text = self.elementsArray[indexPath.row] as String
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        resultTime = self.elementsArray[indexPath.row]
    }
    
    // MARK: MosaicCollectionView
    func collectionView(_ collectionView: UICollectionView, relativeHeightForItemAt indexPath: IndexPath) -> Float {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, isDoubleColumnAt indexPath: IndexPath) -> Bool {
        return false
    }
    public func numberOfColumns(in collectionView: UICollectionView!) -> UInt {
        
        return 2
    }
    
    func epCalendarPicker(_: EPCalendarPicker, didCancel error : NSError) {
        
    }
    func epCalendarPicker(_: EPCalendarPicker, didSelectDate date : Date) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        resultDate = formatter.string(from: date)
        
     
        selectdateUIButton.setTitle(resultDate, for: .normal)
        
        var  type = "AF"
        
        if(Profil.currentProfile().profiltype.trimmingCharacters(in: .whitespaces) == "AFFILIE"){
            type = "AF"
        }else{
            type = "AD"
        }
        
        if Reachability.isConnectedToNetwork(){
         
            loader?.show(in: appDelegate.window)
            
            AppointmentApiClient.HoursList(date: resultDate,type: type, success: { data in
                DispatchQueue.main.async {
                    
                    self.elementsArray.removeAll()
                    
                    let json = JSON(data: data)
                    
                    if(json["code"].stringValue == "OK"){
                        self.messageUILabel.isHidden = true
                        let hourlist = json["heures"].arrayValue
                        
                        for i in 0..<hourlist.count {
                            self.elementsArray.append("\(hourlist[i])")
                        }
                        
                        self.selecthourUICollectionView.reloadData()
                        self.loader?.dismiss()
                    }else{
                        self.messageUILabel.isHidden = false
                        let message = json["message"].stringValue
                        self.messageUILabel.text = message
                        self.loader?.dismiss()
                    }
                    
                }
                
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                }
            })
            
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
        
    }
    func epCalendarPicker(_: EPCalendarPicker, didSelectMultipleDate dates : [Date]) {
        // txtViewDetail.text = "User selected dates: \n\(dates)"

        
    }
    
    
}



