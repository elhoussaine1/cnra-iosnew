//
//  UpdateCancelAppointmentViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 13/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class UpdateCancelAppointmentViewController: BaseViewController {
    
    var index               : Int               = 0

    @IBOutlet weak var AppointmentMessageUILabel: UILabel!
  
    @IBOutlet weak var containerUIView: UIView!
    
    @IBAction func cancelUIButton(_ sender: Any) {
        cancelAppointment()
        Appointment.currentAppointment().remove()
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
       
    }
    
    
    @IBAction func updateUIButton(_ sender: Any) {
        cancelAppointment()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerUIView.layer.cornerRadius = 5
        containerUIView.addshadowColor(shadowColor: UIColor.black)  
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(Appointment.existAppointment()){
            
            
            let day = Appointment.currentAppointment().jour as String
            let hour = Appointment.currentAppointment().heure as String
            
            let dateFormatter = DateFormatter()
            let dateFormat = "dd/MM/yyyy"
            
            dateFormatter.dateFormat = dateFormat
            let getDate = dateFormatter.date(from: day)
            dateFormatter.dateFormat = "EEEE, d MMMM"
            let stringDate = dateFormatter.string(from: getDate!)
            AppointmentMessageUILabel.text = "\(stringDate.capitalized) à \(hour)"
            
        }
        
    
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func requestData() -> URLRequestParams {
        var data = URLRequestParams()
        
        
        if(Appointment.existAppointment()){
       
        let adresseMail = Appointment.currentAppointment().adresseMail as String
        let objet = Appointment.currentAppointment().objet as String
        let tel = Appointment.currentAppointment().tel as String
        let fax = Appointment.currentAppointment().fax as String
        let nomPrenom = Appointment.currentAppointment().nomPrenom as String
        let heure = Appointment.currentAppointment().heure as String
        let jour = Appointment.currentAppointment().jour as String
        let identifiant = Profil.currentProfile().identifiant as String
        let profil = Profil.currentProfile().profiltype as String
   
        data["adresseMail"] = adresseMail
        data["objet"] = objet
        data["tel"] = tel
        data["fax"] = fax
        data["nomPrenom"] = nomPrenom
        data["heure"] = heure
        data["jour"] = jour
        data["identifiant"] = identifiant
        data["profil"] = profil
        data["jourEx"] = jour
            
        }
        
        return data
    }
}
private extension UpdateCancelAppointmentViewController {

    func cancelAppointment() {
 
        if Reachability.isConnectedToNetwork(){
            
            loader?.show(in: appDelegate.window)
            
            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
            AppointmentApiClient.cancelAppointment(identifiant : identifiant,requestData(), success: {  success in
                
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                    self.view.makeToast("Votre rendez vous est annulé", duration: 1.0, position: .center)
                      let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
                    let curentController  = AppointmentViewController() as AppointmentViewController
                    NavigationController.pushViewController(curentController, animated: true)
                }
              
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                }
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
 
    }
    
}
