//
//  AppointmentViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 06/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit


class AppointmentViewController: BaseViewController, UITextFieldDelegate,UITextViewDelegate  {

    // MARK: Properties
   
    var index               : Int               = 0
    var cancelAppointment : Bool = true
    // Mark: - Outlets
    @IBOutlet weak var appointmentDateTextfield: Textfield!
    @IBOutlet weak var emailTextfield: Textfield!
    @IBOutlet weak var phoneTextfield: Textfield!
    @IBOutlet weak var faxTextfield: Textfield!
    @IBOutlet weak var appointmentObjectUITextView: UITextView!
    @IBOutlet weak var fullnameUILabel: UILabel!
    
    
    // Mark: - Actions
    @IBAction func submitUIButton(_ sender: Any) {
        addAppointment()
    }
    
    init() {
        super.init(nibName: "AppointmentViewController", bundle: nil)
    }
    required   init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadContentView()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
 
        appointmentObjectUITextView.text = "Objet de la visite"
        appointmentObjectUITextView.textColor = UIColor.lightGray
        
        if(Appointment.existAppointment()){
            
           
            
            let gethour =   Appointment.currentAppointment().heure as String
            let getday =   Appointment.currentAppointment().jour as String
           
            let email = Appointment.currentAppointment().adresseMail as String
            let phone = Appointment.currentAppointment().tel as String
            let fax = Appointment.currentAppointment().fax as String
            let appointmentObject = Appointment.currentAppointment().objet as String
            
            self.appointmentDateTextfield.txtField.text = "\(getday) \(gethour)"
            self.emailTextfield.txtField.text = email
            self.phoneTextfield.txtField.text = phone
            self.faxTextfield.txtField.text = fax
            self.appointmentObjectUITextView.text = appointmentObject
            
        }


    
     
       
    }
    
    
    func requestData() -> URLRequestParams {
        var data = URLRequestParams()

        let fullDate    = appointmentDateTextfield.txtField.text
        let fullDateArr = fullDate?.components(separatedBy: " ")
        
        let selectedDate    = fullDateArr![0]
        let selectedHour = fullDateArr![1]
        
        data["adresseMail"] = emailTextfield.txtField.text
        data["objet"] = appointmentObjectUITextView.text
        data["tel"] = phoneTextfield.txtField.text
        data["fax"] = faxTextfield.txtField.text
        data["nomPrenom"] = Profil.currentProfile().name
        data["heure"] = selectedHour
        data["jour"] = selectedDate
        data["identifiant"] = Profil.currentProfile().identifiant
        data["profil"] = Profil.currentProfile().profiltype

        return data
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadContentView() {
        
        self.fullnameUILabel.text = Profil.currentProfile().name as String
        self.emailTextfield.txtField.delegate    = self
        self.phoneTextfield.txtField.delegate        = self
        self.faxTextfield.txtField.delegate       = self
        self.appointmentDateTextfield.txtField.delegate     = self
        self.appointmentObjectUITextView.delegate = self
        
        self.emailTextfield.lbldiscription.text         = "Email@exemple.com"
        self.phoneTextfield.lbldiscription.text       = "Numéro de téléphone *"
        self.faxTextfield.lbldiscription.text           = "Numéro du Fax"
        self.appointmentDateTextfield.lbldiscription.text      = "Date et heure du rendez-vous *"
        self.appointmentDateTextfield.rightIconHidding(false,iconName: "calendarIcon")
        self.emailTextfield.txtField.keyboardType   = UIKeyboardType.emailAddress
        self.phoneTextfield.txtField.keyboardType   = UIKeyboardType.phonePad
        self.faxTextfield.txtField.keyboardType   = UIKeyboardType.phonePad
    
        appointmentDateTextfield.txtField.addTarget(self, action: #selector(fexibleTimePicker(textField:)), for: .editingDidBegin)
        
    }
    
    @objc func fexibleTimePicker(textField:UITextField) {
     
           let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
        let controller : AppointmentPopUp = AppointmentPopUp()
         controller.selectionDelegate = self
       NavigationController.present(controller, animated:true, completion: nil)
    }
    
    // MARK: - Methods
    func isAppointFormValid() -> Bool {
        var isValid     =   true
        
        if self.emailTextfield.txtField.text == "" {
            isValid = false
            self.emailTextfield.showError("Merci d'entrer votre email")
        }else
            if self.phoneTextfield.txtField.text == "" {
                isValid = false
                self.phoneTextfield.showError("Merci d'entrer votre numéro du téléphone")
            }else
                if self.appointmentDateTextfield.txtField.text == "" {
                    isValid = false
                    self.appointmentDateTextfield.showError("Merci d'entrer votre Date et heure du rendez-vous")
        }
        
        return isValid
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Objet de la visite"
            textView.textColor = UIColor.lightGray
        }
    }
    
}

private extension AppointmentViewController {

        func existAppointment() {

            if Reachability.isConnectedToNetwork(){
                let loader = Utils.getHUDIndeterminate()
                loader?.show(in: appDelegate.window)
                
                let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
      
                AppointmentApiClient.existAppointment(identifiant: identifiant, success: { appointment in
               
                    
                        DispatchQueue.main.async {
                           
                            if(appointment.adresseMail != ""){
                        
                                let appointment :Appointment!    = appointment
                                Appointment.currentAppointment().remove()
                                appointment.save()
                        
                        }
                             loader?.dismiss()
                        }
                    
                }, failure: { apiError in
                    DispatchQueue.main.async {
                        loader?.dismiss()
                    }
                })
                
                
            }else{
                self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
            }

        }
    
    func addAppointment() {
        
        if self.isAppointFormValid() == true {
            if Reachability.isConnectedToNetwork(){
                
                loader?.show(in: appDelegate.window)
                
                let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
                AppointmentApiClient.addAppointment(identifiant : identifiant,requestData(), success: {  success in
                    DispatchQueue.main.async {
                        Appointment.currentAppointment().remove()
                        self.loader?.dismiss()
                           let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
                        self.view.makeToast("Votre rendez-vous a été ajouté avec succés", duration: 1.0, position: .center)

                          NavigationController.popToRootViewController(animated: true)

                    }
                    
                }, failure: { apiError in
                    
                    DispatchQueue.main.async {
                        self.loader?.dismiss()
                    }
                })
            }else{
                self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
            }
        }
    }
}

extension AppointmentViewController: SideSelectionDelegate {
    
    func SelectedDateTime(time:String,date:String){
       /* mainImageView.image = image
        nameLabel.text = name
        view.backgroundColor = color*/
        
        self.appointmentDateTextfield.txtField.text = "\(date) \(time)"
    }
    
}


