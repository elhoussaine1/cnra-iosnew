//
//  selectHourCollectionViewCell.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 12/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class selectHourCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var hourUILabel: UILabel!
    override var isSelected: Bool {
        didSet {
            self.hourUILabel.backgroundColor = isSelected ? Utils.color(forHex: "5F8D38"): Utils.color(forHex: "FFFFFF")
            self.hourUILabel.textColor = isSelected ? Utils.color(forHex: "FFFFFF"): Utils.color(forHex: "000000")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
