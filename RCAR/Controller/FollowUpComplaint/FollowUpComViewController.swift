//
//  FollowUpComViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 07/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit


class FollowUpComViewController: BaseViewController {
    
    
    // MARK: Properties
    
    var ComplaintArray = [Complaint]()
    var documentIdList = [String]()
    var documentContentList = [String]()
    var storedOffsets = [Int: CGFloat]()
    // Mark: - Outlets
    @IBOutlet weak var expandableTableView: ExpyTableView!
    // Mark: - Actions
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadContentView()
        expandableTableView.register(UINib(nibName: "NormalCell", bundle: nil), forCellReuseIdentifier: "NormalCell")
        expandableTableView.register(UINib(nibName: "ExpandableCell", bundle: nil), forCellReuseIdentifier: "ExpandableCell")
        expandableTableView.register(UINib(nibName: "ExpandedCell", bundle: nil), forCellReuseIdentifier: "ExpandedCell")
        
    }
    override func viewWillAppear(_ animated: Bool) {

        FollowUpComplaint()

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadContentView() {
        
        expandableTableView.dataSource = self
        //expandableTableView.delegate = self
        
        expandableTableView.rowHeight = UITableView.automaticDimension
        expandableTableView.estimatedRowHeight = 44
        
       
       
        //Alter the animations as you want
        //expandableTableView.expandingAnimation = .fade
       // expandableTableView.collapsingAnimation = .fade
        expandableTableView.tableFooterView = UIView()
        expandableTableView.separatorStyle = .none
    }
    
    
}

private extension FollowUpComViewController {
    
    func FollowUpComplaint() {
        if Reachability.isConnectedToNetwork(){
         
            loader?.show(in: appDelegate.window)
            
            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
          
            let profil = Profil.currentProfile().profiltype.trimmingCharacters(in: .whitespaces)

            ComplaintApiClient.FollowUpComplaint(identifiant: identifiant,profil: profil, success: { complaintArray in
                DispatchQueue.main.async {
                    
                    if(complaintArray.count > 0){
                        self.ComplaintArray.removeAll()
                        self.ComplaintArray = complaintArray
                        
                        for i in 0..<complaintArray.count {
                            
                            self.Complaintdetails(complaintId:complaintArray[i].idd)
                        
                        }

                        self.expandableTableView.reloadData()
                    }else{
                       self.view.makeToast("Impossible de charger les données pour le moment, veuillez réessayer plus tard", duration: 1.0, position: .center)
                    }
                
                    self.loader?.dismiss()
                }
                
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                
                    self.loader?.dismiss()
                    self.view.makeToast(apiError, duration: 1.0, position: .center)
                }
           
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
    
    func Complaintdetails(complaintId:String) {
        
        if Reachability.isConnectedToNetwork(){
    
            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
  
            ComplaintApiClient.Complaintdetails(identifiant: identifiant,complaintId: complaintId, success: { details in
                DispatchQueue.main.async {
                     self.documentContentList.append(details)
                }
                
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    
                    //self.loader?.dismiss()
                    self.view.makeToast(apiError, duration: 1.0, position: .center)
                }
                
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
    
    func DisplayComplaintDoc(documentId:String) {
        if Reachability.isConnectedToNetwork(){
            
            loader?.show(in: appDelegate.window)

            ComplaintApiClient.DisplayComplaintDoc(documentId: documentId, success: { documentData in
                DispatchQueue.main.async {
                
                    let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
                    let controller : printViewController = printViewController()
                    controller.codePDF = documentData
                    NavigationController.pushViewController(controller, animated: true)
                    
                    self.loader?.dismiss()
                }
                
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    
                    self.loader?.dismiss()
                    self.view.makeToast(apiError, duration: 1.0, position: .center)
                }
                
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
}

//MARK: ExpyTableViewDataSourceMethods
extension FollowUpComViewController: ExpyTableViewDataSource {
    
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NormalCell.self)) as! NormalCell
        cell.containerUIView.layer.cornerRadius = 5
        cell.containerUIView.addshadowColor(shadowColor: UIColor.black)
        cell.layoutMargins = UIEdgeInsets.zero
        cell.selectionStyle = .none
        
        let fullcomplDate    = addHour(date:String(ComplaintArray[section].dateReclamation))
        cell.complaintDateUILabel.text = fullcomplDate
        cell.sourceUILabel.text = ComplaintArray[section].canal
        cell.stateUILabel.text = ComplaintArray[section].statut
        cell.responseDateUILabel.text = addHour(date:String(ComplaintArray[section].dateReponse))
        
        return cell
    }
}


//MARK: ExpyTableView delegate methods
extension FollowUpComViewController {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK: UITableView Data Source Methods
extension FollowUpComViewController {
    func numberOfSections(in tableView: UITableView) -> Int {
        return ComplaintArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ExpandedCell.self)) as! ExpandedCell
            cell.selectionStyle = .none
            cell.layoutMargins = UIEdgeInsets.zero
            cell.complaintNumberUILabel.text = ComplaintArray[indexPath.section].idd
            
            let modifiedFont = String(format:"<span style=\"font-family: '-apple-system', 'RobotoCondensed-Regular'; font-size: 12 \">%@</span>", documentContentList[indexPath.section])
            
            cell.complaintdescUILabel.attributedText = try? NSAttributedString(data: modifiedFont.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            
            documentIdList = ComplaintArray[indexPath.section].idtDocument.components(separatedBy: ",")
            if(ComplaintArray[indexPath.section].idtDocument != "" ){
                cell.showDocumentUIView.isHidden = false
                cell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
                cell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
                
            }else{
                cell.showDocumentUIView.isHidden = true
            }
           

            return cell
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ExpandedCell.self)) as! ExpandedCell
            cell.layoutMargins = UIEdgeInsets.zero
            return cell
        }
    }
}

extension FollowUpComViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return documentIdList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "documentCollectionViewCell", for: indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DisplayComplaintDoc(documentId:documentIdList[indexPath.row])
    }
}



