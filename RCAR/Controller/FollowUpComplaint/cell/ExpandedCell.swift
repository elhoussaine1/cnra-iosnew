//
//  ExpandedCell.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 05/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class ExpandedCell: UITableViewCell  {
    
    @IBOutlet weak var showDocumentUIView: UIView!
    
    @IBOutlet weak var complaintNumberUILabel: UILabel!
    @IBOutlet weak var complaintdescUILabel: UILabel!
    @IBOutlet weak var documentUICollectionView: UICollectionView!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        let currentCellView = "documentCollectionViewCell"
        let nib                         = UINib(nibName: currentCellView, bundle: nil)
        documentUICollectionView.register(nib, forCellWithReuseIdentifier: currentCellView)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

extension ExpandedCell {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        documentUICollectionView.delegate = dataSourceDelegate
        documentUICollectionView.dataSource = dataSourceDelegate
        documentUICollectionView.tag = row
        documentUICollectionView.setContentOffset(documentUICollectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        documentUICollectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { documentUICollectionView.contentOffset.x = newValue }
        get { return documentUICollectionView.contentOffset.x }
    }
}
