//
//  NormalCell.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 05/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit


class NormalCell: UITableViewCell, ExpyTableViewHeaderCell {
    @IBOutlet weak var containerUIView: UIView!
    @IBOutlet weak var complaintDateUILabel: UILabel!
    @IBOutlet weak var sourceUILabel: UILabel!
    @IBOutlet weak var stateUILabel: UILabel!
    @IBOutlet weak var responseDateUILabel: UILabel!
    @IBOutlet weak var drawUIImageView: UIImageView!
    
    func changeState(_ state: ExpyState, cellReuseStatus cellReuse: Bool) {
        
        switch state {
        case .willExpand:
            arrowDown(animated: !cellReuse)
            
        case .willCollapse:
            arrowRight(animated: !cellReuse)
            
        case .didExpand:
            break
        case .didCollapse:
            break
        }
    }
    
    private func arrowDown(animated: Bool) {
        UIView.animate(withDuration: (animated ? 0.3 : 0)) {
            self.drawUIImageView.transform = CGAffineTransform(rotationAngle: (CGFloat.pi / 2))
        }
    }
    
    private func arrowRight(animated: Bool) {
        UIView.animate(withDuration: (animated ? 0.3 : 0)) {
            self.drawUIImageView.transform = CGAffineTransform(rotationAngle: 0)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
}
