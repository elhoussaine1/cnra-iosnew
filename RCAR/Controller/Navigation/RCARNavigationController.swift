
//  Copyright (c) 2015 AraMobile. All rights reserved.
//

import UIKit


class RCARNavigationController: UINavigationController, UINavigationControllerDelegate {

    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        self.navigationBar.tintColor        = Utils.color(forHex: "fec601")
        self.navigationBar.barTintColor     = navigationBar.barTintColor
        self.navigationBar.isTranslucent    = false
        self.navigationBar.isHidden         = false
        self.navigationBar.frame.size       = self.navigationBar.sizeThatFits(CGSize(width: self.navigationBar.frame.size.width,height: 84))
        self.navigationBar.setBackgroundImage(UIImage(named: "Nav_Background")!.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch), for: .default)
       
 
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
