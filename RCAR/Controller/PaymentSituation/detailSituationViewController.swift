//
//  detailSituationViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 16/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class detailSituationViewController: BaseViewController {
    
    
    @IBOutlet weak var containerUIView: UIView!
    var paymentSituation : PaymentSituation!
    var indexPath :Int!
    var paymentAmounts = [UILabel]()
  

    @IBOutlet weak var nomUILabel: UILabel!
    @IBOutlet weak var matriculeUILabel: UILabel!
    @IBOutlet weak var agencePaiementUILabel: UILabel!
    @IBOutlet weak var moisanneeUILabel: UILabel!
    
    
    @IBOutlet weak var MONTANTPENSIONINDEXEEUILabel: UILabel!
    @IBOutlet weak var PENSIONRCUILabel: UILabel!
    @IBOutlet weak var MONTANTIRUILabel: UILabel!
    @IBOutlet weak var PRELEVSCCNOPSUILabel: UILabel!
    @IBOutlet weak var PRELEVSMMGPAPUILabel: UILabel!
    @IBOutlet weak var PRELEVCCDMGPAPUILabel: UILabel!
    @IBOutlet weak var netapayeUILabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadContentView()
        detailSituation()
      
       
        let lastName = paymentSituation.nom as String
        self.nomUILabel.text = lastName
        self.matriculeUILabel.text = paymentSituation.matricule as String
        self.agencePaiementUILabel.text = paymentSituation.paiements[indexPath].agencePaiement as String
        let moisEcheance = paymentSituation.paiements[indexPath].moisEcheance as String
        let anneeEcheance = paymentSituation.paiements[indexPath].anneeEcheance as String
        self.moisanneeUILabel.text = "\(moisEcheance) / \(anneeEcheance)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadContentView() {
        containerUIView.layer.cornerRadius = 5
        containerUIView.addshadowColor(shadowColor: UIColor.black)
        
        paymentAmounts.append(self.MONTANTPENSIONINDEXEEUILabel)
        paymentAmounts.append(self.PENSIONRCUILabel)
        paymentAmounts.append(self.MONTANTIRUILabel)
        paymentAmounts.append(self.PRELEVSCCNOPSUILabel)
        paymentAmounts.append(self.PRELEVSMMGPAPUILabel)
        paymentAmounts.append(self.PRELEVCCDMGPAPUILabel)
   
        
    
    }
}

private extension detailSituationViewController{
    
    func detailSituation(){
        
        if Reachability.isConnectedToNetwork(){
     
            loader?.show(in: appDelegate.window)

            
            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
            let moisEche = paymentSituation.paiements[indexPath].moisEcheance as String
            let anneeEche = paymentSituation.paiements[indexPath].anneeEcheance as String
           
            paymentSituationApiClient.detailSituation(identifiant : identifiant,moisEche : moisEche,anneeEche : anneeEche, success: { SituationDetailArray in
                DispatchQueue.main.async {
                    
                 
                    
                    for i in 0..<SituationDetailArray.count {
                      //  self.paymentAmounts[i].text = SituationDetailArray[i].montant as String
                        
                        switch SituationDetailArray[i].libRubriquePaie {
                        case "MONTANT DE LA PENSION INDEXEE":
                            self.MONTANTPENSIONINDEXEEUILabel.text = SituationDetailArray[i].montant as String
                        case "PENSION RC":
                             self.PENSIONRCUILabel.text = SituationDetailArray[i].montant as String
                        case "MONTANT IR":
                             self.MONTANTIRUILabel.text = SituationDetailArray[i].montant as String
                        case "PRELEV SC CNOPS":
                            
                           self.PRELEVSCCNOPSUILabel.text = SituationDetailArray[i].montant as String
                        case "PRELEV SM MGPAP":
                            self.PRELEVSMMGPAPUILabel.text = SituationDetailArray[i].montant as String
                        case "PRELEV CCD MGPAP":
                            self.PRELEVCCDMGPAPUILabel.text = SituationDetailArray[i].montant as String
                        default: break
                            
                             }
                      
                    }
                 

                    self.netapayeUILabel.text =  self.paymentSituation.paiements[self.indexPath].mntPaiementNet as String
                        
                        self.loader?.dismiss()
                
    
                   
                }
                
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                }
            })
            
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
        
    }
    
   
    
    
}
