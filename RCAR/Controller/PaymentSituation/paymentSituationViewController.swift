//
//  paymentSituationViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 14/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//


import UIKit

class paymentSituationViewController: BaseViewController {
    
  
    // MARK: Properties
    var paymentSituation = PaymentSituation()
    var paymentArray = [Payments]()
    // Mark: - Outlets
    
    @IBOutlet weak var profilUILabel: UILabel!
    @IBOutlet weak var registrationnumberUILabel: UILabel!
    @IBOutlet weak var fullnameUILabel: UILabel!
    @IBOutlet weak var paymentSituationExpyTableView: ExpyTableView!
    
    // Mark: - Actions
    
    @IBAction func printUIButton(_ sender: Any) {
        
        printpaymentSituation()
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadContentView()
        getpaymentSituation()
        paymentSituationExpyTableView.register(UINib(nibName: "PaymentSituationTableViewCell", bundle: nil), forCellReuseIdentifier: "PaymentSituationTableViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        

    }
    
    func loadContentView() {
        
        let  profil = Profil.currentProfile().profiltype as String
        //let fullName = Profil.currentProfile().name as String
        
        self.profilUILabel.text =  profil
        paymentSituationExpyTableView.dataSource = self
        paymentSituationExpyTableView.delegate = self
        
        paymentSituationExpyTableView.rowHeight = UITableView.automaticDimension
        paymentSituationExpyTableView.estimatedRowHeight = 44
        paymentSituationExpyTableView.tableFooterView = UIView()
        paymentSituationExpyTableView.separatorStyle = .none
        
    }
    

}

private extension paymentSituationViewController {
    
    func getpaymentSituation() {
        if Reachability.isConnectedToNetwork(){

            loader?.show(in: appDelegate.window)
            
            
            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
            
            paymentSituationApiClient.getpaymentSituation(identifiant: identifiant, success: { paymentSituation in
                DispatchQueue.main.async {
                    self.registrationnumberUILabel.text = paymentSituation.matricule as String
                    
                   
                    let lastName = paymentSituation.nom as String
                    let firstName = paymentSituation.prenom as String
                    
                    self.fullnameUILabel.text = lastName + firstName
                    self.paymentSituation = paymentSituation
                    self.paymentSituationExpyTableView.reloadData()
                   
                    self.loader?.dismiss()
                }
                
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                }
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
    
    
    
}

//MARK: ExpyTableViewDataSourceMethods
extension paymentSituationViewController: ExpyTableViewDataSource {
    
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PaymentSituationTableViewCell.self)) as! PaymentSituationTableViewCell
        
        cell.containerUIView.layer.cornerRadius = 5
        cell.selectionStyle = .none
        cell.containerUIView.addshadowColor(shadowColor: UIColor.black)
        cell.yearUILabel.text = paymentSituation.paiements[section].anneeEcheance
        cell.agencyUILabel.text = paymentSituation.paiements[section].agencePaiement
        cell.monthUILabel.text = paymentSituation.paiements[section].moisEcheance
        cell.amountUILabel.text = paymentSituation.paiements[section].mntPaiementNet
        cell.paymentSituationUILabel.text = paymentSituation.paiements[section].stuationPaiement
    
        cell.layoutMargins = UIEdgeInsets.zero
        // cell.showSeparator()
        return cell
    }
}


//MARK: ExpyTableView delegate methods
extension paymentSituationViewController: ExpyTableViewDelegate {
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
    }
}



extension paymentSituationViewController {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
        let controller : detailSituationViewController = detailSituationViewController()
        controller.paymentSituation =  paymentSituation
        controller.indexPath = indexPath.section
        NavigationController.pushViewController(controller, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK: UITableView Data Source Methods
extension paymentSituationViewController {
    func numberOfSections(in tableView: UITableView) -> Int {
        return paymentSituation.paiements.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ExpandedCell.self)) as! ExpandedCell
            cell.layoutMargins = UIEdgeInsets.zero
            // cell.showSeparator()
            return cell
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ExpandedCell.self)) as! ExpandedCell
            // cell.labelSpecification.text = (sampleData[indexPath.section])[indexPath.row]
            cell.layoutMargins = UIEdgeInsets.zero
            // cell.hideSeparator()
            return cell
        }
    }
}

extension paymentSituationViewController {
    
    
    func printpaymentSituation() {
        if Reachability.isConnectedToNetwork(){
            loader?.show(in: self.view)
            
            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
             let  nom = Profil.currentProfile().name.trimmingCharacters(in: .whitespaces)
   
            paymentSituationApiClient.printpaymentSituation(identifiant : identifiant,nom : nom, success: { data in
                DispatchQueue.main.async {
                    let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
                    let controller : printViewController = printViewController()
                    controller.codePDF = data
                    NavigationController.pushViewController(controller, animated: true)
                    
                    self.loader?.dismiss()
                }
                
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                }
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
    
}
