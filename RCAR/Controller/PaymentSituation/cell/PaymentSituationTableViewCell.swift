//
//  PaymentSituationTableViewCell.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 15/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class PaymentSituationTableViewCell: UITableViewCell {

    @IBOutlet weak var paymentSituationUILabel: UILabel!
    @IBOutlet weak var amountUILabel: UILabel!
    @IBOutlet weak var containerUIView: UIView!
    @IBOutlet weak var agencyUILabel: UILabel!
    @IBOutlet weak var monthUILabel: UILabel!
    @IBOutlet weak var yearUILabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
