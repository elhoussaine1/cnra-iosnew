//
//  printViewController.swift
//  RCAR
//
//  Created by EL houssaine El gamouz on 17/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import WebKit

class printViewController: BaseViewController {
    
    var codePDF :Data!
    @IBOutlet weak var printPDFDocument: WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()

        if let decodeData = NSData(base64Encoded: (codePDF?.base64EncodedString())!, options: .ignoreUnknownCharacters) {
            printPDFDocument.load(decodeData as Data, mimeType: "application/pdf", characterEncodingName: "utf-8", baseURL: NSURL(fileURLWithPath: "") as URL)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
