//
//  CertificationViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 06/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.

import UIKit

class CertificationViewController: BaseViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UITextViewDelegate {
    
    
    // MARK: Properties
    var CertificationTypeArray = [CertificationType]()
    var nextSeq :String = ""
    var selectedTypeId:String = ""
    let StoredNextSeq:UserDefaults = UserDefaults.standard
    let CertificationTypePickerView = UIPickerView()
    // Mark: - Outlets
    
    @IBOutlet weak var profilUILabel: UILabel!
    @IBOutlet weak var registrenumberUILabel: UILabel!
    @IBOutlet weak var fullnameUILabel: UILabel!
    @IBOutlet weak var mailTextfield: Textfield!
    @IBOutlet weak var AddressUITextView: UITextView!
    @IBOutlet weak var typesTextfield: Textfield!
   
    // Mark: - Actions
    @IBAction func sendUIButton(_ sender: Any) {
        addCertification()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadContentView()
        getNextSeq()
        getcertificationType()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        registrenumberUILabel.text = Profil.currentProfile().identifiant as String
        fullnameUILabel.text = Profil.currentProfile().name as String

        AddressUITextView.text = "Adresse"
        AddressUITextView.textColor = UIColor.lightGray
    }
    
    
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return CertificationTypeArray.count
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "Roboto-Medium", size: 16)
            pickerLabel?.textAlignment = .center
        }
        pickerLabel?.text = CertificationTypeArray[row].libelle!
        pickerLabel?.textColor = Utils.color(forHex: "FFFFFF")
        
        return pickerLabel!
    }
    /*func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        selectedTypeId = CertificationTypeArray[row].code
        let title = CertificationTypeArray[row].libelle!
        
        return title
    }*/
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        typesTextfield.txtField.text = CertificationTypeArray[row].libelle as String
     
    }

 
    
    func requestData() -> URLRequestParams {
        let data = URLRequestParams()
        return data
    }
    func storeSelectedNextSeq(NextSeq:String){
        StoredNextSeq.removeObject(forKey: "NextSeq")
        StoredNextSeq.set(NextSeq, forKey: "NextSeq")
    }
 
    
    func loadContentView() {
        
        self.mailTextfield.txtField.delegate       = self
        self.typesTextfield.txtField.delegate = self
        self.AddressUITextView.delegate = self
        self.mailTextfield.lbldiscription.text         = "Email@exemple.com"
        self.mailTextfield.txtField.keyboardType   = UIKeyboardType.emailAddress
        self.typesTextfield.lbldiscription.text       = "Type d'attestation"
        self.typesTextfield.rightIconHidding(false,iconName: "down")
        self.CertificationTypePickerView.delegate = self
        let  profil = Profil.currentProfile().profiltype as String
        //let fullName = Profil.currentProfile().name as String
        self.profilUILabel.text =  profil
        
        typesTextfield.txtField.addTarget(self, action: #selector(callPickerView(textField:)), for: .editingDidBegin)
        
        CertificationTypePickerView.backgroundColor =  Utils.color(forHex: "48575E")
        CertificationTypePickerView.setValue(Utils.color(forHex: "FFFFFF"), forKey: "textColor")
       

        
    }
    
    @objc func callPickerView(textField:UITextField){
        
        typesTextfield.txtField.inputView = CertificationTypePickerView
        
    }
    
    func isCertificationFormValid() -> Bool {
        var isValid     =   true
        
        if self.typesTextfield.txtField.text == "" {
            isValid = false
            self.typesTextfield.showError("Merci d'entrer votre type d'attestation")
        }else
            if self.mailTextfield.txtField.text == "" {
                isValid = false
                self.mailTextfield.showError("Merci d'entrer votre email")
            }
        
        
        return isValid
    }
    
    func requestCertificationData() -> URLRequestParams {
        var data = URLRequestParams()
        
        let date = Date()
        let Yearformatter = DateFormatter()
        Yearformatter.dateFormat = "yy"
        let setAnnee = Yearformatter.string(from: date)
        let Dateformatter = DateFormatter()
        Dateformatter.dateFormat = "dd/MM/yyyy"
        
        
        let setDateDemande = Dateformatter.string(from: date)
        data["nomDemandeur"] = Profil.currentProfile().name.trimmingCharacters(in: .whitespaces)
        data["mail"] = mailTextfield.txtField.text
        data["adresse"] = AddressUITextView.text
        data["idTypeAttestation"] = selectedTypeId
        data["profil"] = "REC"
        data["typeDemande"] = "D"
        data["numeroClient"] = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
        data["produit"] = "RCAR"
        data["canal"] = "MOBILE"
        if(StoredNextSeq.object(forKey: "NextSeq") != nil){
            let setSequence = StoredNextSeq.object(forKey: "NextSeq")
            data["sequence"] = setSequence
        }
        data["annee"] = setAnnee

        let DateDemandeformatter = DateFormatter()
        DateDemandeformatter.dateFormat = "yyyy"

        let numeroDemande = "rcar\(DateDemandeformatter.string(from: date))web\(randomString(length: 10))"
     

        
        return data
        
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Adresse"
            textView.textColor = UIColor.lightGray
        }
    }
    
    
}


extension CertificationViewController{
    
    func getcertificationType(){
        
        if Reachability.isConnectedToNetwork(){
            
            loader?.show(in: appDelegate.window)
            
            
            CertificationApiClient.getcertificationType(requestData() , success: { certificationTypeArray in
                DispatchQueue.main.async {
                    self.CertificationTypeArray = certificationTypeArray
                    self.CertificationTypePickerView.reloadAllComponents()
                    self.loader?.dismiss()
                }
                
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                }
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
        
    }
    
    func getNextSeq(){
        
        if Reachability.isConnectedToNetwork(){
         
            loader?.show(in: appDelegate.window)
            
            
            CertificationApiClient.getNextSeq(requestData() , success: { NextSeq in
                DispatchQueue.main.async {
                    self.storeSelectedNextSeq(NextSeq:NextSeq)
                    self.loader?.dismiss()
                }
                
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                }
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
        
    }
}
private extension CertificationViewController {
    
    func addCertification(){
        
        if self.isCertificationFormValid() == true {
            if Reachability.isConnectedToNetwork(){
              
                loader?.show(in: appDelegate.window)
                CertificationApiClient.addCertification(requestCertificationData(), success: {  success in
                
                    DispatchQueue.main.async {
                        self.loader?.dismiss()
                        self.view.makeToast("Votre demande a été envoyée avec succés", duration: 1.0, position: .center)
                    }
                
                }, failure: { apiError in
                    DispatchQueue.main.async {
                        self.loader?.dismiss()
                    }
                })
            }else{
                self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
            }
        }
    }
}
