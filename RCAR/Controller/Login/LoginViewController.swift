//
//  LoginViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 29/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import WebKit
import FirebaseRemoteConfig


class LoginViewController: BaseViewController , UITextFieldDelegate , UINavigationControllerDelegate  {
    
    // MARK: Properties
    var selectTextField : UITextField!      = UITextField()
    var hasChoose       : Bool              = false
    var isParticulier   : Bool              = true
    var index           : Int               = 0
    var keybordHeight   : CGFloat!
    var keyboard        : Bool              = false
    var profile         : Profil            = Profil.currentProfile()
    var ShowHidePassWord = true
    var remoteConfig: RemoteConfig!
    
    // Mark: - Outlets
    
    @IBOutlet weak var loginTextField: Textfield!
    @IBOutlet weak var passLogTextField: Textfield!
    //InscriptionView
    
    // Mark: - Actions
    
    @IBAction func resignTextField(button :UIButton) {
        self.selectTextField.resignFirstResponder()
    }
    
    
    @IBAction func SendAuthRequest(_ sender: UIButton){
        
        if self.isloginFormValid() == true {
            loader?.show(in: appDelegate.window)
            
            //Authentification
            UserApiClient.login(requestData(), success: {  profil in
                
                let profile :Profil!    = profil
                profile.save()
                self.saveLogin()
                
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                    if(profil.identifiant == self.loginTextField.txtField.text?.trimmingCharacters(in: .whitespaces)){
                        
                        //saveDevice
                        self.SaveDevice()

                        let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
                        let curentController  = HomeViewController() as HomeViewController
                        NavigationController.pushViewController(curentController, animated: true)

                    }else{
                        self.view.makeToast("login ou mot de passe incorrect", duration: 1.0, position: .center)
                        
                    }
                    
                }
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.view.makeToast(apiError, duration: 1.0, position: .center)
                    self.loader?.dismiss()
                    
                }
                
            })
        }
        
    }
    
    
    init() {
        super.init(nibName: "LoginViewController", bundle: nil)
    }
    required   init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if self.index == 0 {
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupAPP()

    }
    
    func createDefaults(){
        
        remoteConfig = RemoteConfig.remoteConfig()
        let remoteConfigSettings = RemoteConfigSettings(developerModeEnabled: true)
        remoteConfig.configSettings = remoteConfigSettings
        // remoteConfig.setDefaults(fromPlist: "RemoteConfigDefaults")
    }
    
    func setupAPP(){
        createDefaults()
        
        remoteConfig.fetch(withExpirationDuration: 0){ (status, error) -> Void in
            if(status == RemoteConfigFetchStatus.success){
                
           
                self.remoteConfig.activateFetched()
                
                let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
 
                if(self.remoteConfig["ios_version_RCAR"].stringValue != currentVersion ){
                    
                    let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
                    let curentController  = NewVersionViewController() as NewVersionViewController
                    curentController.screenshot = self.takeScreenshot()
                    curentController.version_message = self.remoteConfig["ios_version_message_RCAR"].stringValue
                    curentController.modalPresentationStyle = .fullScreen
                    NavigationController.present(curentController, animated: true)
                }
                
            }else{
                print("Config not fetched!")
                print("Erro \(error!.localizedDescription)")
                
            }
            
        }
        

        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadContentView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    // MARK: - Methods
    
    func loadContentView() {
        
        self.loginTextField.txtField.delegate    = self
        self.passLogTextField.txtField.delegate        = self
        self.loginTextField.lbldiscription.text      = "Identifiant *"
        self.passLogTextField.lbldiscription.text            = "Mot de passe *"
        self.passLogTextField.isPassword = true
        if(Profil.currentProfile().identifiant != nil){
            
        }
        
        if(userDefaults.object(forKey: "loginText") != nil){
            self.loginTextField.txtField.text = userDefaults.object(forKey: "loginText") as? String
        }
        
        self.passLogTextField.rightIconHidding(false,iconName: "hidePassWord")
        let passwordtapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showpassword(tapGestureRecognizer:)))
        self.passLogTextField.rightIcon.isUserInteractionEnabled = true
        self.passLogTextField.rightIcon.addGestureRecognizer(passwordtapGestureRecognizer)
        
        
    }
    
    
    @objc func showpassword(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if(ShowHidePassWord == true) {
            self.passLogTextField.isPassword = false
            self.passLogTextField.rightIconHidding(false,iconName: "showPassWord")
        } else {
            self.passLogTextField.isPassword = true
            self.passLogTextField.rightIconHidding(false,iconName: "hidePassWord")
        }
        ShowHidePassWord = !ShowHidePassWord
    }
    
    func requestData() -> URLRequestParams {
        var data = URLRequestParams()
        
        let username = loginTextField.txtField.text
        let password = passLogTextField.txtField.text
        data["grant_type"] = "password"
        data["username"] = username
        data["password"] = password
        return data
    }
    
    func isloginFormValid() -> Bool {
        var isValid     =   true
        if self.loginTextField.txtField.text == "" {
            isValid = false
            self.loginTextField.showError("Merci d'entrer votre identifiant")
        }else
            if self.passLogTextField.txtField.text == "" {
                isValid = false
                self.passLogTextField.showError("Merci d'entrer votre mot de passe")
        }
        return isValid
    }
    
    func saveLogin(){
        
        userDefaults.removeObject(forKey: "loginText")
        userDefaults.set(loginTextField.txtField.text, forKey: "loginText")
        userDefaults.synchronize()
        
    }
    
    func SaveDevice(){
        
       // SaveDevice(identifiant : String,deviceToken
        
        if Reachability.isConnectedToNetwork(){
            
            loader?.show(in: appDelegate.window)
            
            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)

            if( UserDefaults.standard.value(forKey: "DeviceToken") != nil){
               let deviceToken =  UserDefaults.standard.value(forKey: "DeviceToken") as! String
 
                UserApiClient.SaveDevice(identifiant: identifiant,deviceToken: deviceToken, success: { folderArray in
                    DispatchQueue.main.async {
                        
                        //                    if(folderArray.count > 0){
                        //
                        //                        self.FolderArray.removeAll()
                        //                        self.FolderArray = folderArray
                        //                        self.followUpFolderUITableView.reloadData()
                        //
                        //                    }else{
                        //                        self.view.makeToast("Impossible de charger les données pour le moment, veuillez réessayer plus tard", duration: 1.0, position: .center)
                        //                    }
                        
                        self.loader?.dismiss()
                        
                    }
                    
                }, failure: { apiError in
                    DispatchQueue.main.async {
                        self.loader?.dismiss()
                        self.view.makeToast(apiError, duration: 1.0, position: .center)
                        
                    }
                })
            }
            
      
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
