//
//  HomePagerViewController.swift
//  ClashFoot
//
//  Created by lotfi on 26/07/2016.
//  Copyright © 2016 Aramobile. All rights reserved.
//

import UIKit
import AVFoundation

class InscripViewController: BaseViewController,UITextFieldDelegate  {
    
    // MARK: Properties
    var index               : Int               = 0
    let birthdayPickerView = UIDatePicker()
    var  ProfilType :String = " "
    var ShowHidePassWord = true
    
    // Mark: - Outlets
    @IBOutlet weak var AffiliatedLTHRadioButton: LTHRadioButton!
    @IBOutlet weak var BeneficiaryLTHRadioButton: LTHRadioButton!
    @IBOutlet weak var confirmPasswordTextfield: Textfield!
    @IBOutlet weak var passwordTextfield: Textfield!
    @IBOutlet weak var mailTextfield: Textfield!
    @IBOutlet weak var birthdateTextfield: Textfield!
    @IBOutlet weak var fullnameTextfield: Textfield!
    @IBOutlet weak var loginTextfield: Textfield!
    
    // Mark: - Actions
    @IBAction func submitUIButton(_ sender: Any) {
        
        signUp()
    }
    @IBAction func nextUIButton(_ sender: Any) {
        
        
        
    }
    
    
    init() {
        super.init(nibName: "InscripViewController", bundle: nil)
    }
    required   init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadContentView()
        
    }
    
    
    func loadContentView() {
        
        AffiliatedLTHRadioButton.translatesAutoresizingMaskIntoConstraints = false
        BeneficiaryLTHRadioButton.translatesAutoresizingMaskIntoConstraints = false
        BeneficiaryLTHRadioButton.select()
        
        BeneficiaryLTHRadioButton.onSelect {
            
            self.ProfilType = "BENEFICIARE"
            self.AffiliatedLTHRadioButton.deselect(animated: false)
        }
        
        AffiliatedLTHRadioButton.onSelect {
            
            self.ProfilType = "AFFILIE"
            self.BeneficiaryLTHRadioButton.deselect(animated: false)
        }
        
        self.passwordTextfield.txtField.delegate    = self
        self.confirmPasswordTextfield.txtField.delegate = self
        self.mailTextfield.txtField.delegate        = self
        self.birthdateTextfield.txtField.delegate       = self
        self.fullnameTextfield.txtField.delegate     = self
        self.loginTextfield.txtField.delegate       = self
        
        
        self.passwordTextfield.lbldiscription.text         = "Mot de passe *"
        self.passwordTextfield.isPassword = true
        self.confirmPasswordTextfield.lbldiscription.text         = "Confirmation du mot de passe *"
        self.confirmPasswordTextfield.isPassword = true
        
        self.mailTextfield.lbldiscription.text       = "Email@exemple.com"
        self.birthdateTextfield.lbldiscription.text           = "Date de naissance *"
        self.birthdateTextfield.rightIconHidding(false,iconName: "calendarIcon")
        self.fullnameTextfield.lbldiscription.text      = "Nom et Prénom *"
        self.loginTextfield.lbldiscription.text            = "Identifiant *"
        self.mailTextfield.txtField.keyboardType   = UIKeyboardType.emailAddress
        
        
        
        birthdateTextfield.txtField.addTarget(self, action: #selector(callDatePicker(textField:)), for: .editingDidBegin)
        birthdayPickerView.backgroundColor =  Utils.color(forHex: "48575E")
        birthdayPickerView.setValue(Utils.color(forHex: "FFFFFF"), forKey: "textColor")
        
        self.passwordTextfield.rightIconHidding(false,iconName: "hidePassWord")
        let passwordtapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showpassword(tapGestureRecognizer:)))
        self.passwordTextfield.rightIcon.isUserInteractionEnabled = true
        self.passwordTextfield.rightIcon.addGestureRecognizer(passwordtapGestureRecognizer)
        
        self.confirmPasswordTextfield.rightIconHidding(false,iconName: "hidePassWord")
        
        let confirmPtapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showpassword(tapGestureRecognizer:)))
        self.confirmPasswordTextfield.rightIcon.isUserInteractionEnabled = true
        self.confirmPasswordTextfield.rightIcon.addGestureRecognizer(confirmPtapGestureRecognizer)
        
        self.passwordTextfield.rightIcon.tag = 1
        self.confirmPasswordTextfield.rightIcon.tag = 2
        
    }
    
    @objc func showpassword(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let sender = tapGestureRecognizer.view!.tag

        switch sender {
        case 1 :
            if(ShowHidePassWord == true) {
                self.passwordTextfield.isPassword = false
                self.passwordTextfield.rightIconHidding(false,iconName: "showPassWord")
            } else {
                self.passwordTextfield.isPassword = true
                self.passwordTextfield.rightIconHidding(false,iconName: "hidePassWord")
            }
        case 2 :
            if(ShowHidePassWord == true) {
                self.confirmPasswordTextfield.isPassword = false
                self.confirmPasswordTextfield.rightIconHidding(false,iconName: "showPassWord")
                
            } else {
                self.confirmPasswordTextfield.isPassword = true
                self.confirmPasswordTextfield.rightIconHidding(false,iconName: "hidePassWord")
            }
            
        default :
            break;
        }
        
        
        ShowHidePassWord = !ShowHidePassWord
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func callDatePicker(textField: UITextField) {
        
        birthdayPickerView.datePickerMode = UIDatePicker.Mode.date
        birthdateTextfield.txtField.inputView = birthdayPickerView
        birthdayPickerView.addTarget(self, action: #selector(fillingbirthdateTextfield), for: UIControl.Event.valueChanged)
    }
    
    
    @objc func fillingbirthdateTextfield() {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let bithday = formatter.string(from: birthdayPickerView.date)
        birthdateTextfield.txtField.text = bithday
    }
    
    func issignUpFormValid() -> Bool {
        var isValid     =   true
        if self.loginTextfield.txtField.text == "" {
            isValid = false
            self.loginTextfield.showError("Merci d'entrer votre identifiant")
        }else
            if self.fullnameTextfield.txtField.text == "" {
                isValid = false
                self.fullnameTextfield.showError("Merci d'entrer votre nom et prénom")
            }else
                if self.birthdateTextfield.txtField.text == "" {
                    isValid = false
                    self.birthdateTextfield.showError("Merci d'entrer votre date de naissance")
                }else
                    if self.mailTextfield.txtField.text == "" {
                        isValid = false
                        self.mailTextfield.showError("Merci d'entrer votre email")
                    }else
                        if self.passwordTextfield.txtField.text == "" {
                            isValid = false
                            self.passwordTextfield.showError("Merci d'entrer votre mot de passe")
                        }else
                            if self.confirmPasswordTextfield.txtField.text == "" {
                                isValid = false
                                self.confirmPasswordTextfield.showError("Merci de confirmer votre mot de passe")
        }
        return isValid
    }
    
    
    func requestData() -> URLRequestParams {
        var data = URLRequestParams()
        
        data["nomPrenom"] = fullnameTextfield.txtField.text
        data["identifiant"] = loginTextfield.txtField.text
        data["email"] = mailTextfield.txtField.text
        data["dateNaissance"] = birthdateTextfield.txtField.text
        data["profil"] = ProfilType
        data["password"] = passwordTextfield.txtField.text
        data["confirmPassword"] = confirmPasswordTextfield.txtField.text
        data["produit"] = "RCAR"
        data["isRecoverPwd"] = "N"
        
        return data
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

private extension InscripViewController {
    
    func signUp(){
        
        if self.issignUpFormValid() == true {
            
            loader?.show(in: appDelegate.window)
            
            //Authentification
            UserApiClient.signup(requestData(), success: {  result in
                
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                    if(result["code"].stringValue == "ERROR"){
                        
                        let result =
                            result["message"].stringValue.data(using: .utf8)
                        let message = NSString(data: result!, encoding: String.Encoding.utf8.rawValue)! as String
                        self.view.makeToast(message, duration: 3.0, position: .center)
                        
                    }else{
                        
                        self.view.makeToast(result["message"].stringValue, duration: 3.0, position: .center)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loginSwipeViewPager"), object: nil)
                    }
                    
                }
                
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                }
            })
        }
        
    }
}

