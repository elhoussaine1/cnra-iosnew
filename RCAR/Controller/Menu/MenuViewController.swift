 //
 //  MenuViewController.swift
 //  ClashFoot
 //
 //  Created by lotfi on 22/10/2015.
 //  Copyright (c) 2015 AraMobile. All rights reserved.
 //
 
 import UIKit
 import MessageUI
 
 class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate ,MFMailComposeViewControllerDelegate {
    
    
    @IBOutlet weak var menuTableView    :UITableView!
    @IBOutlet weak var loggedView       :UIView!
 
    
    var menuList        : NSMutableArray!   = NSMutableArray()
    
    
    var selectedIndex   : Int               = 0
    var profile         : Profil            = Profil.currentProfile()
    
    init() {
        super.init(nibName: "MenuViewController", bundle: nil)
    }
    required   init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let menuViewCell = UINib(nibName: "MenuViewCell", bundle:nil)
        self.menuTableView.register(menuViewCell, forCellReuseIdentifier: "MenuViewCell")
        
        let menuViewCellCD = UINib(nibName: "MenuViewCellCD", bundle:nil)
        self.menuTableView.register(menuViewCellCD, forCellReuseIdentifier: "MenuViewCellCD")

    }
    //
    func loadMenu(){
        
        self.menuList.removeAllObjects()
        self.menuList.add("ACTUALITÉS")
        self.menuList.add("LE RCAR")
        self.menuList.add("AIDE")
        self.menuList.add("CENTRE D'APPEL")
        self.menuList.add("FACEBOOK")
        self.menuList.add("LINKEDIN")
        self.menuTableView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadMenu()
        
       menuTableView.backgroundView = UIImageView(image: UIImage(named: "bg_menu.png"))

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 57
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = menuTableView.dequeueReusableCell(withIdentifier: String(describing: MenuViewCell.self)) as! MenuViewCell
        let menuElement : NSString  = self.menuList.object(at: indexPath.row) as! NSString
        cell.titleCell?.text        = NSLocalizedString( menuElement as String, comment: "")
        let named                   = String(format : (menuElement as String) + ".png")
        cell.imageCell.image        = UIImage(named: named)
        
        if(indexPath.row == 3){
            let cell = menuTableView.dequeueReusableCell(withIdentifier: String(describing: MenuViewCellCD.self)) as! MenuViewCellCD
          
            let menuElement : NSString  = self.menuList.object(at: indexPath.row) as! NSString
            cell.titleCell?.text        = NSLocalizedString( menuElement as String, comment: "")
            let named                   = String(format : (menuElement as String) + ".png")
            cell.imageCell.image        = UIImage(named: named)
            
            return cell
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let center : UINavigationController = (UIApplication.shared.delegate as! AppDelegate).sideMenuViewController!.centerViewController as! UINavigationController
        (UIApplication.shared.delegate as! AppDelegate).sideMenuViewController!.toggle(MMDrawerSide.left, animated: true, completion: nil)
    
        
        self.selectedIndex = indexPath.row
        self.menuTableView.reloadData()
        switch (indexPath.row){
        case 0:
         
            let NewsViewController = newsViewController()
            center.pushViewController(NewsViewController, animated: true)
            break;
        case 1 :
          
            let curentController  = RCARHelpViewController()
            curentController.RCARHelp = "http://aramobile.com/projects/rcar/rcar.html"
            center.pushViewController(curentController, animated: true)
            
            break;
        case 2 :
       
            let curentController  = RCARHelpViewController()
            curentController.RCARHelp = "http://aramobile.com/projects/rcar/aide.html"
            center.pushViewController(curentController, animated: true)
            break;
        case 3 :
            
            if let url = URL(string: "tel://0801008888"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            
            break;
        case 4 :
            UIApplication.shared.openURL(NSURL(string: "https://www.facebook.com/prevoyanceCDG")! as URL)
            break;
        case 5 :
            UIApplication.shared.openURL(NSURL(string: "http://linkedin.com/company/cdg-pr%C3%A9voyance")! as URL)
            break;
            
        default : break;
        }
        
    }
    // MARK: IBAction
    
    @IBAction func pushAuthController(button :UIButton) {
        
        let center : UINavigationController = (UIApplication.shared.delegate as! AppDelegate).sideMenuViewController!.centerViewController as! UINavigationController
        let controller :PagerLoginViewController = PagerLoginViewController()
        center.pushViewController(controller, animated: true)
        (UIApplication.shared.delegate as! AppDelegate).sideMenuViewController!.toggle(MMDrawerSide.left, animated: true, completion: nil)
        
    }
    
 
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 }
 
