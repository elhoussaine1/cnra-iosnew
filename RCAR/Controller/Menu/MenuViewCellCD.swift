//
//  MenuViewCellCD.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 01/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class MenuViewCellCD: UITableViewCell {
    @IBOutlet weak var titleCell: UILabel!
    @IBOutlet weak var imageCell: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
