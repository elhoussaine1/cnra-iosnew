//
//  MenuViewCell.swift
//  ClashFoot
//
//  Created by lotfi on 22/10/2015.
//  Copyright (c) 2015 AraMobile. All rights reserved.
//

import UIKit

class MenuViewCell: UITableViewCell {
    
    @IBOutlet weak var titleCell : UILabel!
    @IBOutlet weak var imageCell : UIImageView!
    @IBOutlet weak var rowView : UIView!
    @IBOutlet weak var separateurView : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
