//
//  BaseViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 06/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

func randomString(length: Int) -> String {
    
    let letters : NSString = "0123456789"
    let len = UInt32(letters.length)
    
    var randomString = ""
    for _ in 0 ..< length {
        let rand = arc4random_uniform(len)
        var nextChar = letters.character(at: Int(rand))
        randomString += NSString(characters: &nextChar, length: 1) as String
    }
    return randomString
}

class BaseViewController: UIViewController {
    
    // MARK: Properties
    let userDefaults:UserDefaults = UserDefaults.standard
    let loader = Utils.getHUDIndeterminate()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate


    override func viewDidLoad() {
        super.viewDidLoad()

        let navigation = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        let topViewController : UIViewController = navigation.topViewController!
        let leftButton = UIButton(type:UIButton.ButtonType.custom)
        var aSelector : Selector
     
        let image = UIImage(named: "Logo_Action")
        let imageView = UIImageView(image: image!)
        
        let iconWidth : CGFloat = navigation.view.frame.width * 0.25
        let iconheight : CGFloat = navigation.view.frame.height * 0.5
        let marginX: CGFloat = (navigation.view.frame.width / 2) - (iconWidth / 2)
        imageView.frame = CGRect(x: marginX , y: 0, width:iconWidth, height: iconheight)
        imageView.contentMode = .scaleAspectFit
        navigationItem.titleView = imageView

   
        switch topViewController {
        case is HomeViewController,is PagerLoginViewController:
        
            // left button
            aSelector = #selector(BaseViewController.menuButton(sender:))
            leftButton.addTarget(self, action: aSelector, for: UIControl.Event.touchUpInside)
            let menuImage : UIImage = UIImage(named: "Side_Menu.png" as String)!
            leftButton.setImage(menuImage, for: UIControl.State.normal)
            leftButton.frame = CGRect(x: 0,y: 0,width: menuImage.size.width,height : menuImage.size.height)
            
            let leftDrawerButton = UIBarButtonItem(customView: leftButton)
             self.navigationItem.setLeftBarButton(leftDrawerButton, animated: true)
            
        default:
           
            navigation.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            navigation.navigationItem.backBarButtonItem?.tintColor = UIColor.black
        }
    }
    
    @IBAction func menuButton(sender: AnyObject) {
        appDelegate.sideMenuViewController!.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    @IBAction func back(sender: AnyObject) {
        let center : UINavigationController = (UIApplication.shared.delegate as! AppDelegate).sideMenuViewController!.centerViewController as! UINavigationController
        center.popViewController(animated: true)
    }
    
    func randomString(length: Int) -> String {
      
        let letters : NSString = "0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }
    
    func getSavedImage() -> UIImage? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent("personPic.jpg").path)
        }
        return nil
    }
    
    
    func addHour(date:String) -> String {
        
        let isoDate = String(date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "ar_MA") // set locale to reliable US_POSIX
        let datedd = dateFormatter.date(from:isoDate)!
        
        
        if let dateWithTime = Calendar.current.date(byAdding:
            .hour, // updated this params to add hours
            value: 1,
            to: datedd) {
            
            return dateFormatter.string(from: dateWithTime)
           
           }
        
        return ""

}
    
     func takeScreenshot() -> UIImage? {
        
        var screenshotImage :UIImage?
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        guard let context = UIGraphicsGetCurrentContext() else {return nil}
        layer.render(in:context)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return screenshotImage
    }
    
    func saveImage(image: UIImage) -> Bool {
        guard let data = image.jpegData(compressionQuality: 1) ?? image.pngData() else {
            return false
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return false
        }
        do {
            try data.write(to: directory.appendingPathComponent("personPic.jpg")!)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }

}





