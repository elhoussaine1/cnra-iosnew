//
//  detailsRGTableViewCell.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 30/10/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class detailsRGTableViewCell: UITableViewCell {

    @IBOutlet weak var yearUILabel: UILabel!
    @IBOutlet weak var quarterUILabel: UILabel!
    @IBOutlet weak var monthUILabel: UILabel!
    @IBOutlet weak var dayUILabel: UILabel!
    @IBOutlet weak var membershipfeeUILabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
