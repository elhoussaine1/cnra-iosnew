//
//  RGTableViewCell.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 06/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class RGTableViewCell: UITableViewCell {

    @IBOutlet weak var containerUIView: UIView!
    @IBOutlet weak var anneeUILabel: UILabel!
    @IBOutlet weak var moisUILabel: UILabel!
    @IBOutlet weak var joursUILabel: UILabel!
    @IBOutlet weak var cotisationUILabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
