//
//  TableViewHeaderFooterView.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 21/11/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class TableViewHeaderFooterView: UITableViewCell {

   
    @IBOutlet weak var DaterecrutUILabel: UILabel!
    @IBOutlet weak var profilUILabel: UILabel!
    @IBOutlet weak var NomPrenomUILabel: UILabel!
    @IBOutlet weak var OrganismeemployeurUILabel: UILabel!
    @IBOutlet weak var NAffiliationUILabel: UILabel!
    @IBOutlet weak var MatriculeUILabel: UILabel!
    @IBOutlet weak var NadhésionUILabel: UILabel!
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
