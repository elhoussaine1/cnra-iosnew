//
//  RGTableViewHeaderFooterView.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 21/11/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class RGTableViewHeaderFooterView: UITableViewCell {

    @IBOutlet weak var profilUILabel: UILabel!
    @IBOutlet weak var nomAffilieUILabel: UILabel!
    @IBOutlet weak var organismeUILabel: UILabel!
    @IBOutlet weak var noAffiliationUILabel: UILabel!
    @IBOutlet weak var matriculeUILabel: UILabel!
    @IBOutlet weak var dateRecrutementUILabel: UILabel!
    @IBOutlet weak var noAdhesionUILabel: UILabel!
    @IBOutlet weak var periodeValideeUILabel: UILabel!
    @IBOutlet weak var cotisationValidationUILabel: UILabel!
    @IBOutlet weak var periodeRachatUILabel: UILabel!
    @IBOutlet weak var perioderecheteeUILabel: UILabel!
    @IBOutlet weak var pensiontransfereeUILabel: UILabel!
    @IBOutlet weak var pensionrevaloriseeUILabel: UILabel!
    @IBOutlet weak var dureep1UILabel: UILabel!
    
}
