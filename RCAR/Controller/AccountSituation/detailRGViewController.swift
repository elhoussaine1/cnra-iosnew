//
//  detailRGViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 30/10/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class detailRGViewController: BaseViewController {
    
    // MARK: Properties
    
    var  year : String!
    var  contributionRGDtosArray = [contributionRGDtos]()
 
    // Mark: - Outlets
    @IBOutlet weak var fullNameUILabel: UILabel!
    @IBOutlet weak var pensionNumberUILabel: UILabel!
    @IBOutlet weak var membershipfeeExpyTableView: ExpyTableView!
    // Mark: - Actions
    
    override func viewDidLoad() {
        super.viewDidLoad()
         membershipfeeExpyTableView.register(UINib(nibName: "detailsRGTableViewCell", bundle: nil), forCellReuseIdentifier: "detailsRGTableViewCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        
       loadContentView()
       loadsubscriptiondetails()
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func loadContentView(){
        
          let fullName = Profil.currentProfile().name as String
          let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces) as String
        
        fullNameUILabel.text =  fullName
        pensionNumberUILabel.text =  identifiant
        
        membershipfeeExpyTableView.dataSource = self
        membershipfeeExpyTableView.delegate = self
        membershipfeeExpyTableView.rowHeight = UITableView.automaticDimension
        membershipfeeExpyTableView.estimatedRowHeight = 44
        membershipfeeExpyTableView.tableFooterView = UIView()
        membershipfeeExpyTableView.separatorStyle = .none
        membershipfeeExpyTableView.reloadData()
        
    }

}
//MARK: ExpyTableViewDataSourceMethods
extension detailRGViewController: ExpyTableViewDataSource {
    
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: detailsRGTableViewCell.self)) as! detailsRGTableViewCell
        
        cell.yearUILabel.text = contributionRGDtosArray[section].annee
        cell.quarterUILabel.text = contributionRGDtosArray[section].trimestre
        cell.monthUILabel.text = contributionRGDtosArray[section].mois
        cell.dayUILabel.text  = contributionRGDtosArray[section].jours
        cell.membershipfeeUILabel.text = contributionRGDtosArray[section].cotisation
        
   
        return cell
    }
}
//MARK: ExpyTableView delegate methods
extension detailRGViewController: ExpyTableViewDelegate {
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
    }
}



extension detailRGViewController {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
       /* let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
        let controller : detailFolderViewController = detailFolderViewController()
        controller.folder = FolderArray[indexPath.section]
        NavigationController.pushViewController(controller, animated: true)*/
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK: UITableView Data Source Methods
extension detailRGViewController {
    func numberOfSections(in tableView: UITableView) -> Int {
        return contributionRGDtosArray.count
    
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ExpandedCell.self)) as! ExpandedCell
            cell.layoutMargins = UIEdgeInsets.zero
            return cell
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ExpandedCell.self)) as! ExpandedCell
            cell.layoutMargins = UIEdgeInsets.zero
            return cell
        }
    }
}

extension detailRGViewController {

    func loadsubscriptiondetails() {
        if Reachability.isConnectedToNetwork(){
            
            loader?.show(in: appDelegate.window)
            
            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
            let Year = year
            AccountsituationApiClient.subscriptiondetails(identifiant : identifiant,year : Year!, success: { ContributionRGDtosArray in
                DispatchQueue.main.async {
                    
                    if(ContributionRGDtosArray.count > 0){
                        
                        self.contributionRGDtosArray.removeAll()
                        self.contributionRGDtosArray = ContributionRGDtosArray
                        self.membershipfeeExpyTableView.reloadData()
                        
                    }else{
                        self.view.makeToast("Impossible de charger les données pour le moment, veuillez réessayer plus tard", duration: 1.0, position: .center)
                    }
                    
                    self.loader?.dismiss()
                    
                }
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                    self.view.makeToast(apiError, duration: 1.0, position: .center)
                    
                }
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
    
}



