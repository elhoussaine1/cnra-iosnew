//
//  SupplementaryRegimeViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 07/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class SupplementaryRegimeViewController: BaseViewController {
    
    // MARK: Properties
    var index           : Int               = 0
    var contributionRCDtosArray = [contributionRCDtos]()
    var accountsituationRCDto = AccountsituationRC()
    // Mark: - Outlets
   
    @IBOutlet weak var RCExpyTableView: UITableView!
    
    // Mark: - Actions

    override func viewDidLoad() {
        super.viewDidLoad()
           RCExpyTableView.register(UINib(nibName: "RCTableViewCell", bundle: nil), forCellReuseIdentifier: "RCTableViewCell")
        
           RCExpyTableView.register(UINib(nibName: "TableSectionHeader", bundle: nil), forCellReuseIdentifier: "TableViewHeaderFooterView")
        loadContentView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
         AccountsituationRCFun()
    }
    
    func loadContentView() {
        
        RCExpyTableView.dataSource = self
        RCExpyTableView.delegate = self
        
        RCExpyTableView.rowHeight = UITableView.automaticDimension
        RCExpyTableView.estimatedRowHeight = 44
        RCExpyTableView.tableFooterView = UIView()
        
        //Delete lines between UITableViewCells
        RCExpyTableView.separatorStyle = .none
    }
    
}

private extension SupplementaryRegimeViewController {
    
    func AccountsituationRCFun() {
        if Reachability.isConnectedToNetwork(){
        
            loader?.show(in: appDelegate.window)
            
            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
            
            AccountsituationApiClient.AccountsituationRCDtos(identifiant: identifiant, success: { accountsituationRC in
                DispatchQueue.main.async {
                    
                    if(accountsituationRC.noAffiliation != ""){

                        //filling RGExpyTableView
                        self.accountsituationRCDto = accountsituationRC
                        self.contributionRCDtosArray = accountsituationRC.cotisationRCDtos
                        self.RCExpyTableView.reloadData()

                    }else{
                        self.view.makeToast("Impossible de charger les données pour le moment, veuillez réessayer plus tard", duration: 1.0, position: .center)
                    }
                    
                    self.loader?.dismiss()
     
                }
                
                
            }, failure: { apiError in
                self.loader?.dismiss()
            
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
}
}

//MARK: ExpyTableViewDataSourceMethods
extension SupplementaryRegimeViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contributionRCDtosArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row == 0){
            
             let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewHeaderFooterView") as! TableViewHeaderFooterView
             cell.selectionStyle = .none
            
            if(accountsituationRCDto.nomAffilie != nil){
                
                cell.profilUILabel.text = Profil.currentProfile().profiltype as String
                cell.NomPrenomUILabel.text = accountsituationRCDto.nomAffilie as String
                cell.OrganismeemployeurUILabel.text = accountsituationRCDto.organisme as String
                cell.NAffiliationUILabel.text = accountsituationRCDto.noAffiliation as String
                cell.MatriculeUILabel.text = accountsituationRCDto.matricule as String
                cell.DaterecrutUILabel.text = accountsituationRCDto.dateRecrutement as String
                cell.NadhésionUILabel.text = accountsituationRCDto.noAdhesion as String
                
            }
            
            return cell

        }
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: RCTableViewCell.self)) as! RCTableViewCell
        let indexValue = indexPath.row - 1
        cell.containerUIView.layer.cornerRadius = 5
        cell.selectionStyle = .none
        cell.containerUIView.addshadowColor(shadowColor: UIColor.black)
        cell.anneeUILabel.text = contributionRCDtosArray[indexValue].annee
        cell.cotisationAffiliationUILabel.text = contributionRCDtosArray[indexValue].cotisationAffiliation
        cell.cotisationValidationUILabel.text = contributionRCDtosArray[indexValue].cotisationValidation
        cell.nbrPointAcquisUILabel.text = contributionRCDtosArray[indexValue].nbrPointAcquis
        return cell
    }
  
}








