//
//  OverallRegimeViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 07/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class OverallRegimeViewController: BaseViewController {
    

    // MARK: Properties
    var index           : Int               = 0
    var contributionRGDtosArray = [contributionRGDtos]()
    var accountsituationRGDto = AccountsituationRG()
    // Mark: - Outlets
    
    @IBOutlet weak var RGExpyTableView: UITableView!
    // Mark: - Actions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RGExpyTableView.register(UINib(nibName: "RGTableViewCell", bundle: nil), forCellReuseIdentifier: "RGTableViewCell")
             RGExpyTableView.register(UINib(nibName: "RGTableViewHeaderView", bundle: nil), forCellReuseIdentifier: "RGTableViewHeaderFooterView")
        loadContentView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AccountsituationRGfunc()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadContentView() {
        
        RGExpyTableView.dataSource = self
        RGExpyTableView.delegate = self
      
        RGExpyTableView.rowHeight = UITableView.automaticDimension
        RGExpyTableView.estimatedRowHeight = 44
        RGExpyTableView.tableFooterView = UIView()
        
        //Delete lines between UITableViewCells
        RGExpyTableView.separatorStyle = .none
    }
    
}

private extension OverallRegimeViewController {
    
    func AccountsituationRGfunc() {
        if Reachability.isConnectedToNetwork(){
            
            loader?.show(in: appDelegate.window)
            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
            
            AccountsituationApiClient.AccountsituationRGDtos(identifiant: identifiant, success: { accountsituationRG in
                DispatchQueue.main.async {
                    
                    
                    if(accountsituationRG.noAffiliation != ""){
                    
                        
                        //filling RGExpyTableView
                        self.accountsituationRGDto = accountsituationRG
                        self.contributionRGDtosArray = accountsituationRG.cotisationRGDtos
                        self.RGExpyTableView.reloadData()
                        
                        
                    }else{
                        self.view.makeToast("Impossible de charger les données pour le moment, veuillez réessayer plus tard", duration: 1.0, position: .center)
                    }
                    
                    self.loader?.dismiss()

                }
                
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.view.makeToast(apiError, duration: 1.0, position: .center)
                    self.loader?.dismiss()
                }
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
}

//MARK: ExpyTableViewDataSourceMethods
extension OverallRegimeViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row == 0){
            
             let cell = tableView.dequeueReusableCell(withIdentifier: "RGTableViewHeaderFooterView") as! RGTableViewHeaderFooterView
             cell.selectionStyle = .none
            
            if(accountsituationRGDto.nomAffilie != nil){
                
                let nomprenom = accountsituationRGDto.nomAffilie as String
                let organisme = accountsituationRGDto.organisme as String
                let noAffiliation = accountsituationRGDto.noAffiliation as String
                let matricule = accountsituationRGDto.matricule as String
                let dateRecrutement = accountsituationRGDto.dateRecrutement as String
                let noAdhesion = accountsituationRGDto.noAdhesion as String
                let periodeValidee = accountsituationRGDto.periodeValidee as String
                let cotisationValidation = accountsituationRGDto.cotisationValidation as String
                let periodeRachat = accountsituationRGDto.cotisationRachat as String
                let perioderechetee = accountsituationRGDto.periodeRachat as String
                let pensiontransferee = accountsituationRGDto.montantAcqui as String
                let pensionrevalorisee = accountsituationRGDto.montantAcquiRevalorise as String
                let dureeAA = accountsituationRGDto.dureeAA as String
                let dureeMM = accountsituationRGDto.dureeMM as String
                let  profil = Profil.currentProfile().profiltype as String
                
                cell.profilUILabel.text =  profil
                cell.nomAffilieUILabel.text =  nomprenom
                cell.organismeUILabel.text = organisme
                cell.noAffiliationUILabel.text = noAffiliation
                cell.matriculeUILabel.text = matricule
                cell.dateRecrutementUILabel.text = dateRecrutement
                cell.noAdhesionUILabel.text = noAdhesion
                cell.periodeValideeUILabel.text =  periodeValidee
                cell.cotisationValidationUILabel.text = cotisationValidation
                cell.periodeRachatUILabel.text = periodeRachat
                cell.perioderecheteeUILabel.text =  perioderechetee
                cell.pensiontransfereeUILabel.text =  pensiontransferee
                cell.pensionrevaloriseeUILabel.text =  pensionrevalorisee
                cell.dureep1UILabel.text =  "\(dureeAA) Années et \(dureeMM) Mois"
            }
            
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: RGTableViewCell.self)) as! RGTableViewCell
        let indexValue = indexPath.row - 1
        cell.containerUIView.layer.cornerRadius = 5
        cell.selectionStyle = .none
        cell.containerUIView.addshadowColor(shadowColor: UIColor.black)
        cell.layoutMargins = UIEdgeInsets.zero
        cell.anneeUILabel.text = contributionRGDtosArray[indexValue].annee
        cell.joursUILabel.text = contributionRGDtosArray[indexValue].jours
        cell.moisUILabel.text = contributionRGDtosArray[indexValue].mois
        cell.cotisationUILabel.text = contributionRGDtosArray[indexValue].cotisation
        
        return cell
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contributionRGDtosArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        if(indexPath.row != 0){
        
        let indexvalue = indexPath.row - 1
        let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
        let controller : detailRGViewController = detailRGViewController()
        controller.year = contributionRGDtosArray[indexvalue].annee
        NavigationController.pushViewController(controller, animated: true)
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}







