//
//  ComplaintViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 06/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class ComplaintViewController: BaseViewController, UITextFieldDelegate,UITextViewDelegate  {
    
    // MARK: Properties
    
    var keybordHeight   : CGFloat!
    var keyboard        : Bool              = false
    var selectTextField : UITextField!      = UITextField()
    
    // Mark: - Outlets
    @IBOutlet weak var textfield: Textfield!
    @IBOutlet weak var emailUITextField: Textfield!
    @IBOutlet weak var addressUITextField: Textfield!
    @IBOutlet weak var cityUITextField: Textfield!
    @IBOutlet weak var countryUITextField: Textfield!
    @IBOutlet weak var phoneUITextField: Textfield!
    @IBOutlet weak var GSmUItextField: Textfield!
    @IBOutlet weak var descriptionUITextView: UITextView!
    
    
    // Mark: - Actions
    @IBAction func nextUIButton(_ sender: Any) {
        
        if self.isComplFormValid() == true {
            if Reachability.isConnectedToNetwork(){
                loader?.show(in: appDelegate.window)
                
                //addComplaint
                ComplaintApiClient.addComplaint(requestData(), completionHandler: {  success in
                    if (success.trimmingCharacters(in: .whitespaces) == "success"){
                        
                        DispatchQueue.main.async {
                            self.loader?.dismiss()
                            self.view.makeToast("Votre réclamation a été envoyée avec succés", duration: 1.0, position: .center)
                        }
                    }
                    
                }, failure: { apiError in
                    DispatchQueue.main.async {
                        self.loader?.dismiss()
                    }
                })
            }else{
                self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
            }
        }
    }
    
    init() {
        super.init(nibName: "ComplaintViewController", bundle: nil)
    }
    required   init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadContentView()
        self.getBeneficiryAffiliatedCor()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        descriptionUITextView.text = "Objet de réclamation *"
        descriptionUITextView.textColor = UIColor.lightGray
        
    }
    
    
    func requestData() -> URLRequestParams {
        var data = URLRequestParams()
        data["nom"] = Profil.currentProfile().name
        data["mail"] = emailUITextField.txtField.text
        data["adresse"] = addressUITextField.txtField.text
        data["ville"] = cityUITextField.txtField.text
        data["pays"] = countryUITextField.txtField.text
        data["telephone"] = phoneUITextField.txtField.text
        data["gsm"] = GSmUItextField.txtField.text
        data["identifiant"] = Profil.currentProfile().identifiant
        data["reclamation"] = descriptionUITextView.text
        data["profil"] = Profil.currentProfile().profiltype
        data["produit"] = "RCAR"
        data["canal"] = "MOBILE"
        return data
    }
    
    
    
    // MARK: - Methods
    func isComplFormValid() -> Bool {
        var isValid     =   true
        
        if self.emailUITextField.txtField.text == "" {
            isValid = false
            self.emailUITextField.showError("Merci d'entrer votre email")
        }else
            if self.addressUITextField.txtField.text == "" {
                isValid = false
                self.addressUITextField.showError("Merci d'entrer votre adresse")
            }else
                if self.cityUITextField.txtField.text == "" {
                    isValid = false
                    self.cityUITextField.showError("Merci d'entrer votre ville")
                }else
                    if self.countryUITextField.txtField.text == "" {
                        isValid = false
                        self.countryUITextField.showError("Merci d'entrer votre pays")
                    }else
                        if self.phoneUITextField.txtField.text == "" {
                            isValid = false
                            self.phoneUITextField.showError("Merci d'entrer votre numéro du téléphone")
        }
        
        
        return isValid
    }
    
    
    
    func loadContentView() {
        
        self.emailUITextField.txtField.delegate    = self
        self.addressUITextField.txtField.delegate        = self
        self.cityUITextField.txtField.delegate       = self
        self.countryUITextField.txtField.delegate     = self
        self.phoneUITextField.txtField.delegate       = self
        self.descriptionUITextView.delegate = self
        self.emailUITextField.lbldiscription.text         = "Email@exemple.com *"
        self.addressUITextField.lbldiscription.text       = "Adresse"
        self.cityUITextField.lbldiscription.text           = "Ville"
        self.countryUITextField.lbldiscription.text      = "Pays *"
        self.phoneUITextField.lbldiscription.text            = "Numéro de Téléphone"
        self.GSmUItextField.lbldiscription.text = "GSM"
        
        self.emailUITextField.txtField.keyboardType   = UIKeyboardType.emailAddress
        self.phoneUITextField.txtField.keyboardType   = UIKeyboardType.phonePad
        self.GSmUItextField.txtField.keyboardType   = UIKeyboardType.phonePad
        
     
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Réclamation"
            textView.textColor = UIColor.lightGray
        }
    }
    
    
    
}


private extension ComplaintViewController {

    func getBeneficiryAffiliatedCor() {
        if Reachability.isConnectedToNetwork(){
            loader?.show(in: appDelegate.window)
            
            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
            let profil = Profil.currentProfile().profiltype.trimmingCharacters(in: .whitespaces)
            
            ComplaintApiClient.getBeneficiryAffiliatedCor(profil : profil ,identifiant: identifiant, success: { beneficiry in
                DispatchQueue.main.async {
                    
                 
                    self.emailUITextField.txtField.text = beneficiry.mail
                    self.addressUITextField.txtField.text = beneficiry.adresse
                    self.cityUITextField.txtField.text = beneficiry.ville
                    self.countryUITextField.txtField.text = beneficiry.pays
                    self.phoneUITextField.txtField.text = beneficiry.telephone
                    self.GSmUItextField.txtField.text = beneficiry.gsm
                    self.descriptionUITextView.text = beneficiry.desc

                    self.loader?.dismiss()
                }
                
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                }
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
}
