//
//  AgencyActivationViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 24/09/2020.
//  Copyright © 2020 Aramobile. All rights reserved.
//

import UIKit

protocol AgencyActivationViewControllerDelegate {
    func SelectPageControl(index: Int)
    func GetcodeActivation(codeActivation: String)
}

class AgencyActivationViewController: BaseViewController,LZViewPagerDelegate, LZViewPagerDataSource  {
    
    private var subControllers:[UIViewController] = []
    
    var codeActivation:String!
    let vc3 = AgencyPassportActivationViewController3()

    @IBOutlet weak var viewPager: LZViewPager!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        reloadviewPager(codeActivation: "")
    }
    
    func reloadviewPager(codeActivation:String){
        viewPager.dataSource = self
        viewPager.delegate = self
        viewPager.hostController = self

        let vc1 = AgencyActivationViewController1()
        vc1.agencyActivationViewControllerDelegate = self
        
        let vc2 = AgencyActivationViewController2()
        vc2.agencyActivationViewControllerDelegate = self
     
        vc3.agencyActivationViewControllerDelegate = self
        vc3.fromPassport = false
        vc3.codeActivation = codeActivation
        vc3.lifeControl = false
       
        let vc4 = AgencyActivationViewController4()
        subControllers = [vc1, vc2, vc3, vc4]
        
        viewPager.reload()
    }

}

extension AgencyActivationViewController:AgencyActivationViewControllerDelegate{
    func SelectPageControl(index: Int) {
        viewPager.select(index: index)
    }
    
    func GetcodeActivation(codeActivation: String){
        reloadviewPager(codeActivation: codeActivation)
    }
    
    func numberOfItems() -> Int {
        return self.subControllers.count
    }
    
    func controller(at index: Int) -> UIViewController {
        return subControllers[index]
    }
    
    func button(at index: Int) -> UIButton {
        let button = UIButton()
        return button
    }
}
