//
//  AgencyActivationViewController1.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 24/09/2020.
//  Copyright © 2020 Aramobile. All rights reserved.
//

import UIKit

class AgencyActivationViewController1: BaseViewController {
  
    var agencyActivationViewControllerDelegate: AgencyActivationViewControllerDelegate!
    
    @IBAction func ActivateServiceAction(_ sender: UIButton) {
        self.agencyActivationViewControllerDelegate.SelectPageControl(index: 1)
    }
    
    @IBAction func PrevButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
}


