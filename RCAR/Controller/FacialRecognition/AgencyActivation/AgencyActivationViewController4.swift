//
//  AgencyActivationViewController4.swift
//  RCAR
//
//  Created by El houssaine EL gamouz on 24/09/2020.
//  Copyright © 2020 Aramobile. All rights reserved.
//

import UIKit

class AgencyActivationViewController4: BaseViewController {

    @IBOutlet weak var containerView: UIView!
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView.addshadowColor(shadowColor: UIColor.black)
    }
}
