//
//  AgencyActivationViewController2.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 24/09/2020.
//  Copyright © 2020 Aramobile. All rights reserved.
//

import UIKit

class AgencyActivationViewController2: BaseViewController,UITextFieldDelegate {

    var agencyActivationViewControllerDelegate: AgencyActivationViewControllerDelegate!
    
    @IBOutlet weak var codeTextField: Textfield!
    @IBOutlet weak var containerView: UIView!
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        if(codeTextField.txtField.text != ""){
     
            let produit = "RCAR"
            let numClient = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
            let Activationcode =  codeTextField.txtField.text!.trimmingCharacters(in: .whitespaces) as String
            CheckCodeActivation(produit: produit, numClient: numClient, codeActivation: Activationcode, token:"")
        
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        containerView.addshadowColor(shadowColor: UIColor.black)
        self.codeTextField.txtField.delegate    = self
        self.codeTextField.txtField.keyboardType = .numberPad
        self.codeTextField.lbldiscription.text         = "Tapez votre code"
    }
    
}

extension AgencyActivationViewController2{
    func CheckCodeActivation(produit:String,numClient:String,codeActivation:String,token:String){
        
        if Reachability.isConnectedToNetwork(){
            loader?.show(in: appDelegate.window)
            FacialRecognition.CheckCodeActivation(produit: produit, numClient: numClient, codeActivation: codeActivation,token:token,  success: {  result in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                    
                    switch result {
                    case "success":
                        self.agencyActivationViewControllerDelegate.SelectPageControl(index: 2)
                        self.agencyActivationViewControllerDelegate.GetcodeActivation(codeActivation: self.codeTextField.txtField.text!.trimmingCharacters(in: .whitespaces) as String)
                    case "fail":
                        self.view.makeToast("Code erroné, veuillez réessayer de saisir un code valide ", duration: 1.0, position: .center)
                    default:
                        self.view.makeToast(result, duration: 1.0, position: .center)
                       
                    }
                }
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                }
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
}



