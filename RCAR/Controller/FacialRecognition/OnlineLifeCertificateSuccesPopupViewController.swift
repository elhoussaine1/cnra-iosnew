//
//  OnlineLifeCertificateSuccesPopupViewController.swift
//  RCAR
//
//  Created by El houssaine el gamouz on 26/11/2020.
//  Copyright © 2020 Aramobile. All rights reserved.
//

import UIKit

class OnlineLifeCertificateSuccesPopupViewController: BaseViewController {

    @IBOutlet weak var PupupContainer: UIView!
    
    @IBAction func endButtonAction(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.PupupContainer.layer.cornerRadius = 2
        self.PupupContainer.addshadowColor(shadowColor: UIColor.black)
    }
}
