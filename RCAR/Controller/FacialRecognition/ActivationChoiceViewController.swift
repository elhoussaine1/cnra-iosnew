//
//  ActivationChoiceViewController.swift
//  RCAR
//
//  Created by ab on 23/09/2020.
//  Copyright © 2020 Aramobile. All rights reserved.
//

import UIKit
import CoreNFC

class ActivationChoiceViewController: BaseViewController {
    @IBOutlet weak var passPortActivation: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if NFCNDEFReaderSession.readingAvailable == false {
            passPortActivation.image = passPortActivation.image?.withRenderingMode(.alwaysTemplate)
            passPortActivation.tintColor = UIColor.gray
            self.view.makeToast("Pour activer le service via la passeport, votre téléphone doit disposer de la technologie NFC.", duration: 2.0, position: .center)
        }
    }


    @IBAction func AgencyActivationAction(_ sender: UIButton) {
        let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
        let curentController  = AgencyActivationViewController() as AgencyActivationViewController
        NavigationController.pushViewController(curentController, animated: true)
    }
    
    @IBAction func PassportActivationAction(_ sender: UIButton) {
        if (NFCNDEFReaderSession.readingAvailable == true) {
            let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
            let curentController  = PassportActivationViewController() as PassportActivationViewController
            NavigationController.pushViewController(curentController, animated: true)
        }else{
            self.view.makeToast("Pour activer le service via la passeport, votre téléphone doit disposer de la technologie NFC.", duration: 2.0, position: .center)
        }
    }
    

}
