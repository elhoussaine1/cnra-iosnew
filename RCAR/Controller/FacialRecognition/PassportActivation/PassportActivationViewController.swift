//
//  PassportActivationViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 24/09/2020.
//  Copyright © 2020 Aramobile. All rights reserved.
//

import UIKit

class PassportActivationViewController:BaseViewController,LZViewPagerDelegate, LZViewPagerDataSource  {
    
    var imagePassport:UIImage!
    let vc3 = AgencyPassportActivationViewController3()
    
    private var subControllers:[UIViewController] = []
    
    @IBOutlet weak var viewPager: LZViewPager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reloadviewPager()
    }
    func reloadviewPager(){
        viewPager.dataSource = self
        viewPager.delegate = self
        viewPager.hostController = self
        
        let vc1 = PassportActivationViewController1()
        vc1.passportActivationViewControllerDelegate = self
        
        let vc2 = PassportActivationViewController2()
        vc2.passportActivationViewControllerDelegate = self
        
        vc3.fromPassport = true
        vc3.lifeControl = false
        vc3.PassportPerPicture = self.imagePassport
        vc3.passportActivationViewControllerDelegate = self
        
        let vc4 = PassportActivationViewController3()
        subControllers = [vc1,vc2,vc3,vc4]
        viewPager.reload()
    }
    
}

extension PassportActivationViewController:PassportActivationViewControllerDelegate{
    func SelectPageControl(index: Int) {
        viewPager.select(index: index)
    }
    
    func GetPassportImage(passportImage: UIImage) {
        self.imagePassport = passportImage
        reloadviewPager()
 
    }
    
    func numberOfItems() -> Int {
        return self.subControllers.count
    }
    
    func controller(at index: Int) -> UIViewController {
        return subControllers[index]
    }
    
    func button(at index: Int) -> UIButton {
        let button = UIButton()
        return button
    }
}

