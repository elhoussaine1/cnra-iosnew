//
//  PassportActivationViewController2.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 25/09/2020.
//  Copyright © 2020 Aramobile. All rights reserved.
//

import UIKit
import NFCPassportReader
import CoreNFC

class PassportActivationViewController2: BaseViewController, NFCTagReaderSessionDelegate{
    
    var dg1: Data?
    var dg2: Data?
    var sod: Data?
    var isTest: Bool?
    @IBOutlet weak var personPic: UIImageView!
    
    func tagReaderSessionDidBecomeActive(_ session: NFCTagReaderSession) {
    }
    
    func tagReaderSession(_ session: NFCTagReaderSession, didInvalidateWithError error: Error) {
    }
    
    func tagReaderSession(_ session: NFCTagReaderSession, didDetect tags: [NFCTag]) {
        
        let tag = tags.first!
        
        nfcTagReaderSession?.connect(to: tag) { (error: Error?) in
                if case let .iso7816(iso7816Tag) = tag {
                    let reader = Reader(tag: iso7816Tag, completionHandler: self.nfcReadingCompleted, progressHandler: self.progressHandler)
                    
                    if let doeValue = self.userDefaults.string(forKey: "doeValue") {
                        if let dobValue = self.userDefaults.string(forKey: "dobValue") {
                            if var passportNbr = self.userDefaults.string(forKey: "passportNbr") {
                                if passportNbr.count == 8 {
                                    passportNbr.append("<")
                                }
                                reader.setBacKeys(BacKeys(documentNumber: passportNbr, dateOfBirth: dobValue, dateOfExpiry:doeValue))
                            }
                        }
                    }
                    
                    reader.read()
                 }
         }
    }
    
    func progressHandler(progress: UInt) {
        nfcTagReaderSession?.alertMessage =  NSLocalizedString("Reading", comment:"") + " (" + String(progress) + " %)"
    }
    
    func nfcReadingCompleted(success: Bool, dg1: Data, dg2: Data, sod: Data) {
        
        if success {
            self.dg1 = dg1
            self.dg2 = dg2
            self.sod = sod
            
            nfcTagReaderSession?.invalidate()
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "segueResultViewController", sender: self)
            }
        } else {

            nfcTagReaderSession?.invalidate(errorMessage: NSLocalizedString("Reading failed, check the passport number, date of birth and date of expiry", comment:""))
        }
    }
    
    var passportActivationViewControllerDelegate : PassportActivationViewControllerDelegate!
  
    let passportReader = PassportReader()
    var nfcTagReaderSession: NFCTagReaderSession?
    
    @IBOutlet weak var ScanImage: UIImageView!
    @IBAction func scanButtonAction(_ sender: UIButton) {

        let passportNumber = UserDefaults.standard.string(forKey:"passportNumber" )! as String
        let dateOfBirth = UserDefaults.standard.string(forKey:"dateOfBirth")! as String
        let expiryDate = UserDefaults.standard.string(forKey:"expiryDate")! as String
        
        let mrzKey = getMRZKey(passportNumber: passportNumber, dateOfBirth: dateOfBirth, expiryDate: expiryDate)
        NFCpassportReader(mrzKey: mrzKey)
      
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let loadGif = UIImage.gifImageWithName("Passport")
        ScanImage.image = loadGif
    }
    
    func beginScanning() {
        var session = NFCTagReaderSession(pollingOption: .iso14443, delegate: self)
           session?.alertMessage = "Hold your iPhone near the ISO7816 tag to begin transaction."
           session?.begin()
       }

    func back(sender: UIBarButtonItem) {
    self.passportActivationViewControllerDelegate.SelectPageControl(index: 0)
    }
}
extension PassportActivationViewController2 {

    func NFCpassportReader(mrzKey:String){

        passportReader.readPassport(mrzKey: mrzKey, customDisplayMessage: { (displayMessage) in
            switch displayMessage {
                case .requestPresentPassport:
                    return "Tenez votre téléphone près d'un passeport."
                default:
                    // Return nil for all other messages so we use the provided default
                    return nil
            }
        }, completed: { (passport, error) in

            if let passport = passport {
              
                DispatchQueue.main.async {
                    self.personPic.image = passport.passportImage
                    self.passportActivationViewControllerDelegate.GetPassportImage(passportImage: passport.passportImage!)
                    self.passportActivationViewControllerDelegate.SelectPageControl(index: 2)
                }

            } else {
//                self.alertTitle = "Oops"
//                self.alertTitle = "\(error?.localizedDescription ?? "Unknown error")"
//                self.showingAlert = true
            }
        })
           }
    
    func getMRZKey(passportNumber:String,dateOfBirth:String,expiryDate:String) -> String {

            // Calculate checksums
            let passportNrChksum = calcCheckSum(passportNumber)
            let dateOfBirthChksum = calcCheckSum(dateOfBirth)
            let expiryDateChksum = calcCheckSum(expiryDate)
            
            let mrzKey = "\(passportNumber)\(passportNrChksum)\(dateOfBirth)\(dateOfBirthChksum)\(expiryDate)\(expiryDateChksum)"
            
            return mrzKey
        }
    
    func calcCheckSum( _ checkString : String ) -> Int {
           let characterDict  = ["0" : "0", "1" : "1", "2" : "2", "3" : "3", "4" : "4", "5" : "5", "6" : "6", "7" : "7", "8" : "8", "9" : "9", "<" : "0", " " : "0", "A" : "10", "B" : "11", "C" : "12", "D" : "13", "E" : "14", "F" : "15", "G" : "16", "H" : "17", "I" : "18", "J" : "19", "K" : "20", "L" : "21", "M" : "22", "N" : "23", "O" : "24", "P" : "25", "Q" : "26", "R" : "27", "S" : "28","T" : "29", "U" : "30", "V" : "31", "W" : "32", "X" : "33", "Y" : "34", "Z" : "35"]
           
           var sum = 0
           var m = 0
           let multipliers : [Int] = [7, 3, 1]
           for c in checkString {
               guard let lookup = characterDict["\(c)"],
                   let number = Int(lookup) else { return 0 }
               let product = number * multipliers[m]
               sum += product
               m = (m+1) % 3
           }
           
           return (sum % 10)
       }
}
