//
//  PassportActivationViewController3.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 1/8/21.
//  Copyright © 2021 Aramobile. All rights reserved.
//

import UIKit

class PassportActivationViewController3: BaseViewController {
 
    @IBOutlet weak var containerView: UIView!
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView.addshadowColor(shadowColor: UIColor.black)
    }
}
