//
//  PassportActivationViewController1.swift
//  RCAR
//
//  Created by El houssaine EL gamouz on 24/09/2020.
//  Copyright © 2020 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol PassportActivationViewControllerDelegate {
    func SelectPageControl(index: Int)
    func GetPassportImage(passportImage: UIImage)
}

class PassportActivationViewController1: BaseViewController,UITextFieldDelegate{
    
    let calendarPicker = EPCalendarPicker()
   
    @IBOutlet weak var expirationNotif: UILabel!
    @IBOutlet weak var passportNumberTextfield: Textfield!
    @IBOutlet weak var expirationDateTextfield: Textfield!
    @IBOutlet weak var birhtDateTextfield: Textfield!
    @IBOutlet weak var cinNumberTextfield: Textfield!
    @IBOutlet weak var containerView: UIView!
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get {
            return .portrait
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        get {
            return true
        }
    }
    

    @IBAction func StartScanAction(_ sender: Any) {
        if(isFormValid()){
        if(checkTimeStamp(date: self.expirationDateTextfield.txtField.text!)){
            expirationNotif.isHidden = true
            UserDefaults.standard.setValue( self.passportNumberTextfield.txtField.text, forKey: "passportNumber")
            UserDefaults.standard.setValue( self.birhtDateTextfield.txtField.text, forKey: "dateOfBirth")
            UserDefaults.standard.setValue( self.expirationDateTextfield.txtField.text, forKey: "expiryDate")
            UserDefaults.standard.setValue( self.cinNumberTextfield.txtField.text, forKey: "cnie")
            
            self.passportActivationViewControllerDelegate.SelectPageControl(index: 1)
        }else{
            expirationNotif.isHidden = false
        }
        }else{
            self.view.makeToast("Merci de remplir tous les champs", duration: 2.0, position: .center)
        }
    }
    

    var passportActivationViewControllerDelegate : PassportActivationViewControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView.addshadowColor(shadowColor: UIColor.black)
        
//        self.birhtDateTextfield.txtField.addTarget(self, action: #selector(calendarPickerFunction), for: .touchDown)
//        self.birhtDateTextfield.txtField.tag = 0
//
//        self.expirationDateTextfield.txtField.addTarget(self, action: #selector(calendarPickerFunction), for: .touchDown)
//        self.expirationDateTextfield.txtField.tag = 1
      
        let date = Date()
       // let calendarPicker = EPCalendarPicker(startYear: date.year(), endYear: date.year(), multiSelection: false, selectedDates: [])
        
        calendarPicker.calendarDelegate = self
        calendarPicker.startDate = Date()
        calendarPicker.hightlightsToday = true
        calendarPicker.showsTodaysButton = true
        calendarPicker.hideDaysFromOtherMonth = true
        calendarPicker.tintColor = Utils.color(forHex: "000000")
        calendarPicker.weekdayTintColor = Utils.color(forHex: "FFFFFF")
        calendarPicker.dateSelectionColor = Utils.color(forHex: "5F8D38")
        calendarPicker.todayTintColor = Utils.color(forHex: "5F8D38")
        calendarPicker.dayDisabledTintColor = UIColor.gray
        calendarPicker.title = "Séléctionner une date"
        calendarPicker.backgroundColor = Utils.color(forHex: "424A50")
       

        self.passportNumberTextfield.txtField.delegate    = self
        self.passportNumberTextfield.lbldiscription.text         = "Numéro du passeport"
       
        self.expirationDateTextfield.txtField.delegate    = self
        self.expirationDateTextfield.lbldiscription.text         = "Date d'expiration"
        self.expirationDateTextfield.txtField.placeholder = "YYMMDD"
        
        self.birhtDateTextfield.txtField.delegate    = self
        self.birhtDateTextfield.lbldiscription.text         = "Date de naissance"
        self.birhtDateTextfield.txtField.placeholder = "YYMMDD"
        
        self.cinNumberTextfield.txtField.delegate    = self
        self.cinNumberTextfield.lbldiscription.text         = "N° CIN"
    }
    
    func checkTimeStamp(date: String!) -> Bool {
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyMMdd"
            dateFormatter.locale = Locale(identifier: "ar_MA")
            let datecomponents = dateFormatter.date(from: date)

            let now = Date()

            if (datecomponents! > now) {
                return true
            } else {
                return false
            }
        }
    
    func isFormValid() -> Bool {
        var isValid     =   true
        
        self.passportNumberTextfield.txtField.text == "" ? (isValid = false) : (isValid = true)
        self.expirationDateTextfield.txtField.text == "" ? (isValid = false) : (isValid = true)
        self.birhtDateTextfield.txtField.text == "" ? (isValid = false) : (isValid = true)
        self.cinNumberTextfield.txtField.text == "" ? (isValid = false) : (isValid = true)
      
        return isValid
    }
   
    
//    @objc func calendarPickerFunction(textField: UITextField) {
//        let navigationController = UINavigationController(rootViewController: calendarPicker)
//        self.present(navigationController, animated: true, completion: nil)
//    }
}

extension PassportActivationViewController1 :EPCalendarPickerDelegate {
    func epCalendarPicker(_: EPCalendarPicker, didCancel error : NSError) {
        
    }
    func epCalendarPicker(_: EPCalendarPicker, didSelectDate date : Date) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        //resultDate = formatter.string(from: date)
        
    }
    func epCalendarPicker(_: EPCalendarPicker, didSelectMultipleDate dates : [Date]) {
        // txtViewDetail.text = "User selected dates: \n\(dates)
    }
}

