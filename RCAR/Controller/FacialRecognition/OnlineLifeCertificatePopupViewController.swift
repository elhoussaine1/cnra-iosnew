//
//  OnlineLifeCertificatePopupViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 17/11/2020.
//  Copyright © 2020 Aramobile. All rights reserved.
//

import UIKit

class OnlineLifeCertificatePopupViewController: BaseViewController {
    
    var backgroundIImage :UIImage!
    
    @IBOutlet weak var topTitle: UILabel!
    @IBOutlet weak var middleTitle: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var backGorundImageView: UIImageView!
    @IBOutlet weak var checkBox: CheckBox!
    
    @IBAction func CancelButtonAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
        if(checkBox.isChecked){
            UserDefaults.standard.set(false, forKey: "ClCheckBox")
        }
    }
    
    @IBAction func ClCheckBox(_ sender: Any) {
         //Bool
    }
    
    
    @IBAction func ActivateButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        
        let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
        
        if(self.getSavedImage() != nil){
            let curentController  = OnlineLifeCertificateViewController() as OnlineLifeCertificateViewController
            NavigationController.pushViewController(curentController, animated: true)
        }else{
            NavigationController.pushViewController(ActivationChoiceViewController(), animated: true)
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backGorundImageView.image = backgroundIImage
        // Do any additional setup after loading the view.
    }
 
}
