//
//  OnlineLifeCertificateViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 02/12/2020.
//  Copyright © 2020 Aramobile. All rights reserved.
//

import UIKit

class OnlineLifeCertificateViewController: BaseViewController {

    @IBAction func startButtonAction(_ sender: UIButton) {
                let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
                let curentController  = AgencyPassportActivationViewController3() as AgencyPassportActivationViewController3
                curentController.lifeControl = true
                NavigationController.pushViewController(curentController, animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
