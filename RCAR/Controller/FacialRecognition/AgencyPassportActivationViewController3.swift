//
//  AgencyActivationViewController3.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 24/09/2020.
//  Copyright © 2020 Aramobile. All rights reserved.
//
import UIKit
import SFaceCompare
import CoreMedia
import AVFoundation
import CoreImage
import Vision
class AgencyPassportActivationViewController3: BaseViewController,AVCaptureVideoDataOutputSampleBufferDelegate {
    
    private let captureSession = AVCaptureSession()
    private lazy var previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
    private let videoDataOutput = AVCaptureVideoDataOutput()
    private var drawings: [CAShapeLayer] = []
    
    var Compareimages = [UIImage]()
    var PersonPicture:UIImage!
    var takePictureTimer: Timer?
    
    var fromPassport = false
    var PictureHasAFace = false
    var lifeControl = true
    var codeActivation:String!
    var PassportPerPicture:UIImage!
    var incrementWaitTime = 0
    
    var agencyActivationViewControllerDelegate: AgencyActivationViewControllerDelegate!
    var passportActivationViewControllerDelegate:PassportActivationViewControllerDelegate!
    
    @IBOutlet weak var CoverView: UIView!
    @IBOutlet weak var takePicturePopUp: UIView!
    @IBOutlet weak var takePicturePopUpImage: UIImageView!
    @IBOutlet weak var respectCriteriaViewContainer: UIView!
    @IBOutlet weak var videoContainer: UIView!
    @IBOutlet weak var PhotoContainerView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var personpicture: UIImageView!
    
    @IBAction func respectCriteriaButtonAction(_ sender: UIButton) {
        self.respectCriteriaViewContainer.isHidden = true
        takePictureTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(takePicture), userInfo: nil, repeats: true)
    }
    
    @IBAction func cancelBUttonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func restartTakePictureAction(_ sender: UIButton) {
        takePicturePopUp.isHidden = true
        CoverView.isHidden = true
        takePictureTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(takePicture), userInfo: nil, repeats: true)
    }
    
    @IBAction func submitTakePictureAction(_ sender: UIButton) {
        self.Compareimages.removeAll()
        if(self.takePicturePopUpImage.image != nil){
            self.Compareimages.append(self.takePicturePopUpImage.image!)
            if(self.lifeControl == true){
                self.Compareimages.append(self.getSavedImage()!)
                self.CompareFaces()
            }else{
                if(self.fromPassport == false){
                    ActivateFacePrint()
               }else{
                    self.Compareimages.append(self.PassportPerPicture)
                    self.CompareFaces()
                }
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.previewLayer.frame = self.videoContainer.bounds
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView.addshadowColor(shadowColor: UIColor.black)
        PhotoContainerView.layer.cornerRadius = PhotoContainerView.frame.size.height / 2.0
        PhotoContainerView.layer.masksToBounds = true
        self.addCameraInput()
        self.showCameraFeed()
        self.getCameraFrames()
        self.captureSession.startRunning()
        videoContainer.layer.addSublayer(self.previewLayer)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        takePictureTimer?.invalidate()
    }
    
    @objc func takePicture()
    {
        if(self.PictureHasAFace == true){
                takePicturePopUp.isHidden = false
                CoverView.isHidden = false
                takePicturePopUpImage.image = PersonPicture
                takePictureTimer?.invalidate()
        }else{
            self.view.makeToast("Le visage doit être visible dans le cadre", duration: 2.0, position: .center)
            incrementWaitTime = incrementWaitTime + 5
            if(incrementWaitTime == 15){
                self.view.makeToast("Aucun visage détecté", duration: 2.0, position: .center)
                takePictureTimer?.invalidate()
                incrementWaitTime = 0
                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func captureOutput(
        _ output: AVCaptureOutput,
        didOutput sampleBuffer: CMSampleBuffer,
        from connection: AVCaptureConnection) {
        
        guard let frame = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            debugPrint("unable to get image from sample buffer")
            return
        }
        self.detectFace(in: frame)
    }
    
    private func addCameraInput() {
        guard let device = AVCaptureDevice.DiscoverySession(
            deviceTypes: [.builtInWideAngleCamera, .builtInDualCamera, .builtInTrueDepthCamera],
            mediaType: .video,
            position: .front).devices.first else {
                fatalError("No back camera device found, please make sure to run SimpleLaneDetection in an iOS device and not a simulator")
        }
        let cameraInput = try! AVCaptureDeviceInput(device: device)
        self.captureSession.addInput(cameraInput)
    }
    
    private func showCameraFeed() {
        self.previewLayer.videoGravity = .resizeAspectFill
        self.videoContainer.layer.addSublayer(self.previewLayer)
        self.previewLayer.frame = self.videoContainer.bounds
    }
    
    private func getCameraFrames() {
        self.videoDataOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString) : NSNumber(value: kCVPixelFormatType_32BGRA)] as [String : Any]
        self.videoDataOutput.alwaysDiscardsLateVideoFrames = true
        self.videoDataOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "camera_frame_processing_queue"))
        self.captureSession.addOutput(self.videoDataOutput)
        guard let connection = self.videoDataOutput.connection(with: AVMediaType.video),
            connection.isVideoOrientationSupported else { return }
        connection.videoOrientation = .portrait
    }
    
    private func detectFace(in image: CVPixelBuffer) {
        let faceDetectionRequest = VNDetectFaceLandmarksRequest(completionHandler: { (request: VNRequest, error: Error?) in
            DispatchQueue.main.async {
                
                if let results = request.results as? [VNFaceObservation] {
                    self.handleFaceDetectionResults(results)
                    let image = CIImage(cvPixelBuffer: image)
                    self.PersonPicture = nil
                    self.PersonPicture = self.convert(cmage: image)
                } else {
                    self.clearDrawings()
                }
            }
        })
        let imageRequestHandler = VNImageRequestHandler(cvPixelBuffer: image, orientation: .leftMirrored, options: [:])
        try? imageRequestHandler.perform([faceDetectionRequest])
    }
    
    private func handleFaceDetectionResults(_ observedFaces: [VNFaceObservation]) {
        
        self.clearDrawings()
        self.PictureHasAFace = false
        let facesBoundingBoxes: [CAShapeLayer] = observedFaces.flatMap({ (observedFace: VNFaceObservation) -> [CAShapeLayer] in
            
            let faceBoundingBoxOnScreen = self.previewLayer.layerRectConverted(fromMetadataOutputRect: observedFace.boundingBox)
            let faceBoundingBoxPath = CGPath(rect: faceBoundingBoxOnScreen, transform: nil)
            let faceBoundingBoxShape = CAShapeLayer()
            faceBoundingBoxShape.path = faceBoundingBoxPath
            faceBoundingBoxShape.fillColor = UIColor.clear.cgColor
            faceBoundingBoxShape.strokeColor = UIColor(rgb: 0x648D2F).cgColor
            var newDrawings = [CAShapeLayer]()
            newDrawings.append(faceBoundingBoxShape)
            
            if let landmarks = observedFace.landmarks {
                newDrawings = newDrawings + self.drawFaceFeatures(landmarks, screenBoundingBox: faceBoundingBoxOnScreen)
                self.PictureHasAFace = true
            }
            return newDrawings
        })
        
        facesBoundingBoxes.forEach({ faceBoundingBox in
            self.previewLayer.addSublayer(faceBoundingBox) })
        self.drawings = facesBoundingBoxes
    }
    
    private func clearDrawings() {
        self.drawings.forEach({ drawing in drawing.removeFromSuperlayer() })
    }
    
    private func drawFaceFeatures(_ landmarks: VNFaceLandmarks2D, screenBoundingBox: CGRect) -> [CAShapeLayer] {
        var faceFeaturesDrawings: [CAShapeLayer] = []
        
        if let leftEye = landmarks.leftEye {
            let eyeDrawing = self.drawEye(leftEye, screenBoundingBox: screenBoundingBox)
            faceFeaturesDrawings.append(eyeDrawing)
        }
        
        if let rightEye = landmarks.rightEye {
            let eyeDrawing = self.drawEye(rightEye, screenBoundingBox: screenBoundingBox)
            faceFeaturesDrawings.append(eyeDrawing)
        }
    
        // draw other face features here
        return faceFeaturesDrawings
    }
    private func drawEye(_ eye: VNFaceLandmarkRegion2D, screenBoundingBox: CGRect) -> CAShapeLayer {
        let eyePath = CGMutablePath()
        let eyePathPoints = eye.normalizedPoints
            .map({ eyePoint in
                CGPoint(
                    x: eyePoint.y * screenBoundingBox.height + screenBoundingBox.origin.x,
                    y: eyePoint.x * screenBoundingBox.width + screenBoundingBox.origin.y)
            })
        eyePath.addLines(between: eyePathPoints)
        eyePath.closeSubpath()
        let eyeDrawing = CAShapeLayer()
        eyeDrawing.path = eyePath
        eyeDrawing.borderWidth = 3.0
        eyeDrawing.fillColor = UIColor.clear.cgColor
        eyeDrawing.strokeColor = UIColor(rgb: 0x648D2F).cgColor
        
        return eyeDrawing
    }
    
    func convert(cmage:CIImage) -> UIImage
    {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    
    func requestLifeControl() -> URLRequestParams {
        var data = URLRequestParams()
    
        data["produit"] = "rcar"
        data["matricule"] = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
        
        if(UserDefaults.standard.string(forKey: "DeviceToken") != ""){
        data["token"] = UserDefaults.standard.string(forKey: "DeviceToken")
        }
       
        if(UserDefaults.standard.string(forKey: "faceprintkey") != ""){
        data["keyFacePrint"] = UserDefaults.standard.string(forKey: "faceprintkey")
        }
       
        return data
    }
    
    func requestActivateFacePrint() -> URLRequestParams {
        var data = URLRequestParams()
        
        data["produit"] = "rcar"
        data["numClient"] = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
        if(UserDefaults.standard.string(forKey: "DeviceToken") != ""){
            data["token"] = UserDefaults.standard.string(forKey: "DeviceToken")
        }
        data["codeActivation"] = self.codeActivation
        
        return data
    }
    
    func PassportrequestActivateFacePrintData() -> URLRequestParams {
        var data = URLRequestParams()
        
        let dateOfBirth = UserDefaults.standard.string(forKey:"dateOfBirth")! as String
        let expiryDate = UserDefaults.standard.string(forKey:"expiryDate")! as String
        let cnie = UserDefaults.standard.string(forKey:"cnie")! as String
   
        data["produit"] = "rcar"
        data["numClient"] = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
        data["dateNaissance"] = dateOfBirth
        data["cnie"] = cnie
        data["dateExpiration"] = expiryDate
        if(UserDefaults.standard.string(forKey: "DeviceToken") != ""){
            data["token"] = UserDefaults.standard.string(forKey: "DeviceToken")
            }

        return data
    }
}

extension AgencyPassportActivationViewController3 {
    
    func CompareFaces(){
        
        guard Compareimages.count == 2 else { return }
        loader?.show(in: appDelegate.window)
      
        // 1: Create compare object
        let faceComparator = SFaceCompare(on: self.Compareimages[0], and: self.Compareimages[1])
        
        faceComparator.compareFaces { [weak self] result in
               switch result {
               case .failure(let error):
                self?.view.backgroundColor = UIColor.red
                self?.loader!.dismiss()
                case .success(let data):
                self?.loader!.dismiss()
                self?.view.backgroundColor = UIColor.green
                if(self?.lifeControl == true){
                if(UserDefaults.standard.string(forKey: "faceprintkey") != ""){
                    self?.getLifeControl()
                }}else{
                    self?.PassportActivateFacePrint()
               }
             }
        }

    }
}

extension AgencyPassportActivationViewController3{
    
    func PassportActivateFacePrint(){
        
        if Reachability.isConnectedToNetwork(){
            loader?.show(in: appDelegate.window)
            FacialRecognition.ActivatePassPortFacePrint(PassportrequestActivateFacePrintData(), success: {  result in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                    if(result != "fail"){
                        if(self.saveImage(image: self.PassportPerPicture)){
                            self.userDefaults.removeObject(forKey: "faceprintkey")
                            self.userDefaults.set(result, forKey: "faceprintkey")
                            self.passportActivationViewControllerDelegate.SelectPageControl(index: 3)
                        }
                    }
                }
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                }
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
    
    func ActivateFacePrint(){
        
      if Reachability.isConnectedToNetwork(){
                loader?.show(in: appDelegate.window)
   
      FacialRecognition.ActivateFacePrint(requestActivateFacePrint(), success: {  result in
                    DispatchQueue.main.async {
                        self.loader?.dismiss()
                        if(result != "fail"){
                            if(self.saveImage(image: self.takePicturePopUpImage.image!)){
                                self.userDefaults.removeObject(forKey: "faceprintkey")
                                self.userDefaults.set(result, forKey: "faceprintkey")
                                self.agencyActivationViewControllerDelegate.SelectPageControl(index: 3)
                            }
                        }
                    }
                }, failure: { apiError in
                    DispatchQueue.main.async {
                        self.loader?.dismiss()
                    }
                })
            }else{
                self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
            }
    }

    func getLifeControl(){
        
        if Reachability.isConnectedToNetwork(){
            loader?.show(in: appDelegate.window)
            FacialRecognition.lifeControl(requestLifeControl(), success: {  result in
                DispatchQueue.main.async {
                    if(result != "fail"){
                    self.loader?.dismiss()
                    let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
                    let curentController  = OnlineLifeCertificateSuccesPopupViewController() as OnlineLifeCertificateSuccesPopupViewController
                    NavigationController.pushViewController(curentController, animated: true)
                    }
                }
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                }
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
}
