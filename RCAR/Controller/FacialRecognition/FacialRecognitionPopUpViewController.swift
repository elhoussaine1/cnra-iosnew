//
//  FacialRecognitionPopUpViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 23/09/2020.
//  Copyright © 2020 Aramobile. All rights reserved.
//

import UIKit

class FacialRecognitionPopUpViewController: BaseViewController {
    
    var backgroundIImage :UIImage!
    
    @IBOutlet weak var backGorundImageView: UIImageView!
    @IBOutlet weak var checkBox: CheckBox!
    
    @IBAction func CancelButtonAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func ClCheckBox(_ sender: Any) {
    }
   

    @IBAction func ActivateButtonAction(_ sender: UIButton) {
        let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
        let curentController  = ActivationChoiceViewController() as ActivationChoiceViewController
        NavigationController.pushViewController(curentController, animated: true)
        self.dismiss(animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backGorundImageView.image = backgroundIImage
        // Do any additional setup after loading the view.
    }
}
