//
//  FollowUpFolderCell.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 05/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class FollowUpFolderCell: UITableViewCell {

    @IBOutlet weak var containerUIView: UIView!
    @IBOutlet weak var NumberUILabel: UILabel!
    @IBOutlet weak var requestNatureUILabel: UILabel!
    @IBOutlet weak var receptionDateRCARUILabel: UILabel!
    @IBOutlet weak var stapeUILabel: UILabel!
    @IBOutlet weak var forecastDateUILabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
