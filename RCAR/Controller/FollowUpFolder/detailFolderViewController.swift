//
//  detailFolderViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 18/10/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class detailFolderViewController: BaseViewController {
    
    
    @IBOutlet weak var treatyLibele: UILabel!
    @IBOutlet weak var beginTrai: UILabel!
    @IBOutlet weak var acceptLibele: UILabel!
    @IBOutlet weak var recievedLibele: UILabel!
    
    
    @IBOutlet weak var treatyDate: UILabel!
    @IBOutlet weak var dateEncoursUILabel: UILabel!
    @IBOutlet weak var dateAccepteUILabel: UILabel!
    @IBOutlet weak var dateRecuUILabel: UILabel!
    
    

    @IBOutlet weak var numeroDossierUILabel: UILabel!
    @IBOutlet weak var canalUILabel: UILabel!
 
    @IBOutlet weak var encourInformsUILabel: UILabel!
    @IBOutlet weak var accepteInformsUILabel: UILabel!
    @IBOutlet weak var reçuInformsUILabel: UILabel!
    
    var folder:Folder!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getFolderDetails()
        loadContentView()
    }
    
    func loadContentView(){
        numeroDossierUILabel.text = folder.numero as String
        canalUILabel.text = folder.canal as String
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension detailFolderViewController{
    
    func getFolderDetails() {
        if Reachability.isConnectedToNetwork(){
            
            loader?.show(in: appDelegate.window)
            
            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
            var numero = folder.numero
            
            
            FolderApiClient.getFolderDetails(identifiant: identifiant, numero: numero!,  success: { folderDetails in
                DispatchQueue.main.async {
                    
                    self.treatyDate.text = self.addHour(date:String(folderDetails[0]["dateDebut"].stringValue))
                    self.dateEncoursUILabel.text = self.addHour(date:String(folderDetails[1]["dateDebut"].stringValue))
                    self.dateAccepteUILabel.text = self.addHour(date:String(folderDetails[2]["dateDebut"].stringValue))
                    self.dateRecuUILabel.text = self.addHour(date:String(folderDetails[3]["dateDebut"].stringValue))
                    
                    self.treatyLibele.text = folderDetails[0]["type"].stringValue
                    self.beginTrai.text = folderDetails[1]["type"].stringValue
                    self.acceptLibele.text = folderDetails[2]["type"].stringValue
                    self.recievedLibele.text = folderDetails[3]["type"].stringValue
                    
                    self.loader?.dismiss()
                    
                }
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                    self.view.makeToast(apiError, duration: 1.0, position: .center)
                    
                }
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
}

