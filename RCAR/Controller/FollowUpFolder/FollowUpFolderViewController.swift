//
//  FollowUpFolderViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 08/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class FollowUpFolderViewController: BaseViewController {
    
    // MARK: Properties
     var FolderArray = [Folder]()
    // Mark: - Outlets
    
    @IBOutlet weak var followUpFolderUITableView: UITableView!
    // Mark: - Actions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadContentView()
        followUpFolderUITableView.register(UINib(nibName: "FollowUpFolderCell", bundle: nil), forCellReuseIdentifier: "FollowUpFolderCell")
       
    
    }
    override func viewWillAppear(_ animated: Bool) {
        FollowUpFolder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadContentView() {
        
        followUpFolderUITableView.dataSource = self
        followUpFolderUITableView.delegate = self
        
        followUpFolderUITableView.rowHeight = UITableView.automaticDimension
        followUpFolderUITableView.estimatedRowHeight = 44
        followUpFolderUITableView.tableFooterView = UIView()
        followUpFolderUITableView.separatorStyle = .none
    }
    
}

private extension FollowUpFolderViewController {
    
    func FollowUpFolder() {
        if Reachability.isConnectedToNetwork(){
       
            loader?.show(in: appDelegate.window)
         
            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
            let profil = Profil.currentProfile().profiltype.trimmingCharacters(in: .whitespaces)
            
            FolderApiClient.FollowUpFolder(identifiant: identifiant,profil: profil, success: { folderArray in
                DispatchQueue.main.async {
                  
                    if(folderArray.count > 0){
                        
                        self.FolderArray.removeAll()
                        self.FolderArray = folderArray
                        self.followUpFolderUITableView.reloadData()
                     
                    }else{
                        self.view.makeToast("Impossible de charger les données pour le moment, veuillez réessayer plus tard", duration: 1.0, position: .center)
                    }
                    
                    self.loader?.dismiss()
                 
                }
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                    self.view.makeToast(apiError, duration: 1.0, position: .center)
                   
                }
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
}



//MARK: ExpyTableViewDataSourceMethods
extension FollowUpFolderViewController: ExpyTableViewDataSource {
    
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FollowUpFolderCell.self)) as! FollowUpFolderCell
        
        cell.containerUIView.layer.cornerRadius = 5
        cell.selectionStyle = .none
        cell.containerUIView.addshadowColor(shadowColor: UIColor.black)
        cell.NumberUILabel.text = FolderArray[section].numero
        cell.requestNatureUILabel.text = FolderArray[section].type
        cell.receptionDateRCARUILabel.text = FolderArray[section].dateRecu.capitalized
        cell.stapeUILabel.text = FolderArray[section].dateSituation.capitalized
        
        cell.layoutMargins = UIEdgeInsets.zero
        // cell.showSeparator()
        return cell
    }
}


//MARK: ExpyTableView delegate methods
extension FollowUpFolderViewController: ExpyTableViewDelegate {
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
    }
}



extension FollowUpFolderViewController {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
        let controller : detailFolderViewController = detailFolderViewController()
        controller.folder = FolderArray[indexPath.section]
        NavigationController.pushViewController(controller, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK: UITableView Data Source Methods
extension FollowUpFolderViewController {
    func numberOfSections(in tableView: UITableView) -> Int {
        return FolderArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ExpandedCell.self)) as! ExpandedCell
            cell.layoutMargins = UIEdgeInsets.zero
            return cell
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ExpandedCell.self)) as! ExpandedCell
            cell.layoutMargins = UIEdgeInsets.zero
            return cell
        }
    }
}
