//
//  UpdateSituationViewController.swift
//  RCAR
//
//  Created by ab on 06/05/2020.
//  Copyright © 2020 Aramobile. All rights reserved.
//

import UIKit

class UpdateSituationViewController: BaseViewController,UITextFieldDelegate {
    
    var RecieveSMS = "0"

    @IBOutlet weak var RecieveSMSCheckBox: CheckBox!
    @IBOutlet weak var updateSituationContainer: UIView!
    @IBOutlet weak var placeOfBirth: Textfield!
    @IBOutlet weak var trackTxtfield: Textfield!
    @IBOutlet weak var cityLocationTextfield: Textfield!
    @IBOutlet weak var zipCodeTextfield: Textfield!
    @IBOutlet weak var countryTextfield: Textfield!
    @IBOutlet weak var InformationsUtilsContainer: UIView!
    @IBOutlet weak var telFixTextfield: Textfield!
    @IBOutlet weak var mailTextfield: Textfield!
    @IBOutlet weak var GsmTextfield: Textfield!
    
    @IBAction func simulateButtonAction(_ sender: UIButton) {
        
        if(RecieveSMSCheckBox.isChecked){
           
            if(isFormValid()){
                let profil = Profil.currentProfile().profiltype.trimmingCharacters(in: .whitespaces)
                RecieveSMS = "1"
                if(profil.lowercased() == "affilie"){
                    addAffiliateInformations()
                }else{
                    addBeneficiryInformations()
                }
            }
        }else{
            
            self.view.makeToast("Vous devez accepter l'option de réception SMS pour être informé de toute actualité vous concernant.", duration: 1.0, position: .center)
          
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

      // Do any additional setup after loading the view.
      updateSituationContainer.addshadowColor(shadowColor: UIColor.black)
      InformationsUtilsContainer.addshadowColor(shadowColor: UIColor.black)
      loadContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let profil = Profil.currentProfile().profiltype.trimmingCharacters(in: .whitespaces)
        if(profil.lowercased() == "affilie"){
          getAffiliateInformations()
        }else{
          getBeneficiryInformations()
        }
   
    }
    
    func loadContent(){
   
        self.placeOfBirth.txtField.delegate    = self
        self.trackTxtfield.txtField.delegate    = self
        self.cityLocationTextfield.txtField.delegate    = self
        self.zipCodeTextfield.txtField.delegate    = self
        self.countryTextfield.txtField.delegate    = self
    
        self.telFixTextfield.txtField.delegate    = self
        self.mailTextfield.txtField.delegate    = self
        self.GsmTextfield.txtField.delegate    = self
        
        self.placeOfBirth.lbldiscription.text         = "Lieu de naissance *"
        self.trackTxtfield.lbldiscription.text         = "Voie"
        self.cityLocationTextfield.lbldiscription.text         = "Ville / Localité *"
        self.zipCodeTextfield.lbldiscription.text         = "Code postale *"
        self.countryTextfield.lbldiscription.text         = "Pays *"
        
        self.telFixTextfield.lbldiscription.text         = "Tel fixe"
        self.mailTextfield.lbldiscription.text         = "Email"
        self.GsmTextfield.lbldiscription.text         = "GSM"

    }
    
    func affiliateRequestData() -> URLRequestParams {
    
        var data = URLRequestParams()
        data["identifiant"] = Profil.currentProfile().identifiant
        data["profil"] = Profil.currentProfile().profiltype
        data["lieuResidence"] = self.placeOfBirth.txtField.text
        data["voie"] = self.trackTxtfield.txtField.text
        data["localite"] = self.cityLocationTextfield.txtField.text
        data["codePostal"] = self.zipCodeTextfield.txtField.text
        data["pays"] = self.countryTextfield.txtField.text
        data["telFix1"] = self.telFixTextfield.txtField.text
        data["email"] = self.mailTextfield.txtField.text
        data["telGsm"] = self.GsmTextfield.txtField.text
        data["sms"] = RecieveSMS
        
        return data
    }
    
    func beneficiaryRequestData() -> URLRequestParams {
        
        var data = URLRequestParams()
    
        data["profil"] = Profil.currentProfile().profiltype
        data["lieuResidence"] = self.placeOfBirth.txtField.text
        data["voie"] = self.trackTxtfield.txtField.text
        data["localite"] = self.cityLocationTextfield.txtField.text
        data["codePostal"] = self.zipCodeTextfield.txtField.text
        data["pays"] = self.countryTextfield.txtField.text
        data["telFix1"] = self.telFixTextfield.txtField.text
        data["email"] = self.mailTextfield.txtField.text
        data["telGsm"] = self.GsmTextfield.txtField.text
        data["sms"] = RecieveSMS

        return data
    }
    
    func isFormValid() -> Bool {
        var isValid     =   true
        if self.placeOfBirth.txtField.text == "" {
            isValid = false
            self.placeOfBirth.showError("Merci d'entrer votre lieu de résidence")
        }else
            if self.cityLocationTextfield.txtField.text == "" {
                isValid = false
                self.cityLocationTextfield.showError("Merci d'entrer votre ville / Localité")
            }else
                if self.zipCodeTextfield.txtField.text == "" {
                    isValid = false
                    self.zipCodeTextfield.showError("Merci d'entrer votre Code postale")
                }else
                    if self.countryTextfield.txtField.text == "" {
                            isValid = false
                            self.countryTextfield.showError("Merci d'entrer votre Pays")
                        }
        return isValid
    }
}

private extension UpdateSituationViewController {
    
    func addBeneficiryInformations(){
        if Reachability.isConnectedToNetwork(){
            loader?.show(in: appDelegate.window)
            
            let profil = Profil.currentProfile().profiltype.trimmingCharacters(in: .whitespaces)
            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
            
            ComplaintApiClient.addBeneficiryInformations(beneficiaryRequestData(), profil: profil, identifiant: identifiant, completionHandler: {  success in
                if (success.trimmingCharacters(in: .whitespaces) == "success"){
                    
                    DispatchQueue.main.async {
                        self.loader?.dismiss()
                        self.view.makeToast("Votre demande a été enregistrée avec succès. Elle sera traitée incessamment.", duration: 1.0, position: .center)
                    }
                }
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                }
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
    
    func addAffiliateInformations(){
        if Reachability.isConnectedToNetwork(){
            loader?.show(in: appDelegate.window)
            
            let profil = Profil.currentProfile().profiltype.trimmingCharacters(in: .whitespaces)
            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
            
            AffiliatedApiClient.addAffiliatedInformations(affiliateRequestData(), profil: profil, identifiant: identifiant, completionHandler: {  success in
                if (success.trimmingCharacters(in: .whitespaces) == "success"){
                    
                    DispatchQueue.main.async {
                        self.loader?.dismiss()
                        self.view.makeToast("La mise à jour a été effectuée avec succes", duration: 1.0, position: .center)
                    }
                }
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                }
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
    
    func getAffiliateInformations() {
        if Reachability.isConnectedToNetwork(){
            
            loader?.show(in: appDelegate.window)
            
            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
            let profil = Profil.currentProfile().profiltype.trimmingCharacters(in: .whitespaces)
         
            AffiliatedApiClient.getAffiliateInformations(identifiant: identifiant, profil: profil,  success: { affiliatedCoordinate in
                DispatchQueue.main.async {
                    
             
                   self.placeOfBirth.txtField.text = affiliatedCoordinate.lieuResidence as String
                    
                   self.trackTxtfield.txtField.text = affiliatedCoordinate.voie as String
                   self.cityLocationTextfield.txtField.text = affiliatedCoordinate.localite as String 
                   self.zipCodeTextfield.txtField.text = affiliatedCoordinate.codePostal as String
                   self.countryTextfield.txtField.text = affiliatedCoordinate.pays as String
                   
                    self.telFixTextfield.txtField.text = affiliatedCoordinate.telFix1 as String
                    
                   self.mailTextfield.txtField.text = affiliatedCoordinate.email as String
                   self.GsmTextfield.txtField.text = affiliatedCoordinate.telGsm as String
                    self.loader?.dismiss()
                }
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                    self.view.makeToast(apiError, duration: 1.0, position: .center)
                    
                }
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
    
    func getBeneficiryInformations() {
        if Reachability.isConnectedToNetwork(){
            loader?.show(in: appDelegate.window)
            
            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
            let profil = "beneficiaire"
            
            ComplaintApiClient.getBeneficiryInformations(identifiant: identifiant, profil : profil, success: { beneficiry in
                DispatchQueue.main.async {
                    self.placeOfBirth.txtField.text = beneficiry.lieuResidence as String
                    self.trackTxtfield.txtField.text = beneficiry.voie as String
                    self.cityLocationTextfield.txtField.text = beneficiry.localite as String + beneficiry.ville as String
                    self.zipCodeTextfield.txtField.text = beneficiry.codePostal as String
                    self.countryTextfield.txtField.text = beneficiry.pays as String
                    self.telFixTextfield.txtField.text = beneficiry.telFix1 as String
                    self.mailTextfield.txtField.text = beneficiry.email as String
                    self.GsmTextfield.txtField.text = beneficiry.telGsm as String
                    
                    self.loader?.dismiss()
                }
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                }
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
    
}

