//
//  ProjectionRGViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 02/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class ProjectionRGViewController: BaseViewController {
    
    // MARK: Properties
    var index               : Int               = 0
    var simulationPRG       :Simulation!
    // Mark: - Outlets
    @IBOutlet weak var pensionTotalUILabel: UILabel!
    @IBOutlet weak var pensionTotalUIView: UIView!
    @IBOutlet weak var containerUIView: UIView!
    @IBOutlet weak var p_date_effetUILabel: UILabel!
    @IBOutlet weak var rg_p_tx_ant_ajUILabel: UILabel!
    @IBOutlet weak var rg_p_date_limite_ageUILabel: UILabel!
    @IBOutlet weak var rg_p_annee_mois_affUILabel: UILabel!
    @IBOutlet weak var rg_p_annee_mois_valUILabel: UILabel!
    @IBOutlet weak var rg_p_annee_mois_rach: UILabel!
    @IBOutlet weak var rg_p_samcrUILabel: UILabel!
    @IBOutlet weak var rg_p_d1UILabel: UILabel!
    @IBOutlet weak var rg_p_tx_pensionUILabel: UILabel!
    @IBOutlet weak var rg_p_p1_revaloriseUILabel: UILabel!
    @IBOutlet weak var rg_p_p2UILabel: UILabel!
    @IBOutlet weak var rg_p_pensionUILabel: UILabel!
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        loadContentView()
        
        p_date_effetUILabel.text = simulationPRG.p_date_effet
        rg_p_tx_ant_ajUILabel.text = simulationPRG.rg_p_tx_ant_aj
        rg_p_date_limite_ageUILabel.text = simulationPRG.rg_p_date_limite_age
        
        let rg_p_annee_aff = simulationPRG.rg_p_annee_aff as String
        let rg_p_mois_aff = simulationPRG.rg_p_mois_aff as String
        rg_p_annee_mois_affUILabel.text = "\(rg_p_annee_aff) Années et \(rg_p_mois_aff) Mois"
        let rg_p_annee_val = simulationPRG.rg_p_annee_val as String
        let rg_p_mois_val = simulationPRG.rg_p_mois_val as String
        rg_p_annee_mois_valUILabel.text = "\(rg_p_annee_val) Années et \(rg_p_mois_val) Mois"
        let rg_p_annee_rach = simulationPRG.rg_p_annee_rach as String
        let rg_p_mois_rach = simulationPRG.rg_p_mois_rach as String
        rg_p_annee_mois_rach.text = "\(rg_p_annee_rach) Années et \(rg_p_mois_rach) Mois"
        rg_p_samcrUILabel.text = simulationPRG.rg_p_samcr
        rg_p_d1UILabel.text = simulationPRG.rg_p_d1
        rg_p_tx_pensionUILabel.text = simulationPRG.rg_p_tx_pension
        rg_p_p1_revaloriseUILabel.text = simulationPRG.rg_p_p1_revalorise
        rg_p_p2UILabel.text = simulationPRG.rg_p_p2
        rg_p_pensionUILabel.text = simulationPRG.rg_p_pension
        
        let rg_p_pension = (simulationPRG.rg_p_pension as NSString).doubleValue
        let rc_p_pension = (simulationPRG.rc_p_pension as NSString).doubleValue
        let pensioTotal = simulationPRG.total as NSString
        pensionTotalUILabel.text =  "Pension total brut RG + RC = \(pensioTotal)"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadContentView() {
        containerUIView.layer.cornerRadius = 5
        containerUIView.addshadowColor(shadowColor: UIColor.black)

        pensionTotalUIView.layer.cornerRadius = 5
        pensionTotalUIView.addshadowColor(shadowColor: UIColor.black)
    }
    
}
