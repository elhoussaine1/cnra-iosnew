//
//  ProjectionRCViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 02/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class ProjectionRCViewController: BaseViewController {
    
    // MARK: Properties
    var index               : Int               = 0
    var simulationPRC       :Simulation!
    // Mark: - Outlets
    @IBOutlet weak var pensionTotalUILabel: UILabel!
    @IBOutlet weak var pensionTotalUIView: UIView!
    @IBOutlet weak var containerUIView: UIView!
    @IBOutlet weak var rc_p_date_limite_ageUILabel: UILabel!
    @IBOutlet weak var rc_p_tx_ant_ajUILabel: UILabel!
    @IBOutlet weak var rc_p_nb_point_rc_affUILabel: UILabel!
    @IBOutlet weak var rc_p_nb_point_rc_cont_valUILabel: UILabel!
    @IBOutlet weak var rc_p_nb_point_rc_valUILabel: UILabel!
    @IBOutlet weak var rc_p_val_pUILabel: UILabel!
    @IBOutlet weak var rc_p_pensionUILabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadContentView()
        rc_p_date_limite_ageUILabel.text = simulationPRC.rc_p_date_limite_age
        rc_p_tx_ant_ajUILabel.text = simulationPRC.rc_p_tx_ant_aj
        rc_p_nb_point_rc_affUILabel.text = simulationPRC.rc_p_nb_point_rc_aff
        rc_p_nb_point_rc_cont_valUILabel.text = simulationPRC.rc_p_nb_point_rc_cont_val
        rc_p_nb_point_rc_valUILabel.text = simulationPRC.rc_p_nb_point_rc_val
        rc_p_val_pUILabel.text = simulationPRC.rc_p_val_p!
        rc_p_pensionUILabel.text = simulationPRC.rc_p_pension
        
        let rg_p_pension = (simulationPRC.rg_p_pension as NSString).doubleValue
        let rc_p_pension = (simulationPRC.rc_p_pension as NSString).doubleValue
        let pensioTotal = simulationPRC.total as NSString
        pensionTotalUILabel.text =  "Pension total brut RG + RC = \(pensioTotal)"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadContentView() {
        containerUIView.layer.cornerRadius = 5
        containerUIView.addshadowColor(shadowColor: UIColor.black)
        
        pensionTotalUIView.layer.cornerRadius = 5
        pensionTotalUIView.addshadowColor(shadowColor: UIColor.black)
    }
    
}
