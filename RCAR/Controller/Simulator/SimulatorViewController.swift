//
//  SimulatorViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 06/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class SimulatorViewController: BaseViewController,UITextFieldDelegate {
    
    // MARK: Properties
    var  yearState :String = "2"
    let  OtherDate = UIDatePicker()
    
    // Mark: - Outlets
    
    @IBOutlet weak var profilUILabel: UILabel!
    @IBOutlet weak var otherLTHRadioButton: LTHRadioButton!
    @IBOutlet weak var otherValueTextfield: Textfield!
    @IBOutlet weak var is21yearsLTHRadioButton: LTHRadioButton!
    @IBOutlet weak var is60yearsLTHRadioButton: LTHRadioButton!
    @IBOutlet weak var is55yearsLTHRadioButton: LTHRadioButton!
    
    @IBOutlet weak var fullnameUILabel: UILabel!
    @IBOutlet weak var employerorganizationUILabel: UILabel!
    @IBOutlet weak var affiliationnumberUILabel: UILabel!
    @IBOutlet weak var registrationnumberUILabel: UILabel!
    @IBOutlet weak var recruitmentdateUILabel: UILabel!
    @IBOutlet weak var membershipnumberUILabel: UILabel!
    
    
    // Mark: - Actions
    
    
    @IBAction func simulateUIButton(_ sender: Any) {
        
        if Reachability.isConnectedToNetwork(){
           
            loader?.show(in: appDelegate.window)
            
            if (otherLTHRadioButton.isSelected){
              self.yearState = self.otherValueTextfield.txtField.text!
            }

            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
       
            AffiliatedApiClient.simulation(identifiant: identifiant,dateEffet: self.yearState, success: { simulation in
                DispatchQueue.main.async {
                  
                    self.loader?.dismiss()
                    let NavigationController  : UINavigationController    = self.appDelegate.sideMenuViewController!.centerViewController as! UINavigationController
                    let curentController  = SimulatorResultViewController() as SimulatorResultViewController
                    curentController.simulationResult = simulation
                    NavigationController.pushViewController(curentController, animated: true)
                }
                
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                }
            })
            
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
        
    }
    
    
    init() {
        super.init(nibName: "SimulatorViewController", bundle: nil)
    }
    required   init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadContentView()
        
       
        
        //radios collections
        is55yearsLTHRadioButton.translatesAutoresizingMaskIntoConstraints = false
        is60yearsLTHRadioButton.translatesAutoresizingMaskIntoConstraints = false
        is21yearsLTHRadioButton.translatesAutoresizingMaskIntoConstraints = false
        otherLTHRadioButton.translatesAutoresizingMaskIntoConstraints = false
        is55yearsLTHRadioButton.select()
        
        
        is55yearsLTHRadioButton.onSelect {
            self.yearState = "2"
            self.otherValueTextfield.textFieldDisabling(false)
            self.otherValueTextfield.txtField.text = ""
            self.otherLTHRadioButton.deselect(animated: false)
            self.is60yearsLTHRadioButton.deselect(animated: false)
            self.is21yearsLTHRadioButton.deselect(animated: false)
        }
        
        is60yearsLTHRadioButton.onSelect {
            self.yearState = "3"
            self.otherValueTextfield.textFieldDisabling(false)
            self.otherValueTextfield.txtField.text = ""
            self.otherLTHRadioButton.deselect(animated: false)
            self.is55yearsLTHRadioButton.deselect(animated: false)
            self.is21yearsLTHRadioButton.deselect(animated: false)
        }
        
        
        is21yearsLTHRadioButton.onSelect {
            self.yearState = "1"
            //self.otherValueTextfield.isHidden = true
            self.otherValueTextfield.textFieldDisabling(false)
            self.otherValueTextfield.txtField.text = ""
            self.otherLTHRadioButton.deselect(animated: false)
            self.is55yearsLTHRadioButton.deselect(animated: false)
            self.is60yearsLTHRadioButton.deselect(animated: false)
        }
        
        otherLTHRadioButton.onSelect {
            // self.otherValueTextfield.isHidden = false
            self.otherValueTextfield.textFieldDisabling(true)
            self.otherValueTextfield.txtField.becomeFirstResponder()
            //self.yearState = self.otherValueTextfield.txtField.text!
            self.is21yearsLTHRadioButton.deselect(animated: false)
            self.is55yearsLTHRadioButton.deselect(animated: false)
            self.is60yearsLTHRadioButton.deselect(animated: false)
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
         getAffiliated()
    }
    
    func loadContentView() {
        
        otherValueTextfield.txtField.addTarget(self, action: #selector(callDatePicker(textField:)), for: .editingDidBegin)
        OtherDate.backgroundColor =  Utils.color(forHex: "48575E")
        OtherDate.setValue(Utils.color(forHex: "FFFFFF"), forKey: "textColor")

        self.otherValueTextfield.txtField.delegate    = self
        self.otherValueTextfield.lbldiscription.text         = "JJ/MM/AAAA"
    }
    
    // MARK: - Methods
    func isSimulationFormValid() -> Bool {
        var isValid     =   true
        
        if self.otherValueTextfield.txtField.text == "" {
            isValid = false
            //self.otherValueTextfield.showError("Merci d'entrer votre email")
        }
        
        return isValid
    }
    
    @objc func callDatePicker(textField: UITextField) {
        
        OtherDate.datePickerMode = UIDatePicker.Mode.date
        otherValueTextfield.txtField.inputView = OtherDate
        OtherDate.addTarget(self, action: #selector(fillingotherdateTextfield), for: UIControl.Event.valueChanged)
    }
    
    @objc func fillingotherdateTextfield() {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let bithday = formatter.string(from: OtherDate.date)
        otherValueTextfield.txtField.text = bithday
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

private extension SimulatorViewController {
    
    func getAffiliated() {
        if Reachability.isConnectedToNetwork(){
            loader?.show(in: appDelegate.window)
           

            let  identifiant = Profil.currentProfile().identifiant.trimmingCharacters(in: .whitespaces)
            
            AffiliatedApiClient.getAffilie(identifiant: identifiant, success: { affiliated in
                DispatchQueue.main.async {
                    
                    if(affiliated.identifiant != ""){
                        //let nomprenom = affiliated.nomprenom as String
                        let organisme = affiliated.organisme as String
                        let identifiant = affiliated.identifiant as String
                        let matricule = affiliated.matricule as String
                        let dateRecrutement = affiliated.dateRecrutement as String
                        let noAdhesion = affiliated.noAdhesion as String
                        let  profil = Profil.currentProfile().profiltype as String
                        let fullName = Profil.currentProfile().name as String
                        self.profilUILabel.text =  profil
                        self.fullnameUILabel.text =  fullName
                        self.employerorganizationUILabel.text = organisme
                        self.affiliationnumberUILabel.text = identifiant
                        self.registrationnumberUILabel.text = matricule
                        self.recruitmentdateUILabel.text = dateRecrutement
                        self.membershipnumberUILabel.text = noAdhesion
                    }else{
                        self.view.makeToast("Impossible de charger les données pour le moment, veuillez réessayer plus tard", duration: 1.0, position: .center)
                    }
                    
                    self.loader?.dismiss()

                }
                
                
            }, failure: { apiError in
                DispatchQueue.main.async {
                    self.loader?.dismiss()
                }
            })
        }else{
            self.view.makeToast("Connexion Internet non disponible!", duration: 1.0, position: .center)
        }
    }
    
    
    
}


