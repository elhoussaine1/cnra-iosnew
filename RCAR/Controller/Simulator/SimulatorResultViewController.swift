//
//  SimulatorResultViewController.swift
//  RCAR
//
//  Created by El houssaine El gamouz  on 02/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit

class SimulatorResultViewController: BaseViewController , ViewPagerDelegate , ViewPagerDataSource {
    
    // MARK: Properties
    
    var simulationResult                 : Simulation!
    // var articlesList : NSMutableArray! = NSMutableArray()
    var tabsArray : NSMutableArray = NSMutableArray()
    var categoriesArray : NSMutableArray = NSMutableArray()
    var selectedIndex: Int = 0
    var Currentindex  : Int! = 0
    //let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var pageViewController  : UIPageViewController?
    var pagerController     : ViewPagerController!
    
    // Mark: - Outlets
    // Mark: - Actions
    
    
    
    init() {
        super.init(nibName: "SimulatorResultViewController", bundle: nil)
    }
    required   init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = ""
        self.categoriesArray.add("PROJECTION RG")
        self.categoriesArray.add("PROJECTION RC")
        
        for _ in 0..<self.categoriesArray.count {
            self.tabsArray.add(NSNull.self)
        }
        //self.categoriesArray.add("Matches")
        
        pagerController = ViewPagerController()
        pagerController.delegate = self
        pagerController.dataSource = self
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(!self.view.subviews.contains(pagerController.view)) {
            pagerController.view.frame = CGRect(x: 0, y: -21 , width: self.view.frame.size.width
                , height: self.view.frame.size.height + 21)
            // pagerController.view.frame = self.view.frame
            
            //update her for load pagerController in custom View
            self.view.addSubview(pagerController.view)
            self.pagerController.setNeedsReloadOptions()
            pagerController.reloadData()
            pagerController.view.backgroundColor = UIColor.white
            pagerController.selectTab(at: 0)
        }
    }
    
    
    // MARK: - Pager
    func viewPager(_ viewPager: ViewPagerController!, valueFor option: ViewPagerOption, withDefault value: CGFloat) -> CGFloat {
        switch (option) {
        case ViewPagerOption.startFromSecondTab:
            return 0.0
        case ViewPagerOption.centerCurrentTab:
            return 1.0
        case ViewPagerOption.tabLocation:
            return value
        case ViewPagerOption.tabHeight:
            return 48.0
        case ViewPagerOption.tabOffset:
            return 40.0
        case ViewPagerOption.tabWidth:
            return self.view.frame.size.width / 2
        //isPad()? 158. : 130.0;
        case ViewPagerOption.fixFormerTabsPositions:
            return 0.0
        case ViewPagerOption.tabLocation:
            return 1.0
        case ViewPagerOption.fixLatterTabsPositions:
            return 0.0
        default:
            return value;
        }
    }
    func viewPager(_ viewPager: ViewPagerController!, contentViewControllerForTabAt index: UInt) -> UIViewController! {

        if index == 0 {
            let controller : ProjectionRGViewController! = ProjectionRGViewController()
            controller.index = Int(index)
            controller.simulationPRG = simulationResult
            return controller
        }else{
            let controller : ProjectionRCViewController! = ProjectionRCViewController()
            controller.index = Int(index)
            controller.simulationPRC = simulationResult
            return controller
        }
    }
    
    func viewPager(_ viewPager: ViewPagerController!, viewForTabAt index: UInt) -> UIView! {
        let view : UIView = UIView()
        var frame : CGRect = CGRect(x: 0, y: 50, width: self.view.frame.size.width / 2, height: 40)
        view.frame = frame
        let label :UILabel = UILabel()
        // label
        label.text = NSLocalizedString(self.categoriesArray.object(at: Int(index)) as! String , comment: "")
        
        let font : UIFont       = UIFont.boldSystemFont(ofSize: 18)
        label.font              = font
        label.backgroundColor   = UIColor.clear
        label.textColor         = UIColor.white
        label.textAlignment     = NSTextAlignment.center
        label.font              = UIFont(name: "Roboto-Medium", size: 17)
        frame                   = CGRect(x: 3, y: 0, width: self.view.frame.size.width / 2, height: 40)
        label.frame             = frame
        label.tag               = 100
        view.addSubview(label)
        
        self.tabsArray.replaceObject(at: Int(index), with: view)
        
        return view
    }
    
    func numberOfTabs(forViewPager viewPager: ViewPagerController!) -> UInt {
        return UInt(self.categoriesArray.count)
    }
    
    func viewPager(_ viewPager: ViewPagerController!, colorFor component: ViewPagerComponent,
                   withDefault color: UIColor!) -> UIColor! {
        switch (component) {
        //  ViewPagerContent
        case ViewPagerComponent.indicator:
            return Utils.color(forHex: "598934")
        case ViewPagerComponent.content :
            return Utils.color(forHex: "373634")
        case ViewPagerComponent.tabsView :
            return  Utils.color(forHex: "FFFFFF")
        //UIColor(red: 3.0/255.0, green: 201.0/255.0, blue: 223.0/255.0, alpha: 1.0)
        default:
            return UIColor.clear//Utils.colorForHex("#028ade")
        }
    }
    func viewPager(_ viewPager: ViewPagerController!, didChangeTabTo index: UInt) {
        self.selectedIndex = Int(index)

        for i in 0 ..< self.categoriesArray.count {
            
            let view: UIView  = (self.tabsArray.object(at: i)) as! UIView
            
            if view.isKind(of: UIView.self){
                let label: UILabel = view.viewWithTag(100) as! UILabel
                if (i == self.selectedIndex){
                    label.textColor = Utils.color(forHex: "5B8D38")
                }
                else{
                    label.textColor = Utils.color(forHex: "C0C0C0")
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
