//
//  Profil.swift
//  ClashFoot
//
//  Created by lotfi on 08/09/2015.
//  Copyright (c) 2015 AraMobile. All rights reserved.
//

import UIKit
import SwiftyJSON

class Profil : NSObject , NSCoding {
    
    var identifiant       : String!
    var name              : String!
    var token_type        : String!
    var refresh_token     : String!
    var access_token      : String!
    var profiltype        : String!
    var pieceReclame      : String!
    var facePrintMenuActive:Bool!
    
    
    override init() {
        super.init()
    }
    
    init(fromJSON json: JSON){
        self.identifiant     = json["identifiant"].stringValue
        self.name             = json["name"].stringValue
        self.token_type          = json["token_type"].stringValue
        self.refresh_token   = json["refresh_token"].stringValue
        self.access_token             = json["access_token"].stringValue
        self.profiltype          = json["profil"].stringValue
        self.pieceReclame          = json["pieceReclame"].stringValue
        self.facePrintMenuActive   = json["facePrintMenuActive"].boolValue
    }
    
    internal  func encode(with encoder: NSCoder) {
        
        encoder.encode(self.identifiant, forKey: "identifiant")
        encoder.encode(self.name, forKey: "name")
        encoder.encode(self.token_type, forKey: "token_type")
        encoder.encode(self.refresh_token, forKey: "refresh_token")
        encoder.encode(self.access_token, forKey: "access_token")
        encoder.encode(self.profiltype, forKey: "profiltype")
        encoder.encode(self.pieceReclame, forKey: "pieceReclame")
        encoder.encode(self.facePrintMenuActive, forKey: "facePrintMenuActive")
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        identifiant      = aDecoder.decodeObject(forKey: "identifiant") as! String
        name           = aDecoder.decodeObject(forKey: "name") as! String
        token_type         = aDecoder.decodeObject(forKey:"token_type") as! String
        refresh_token       = aDecoder.decodeObject(forKey:"refresh_token") as! String
        access_token          = aDecoder.decodeObject(forKey:"access_token") as! String
        profiltype        = aDecoder.decodeObject(forKey:"profiltype") as! String
        pieceReclame        = aDecoder.decodeObject(forKey:"pieceReclame") as! String
        facePrintMenuActive = aDecoder.decodeObject(forKey:"facePrintMenuActive") as! Bool
        
    }
    
    
    static func currentProfile() -> Profil {
        if(KeychainWrapper.hasValueForKey(keyName: "profile")) {
            let profile: Profil = KeychainWrapper.objectForKey(keyName: "profile") as! Profil
            return profile
        }
        return Profil()
    }
    
    static func isLoggedIn() -> Bool {
        return KeychainWrapper.hasValueForKey(keyName: "profile")
    }
    
    static func logout() {
        KeychainWrapper.removeObjectForKey(keyName: "profile")
        
    }
    
    func save() {
        KeychainWrapper.setObject(value: self, forKey: "profile")
    }
    
}
