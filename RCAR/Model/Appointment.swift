//
//  Appointment.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 10/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON


class Appointment : NSObject , NSCoding  {
    
    
    // MARK: Properties
    var nomPrenom: String!
    var tel: String!
    var fax: String!
    var jour: String!
    var heure: String!
    var adresseMail: String!
    var objet: String!
    
    
    override init() {
        super.init()
    }
    
    init(fromJSON json: JSON){
        
   
        self.nomPrenom = json["nomPrenom"].stringValue
        self.tel = json["tel"].stringValue
        self.fax = json["fax"].stringValue
        self.jour = json["jour"].stringValue
        self.heure = json["heure"].stringValue
        self.adresseMail = json["adresseMail"].stringValue
        self.objet = json["objet"].stringValue
        
    }
    
    internal  func encode(with encoder: NSCoder) {
        
        
       
        encoder.encode(self.nomPrenom, forKey:"nomPrenom")
        encoder.encode(self.tel, forKey:"tel")
        encoder.encode(self.fax, forKey:"fax")
        encoder.encode(self.jour, forKey:"jour")
        encoder.encode(self.heure, forKey:"heure")
        encoder.encode(self.adresseMail, forKey:"adresseMail")
        encoder.encode(self.objet, forKey:"objet")
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
       
        nomPrenom = aDecoder.decodeObject(forKey: "nomPrenom") as! String
        tel = aDecoder.decodeObject(forKey: "tel") as! String
        fax = aDecoder.decodeObject(forKey: "fax") as! String
        jour = aDecoder.decodeObject(forKey: "jour") as! String
        heure = aDecoder.decodeObject(forKey: "heure") as! String
        adresseMail = aDecoder.decodeObject(forKey: "adresseMail") as! String
        objet = aDecoder.decodeObject(forKey: "objet") as! String
        
    }
    
    static func currentAppointment() -> Appointment {
        if(KeychainWrapper.hasValueForKey(keyName: "appointment")) {
            let appointment: Appointment = KeychainWrapper.objectForKey(keyName: "appointment") as! Appointment
            return appointment
        }
        return Appointment()
    }
    
    static func existAppointment() -> Bool {
        return KeychainWrapper.hasValueForKey(keyName: "appointment")
    }
    

    func remove() {
        KeychainWrapper.removeObjectForKey(keyName: "appointment")
        
    }
    
    static func cancelAppointment() -> Bool {
        if(KeychainWrapper.removeObjectForKey(keyName: "appointment")) {
       
            return true
        }
        return false
    }
    
    
    
    func save() {
        KeychainWrapper.setObject(value: self, forKey: "appointment")
    }
    
    
}
