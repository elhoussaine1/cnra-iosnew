//
//  Affiliated.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 03/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON


class Affiliated : NSObject {
    
    // MARK: Properties
    
    var  nomprenom: String!
    var identifiant: String!
    var matricule : String!
    var organisme : String!
    var noAdhesion: String!
    var dateRecrutement: String!
    
    override init() {
        super.init()
    }
    
    init(fromJSON json: JSON){
        
        self.nomprenom     = json["nomAffilie"].stringValue
        self.identifiant             = json["noAffiliation"].stringValue
        self.matricule          = json["matricule"].stringValue
        self.organisme   = json["organisme"].stringValue
        self.noAdhesion             = json["noAdhesion"].stringValue
        self.dateRecrutement          = json["dateRecrutement"].stringValue
        
        
    }
    
    
    static func currentAffiliated() -> Affiliated {
        if(KeychainWrapper.hasValueForKey(keyName: "Affiliated")) {
            let affiliated: Affiliated = KeychainWrapper.objectForKey(keyName: "Affiliated") as! Affiliated
            return affiliated
        }
        
        return Affiliated()
    }
    
    static func isLoggedIn() -> Bool {
        return KeychainWrapper.hasValueForKey(keyName: "Affiliated")
    }
    
    static func logout() {
        KeychainWrapper.removeObjectForKey(keyName: "Affiliated")
    }
    
    func save() {
        //  KeychainWrapper.setObject(value: self, forKey: "Affiliated")
    }
    
}

