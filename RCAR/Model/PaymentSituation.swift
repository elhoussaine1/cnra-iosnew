//
//  paymentSituation.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 15/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON

class PaymentSituation : NSObject  {
    
    var identifiant: String!
    var idBeneficiaire: String!
    var idCategorieBenef: String!
    var rangBeneficiaire: String!
    var matricule: String!
    var nom: String!
    var prenom: String!
    var cin: String!
    var noCompte: String!
    var idMutuelle: String!
    var flagIR: String!
    var flagExactitudeAdresse: String!
    var flagAbattement: String!
    var resident: String!
    var exactitudeAdresse: String!
    var adresseEntreeImm: String!
    var adresseVoie: String!
    var lieuDit: String!
    var localite: String!
    var province: String!
    var region: String!
    var codePostal: String!
    var flagSms: String!
    var creeLe: String!
    var matriculeMutuelle: String!
    var dateNaissance: String!
    var nbrEnfants: String!
    var nbrConjoints: String!
    var droitMcf: String!
    var dateEffetPens: String!
    var naturePension: String!
    var flagAmo: String!
    var flagLiq: String!
    var situationFamiliale: String!
    var paiements = [Payments]()
    var enfants = [Childs]()
    
    override init() {
        super.init()
    }
    
    init(fromJSON json: JSON){
        
        self.identifiant = json["identifiant"].stringValue
        self.idBeneficiaire = json["idBeneficiaire"].stringValue
        self.idCategorieBenef = json["idCategorieBenef"].stringValue
        self.rangBeneficiaire = json["rangBeneficiaire"].stringValue
        self.matricule = json["matricule"].stringValue
        self.nom = json["nom"].stringValue
        self.prenom = json["prenom"].stringValue
        self.cin = json["cin"].stringValue
        self.noCompte = json["noCompte"].stringValue
        self.idMutuelle = json["idMutuelle"].stringValue
        self.flagIR = json["flagIR"].stringValue
        self.flagExactitudeAdresse = json["flagExactitudeAdresse"].stringValue
        self.flagAbattement = json["flagAbattement"].stringValue
        self.resident = json["resident"].stringValue
        self.exactitudeAdresse = json["exactitudeAdresse"].stringValue
        self.adresseEntreeImm = json["adresseEntreeImm"].stringValue
        self.adresseVoie = json["adresseVoie"].stringValue
        self.lieuDit = json["lieuDit"].stringValue
        self.localite = json["localite"].stringValue
        self.province = json["province"].stringValue
        self.region = json["region"].stringValue
        self.codePostal = json["codePostal"].stringValue
        self.flagSms = json["flagSms"].stringValue
        self.creeLe = json["creeLe"].stringValue
        self.matriculeMutuelle = json["matriculeMutuelle"].stringValue
        self.dateNaissance = json["dateNaissance"].stringValue
        self.nbrEnfants = json["nbrEnfants"].stringValue
        self.nbrConjoints = json["nbrConjoints"].stringValue
        self.droitMcf = json["droitMcf"].stringValue
        self.dateEffetPens = json["dateEffetPens"].stringValue
        self.naturePension = json["naturePension"].stringValue
        self.flagAmo = json["flagAmo"].stringValue
        self.flagLiq = json["flagLiq"].stringValue
        self.situationFamiliale = json["situationFamiliale"].stringValue
        
        for paymentsJSON in json["paiements"].arrayValue {
            let payments = Payments(fromJSON: paymentsJSON)
            self.paiements.append(payments)
        }
        
        for childsJSON in json["enfants"].arrayValue {
            let childs = Childs(fromJSON: childsJSON)
            self.enfants.append(childs)
        }
    }
    
}

