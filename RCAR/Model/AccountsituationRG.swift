//
//  AccountsituationRG.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 06/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//


import UIKit
import SwiftyJSON

class AccountsituationRG: NSObject  {
    
    // MARK: Properties
    var nomAffilie : String!
    var noAffiliation : String!
    var matricule : String!
    var organisme : String!
    var noAdhesion : String!
    var dateRecrutement : String!
    var periodeValidee : String!
    var cotisationValidation : String!
    var cotisationRachat: String!
    var periodeRachat : String!
    var montantAcqui : String!
    var montantAcquiRevalorise : String!
    var dureeAA : String!
    var dureeMM : String!
    var cotisationRGDtos = [contributionRGDtos]()
    
    override init() {
        super.init()
    }
    
    init(fromJSON json: JSON){
        
        self.nomAffilie = json["nomAffilie"].stringValue
        self.noAffiliation = json["noAffiliation"].stringValue
        self.matricule = json["matricule"].stringValue
        self.organisme = json["organisme"].stringValue
        self.noAdhesion = json["noAdhesion"].stringValue
        self.dateRecrutement = json["dateRecrutement"].stringValue
        self.periodeValidee = json["periodeValidee"].stringValue
        self.cotisationValidation = json["cotisationValidation"].stringValue
        self.cotisationRachat = json["cotisationRachat"].stringValue
        self.periodeRachat = json["periodeRachat"].stringValue
        self.montantAcqui = json["montantAcqui"].stringValue
        self.montantAcquiRevalorise = json["montantAcquiRevalorise"].stringValue
        self.dureeAA = json["dureeAA"].stringValue
        self.dureeMM = json["dureeMM"].stringValue
        for cotisationRGJSON in json["cotisationRGDtos"].arrayValue {
            let cotisationRG = contributionRGDtos(fromJSON: cotisationRGJSON)
            self.cotisationRGDtos.append(cotisationRG)
        }
        
    }
}
