//
//  News.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 07/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON

class News {
    
    // MARK: Properties
    var identifiant : String!
    var image : String!
    var titre : String!
    var detail : String!
  

    init(fromJSON json: JSON){
        self.identifiant = json["identifiant"].stringValue
        self.image = json["image"].stringValue
        self.titre = json["titre"].stringValue
        self.detail = json["detail"].stringValue
    }
    
    
}

