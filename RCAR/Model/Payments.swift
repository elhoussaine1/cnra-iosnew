//
//  payments.swift
//  RCAR
//
//  Created by ab on 16/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON

class Payments {
    
    
    // MARK: Properties
    
    var anneeEcheance: String!
    var moisEcheance: String!
    var agencePaiement: String!
    var mntPaiementNet: String!
    var stuationPaiement: String!
    
    init(fromJSON json: JSON){
        
        self.anneeEcheance = json["anneeEcheance"].stringValue
        self.moisEcheance = json["moisEcheance"].stringValue
        self.agencePaiement = json["agencePaiement"].stringValue
        self.mntPaiementNet = json["mntPaiementNet"].stringValue
        self.stuationPaiement = json["stuationPaiement"].stringValue
    }
    
    
}
