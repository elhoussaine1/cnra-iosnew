//
//  AccountsituationRC.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 06/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON

class AccountsituationRC: NSObject  {
    
    
    // MARK: Properties
    var nomAffilie : String!
    var noAffiliation : String!
    var matricule : String!
    var organisme : String!
    var noAdhesion : String!
    var dateRecrutement : String!
    var cotisationRCDtos = [contributionRCDtos]()
    
    override init() {
        super.init()
    }
    
    
    init(fromJSON json: JSON){
        
        self.nomAffilie = json["nomAffilie"].stringValue
        self.noAffiliation = json["noAffiliation"].stringValue
        self.matricule = json["matricule"].stringValue
        self.organisme = json["organisme"].stringValue
        self.noAdhesion = json["noAdhesion"].stringValue
        self.dateRecrutement = json["dateRecrutement"].stringValue
        for cotisationRCJSON in json["cotisationRCDtos"].arrayValue {
            let cotisationRC = contributionRCDtos(fromJSON: cotisationRCJSON)
            self.cotisationRCDtos.append(cotisationRC)
        }
        
    }
}

