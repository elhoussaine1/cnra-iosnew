//
//  CertificationType.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 17/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//


import UIKit
import SwiftyJSON

class CertificationType {
    
    
    // MARK: Properties
    var libelle: String!
    var code: String!

    init(fromJSON json: JSON){
        
        self.libelle = json["libelle"].stringValue
        self.code = json["code"].stringValue
    }
    
    
}
