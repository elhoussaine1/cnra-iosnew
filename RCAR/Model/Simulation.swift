//
//  Simulation.swift
//  RCAR
//
//  Created by El houssaine El gamouz  on 02/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON

class Simulation {
    
    
    // MARK: Properties
    
    public var dernierSalaire : String!
    public var message : String!
    public var messageOut : String!
    public var p_date_effet : String!
    public var p_nature : String!
    public var p_nosequence : String!
    public var p_type_liqid : String!
    public var rc_p_date_limite_age : String!
    public var rc_p_nb_point_rc_aff : String!
    public var rc_p_nb_point_rc_cont_val : String!
    public var rc_p_nb_point_rc_val : String!
    public var rc_p_pension : String!
    public var rc_p_tx_ant_aj : String!
    public var rc_p_val_p : String!
    public var rg_p_annee_aff : String!
    public var rg_p_annee_rach : String!
    public var rg_p_annee_val : String!
    public var rg_p_d1 : String!
    public var rg_p_d1_annee : String!
    public var rg_p_d1_mois : String!
    public var rg_p_date_limite_age : String!
    public var rg_p_mois_aff : String!
    public var rg_p_mois_rach : String!
    public var rg_p_mois_val : String!
    public var rg_p_p1_revalorise : String!
    public var rg_p_p2 : String!
    public var rg_p_pension : String!
    public var rg_p_samcr : String!
    public var rg_p_tx_ant_aj : String!
    public var rg_p_tx_pension : String!
    public var total : String!

    init(fromJSON json: JSON){
        
        self.dernierSalaire = json["dernierSalaire"].stringValue
        self.message = json["message"].stringValue
        self.messageOut = json["messageOut"].stringValue
        self.p_date_effet = json["p_date_effet"].stringValue
        self.p_nature = json["p_nature"].stringValue
        self.p_nosequence = json["p_nosequence"].stringValue
        self.p_type_liqid = json["p_type_liqid"].stringValue
        self.rc_p_date_limite_age = json["rc_p_date_limite_age"].stringValue
        self.rc_p_nb_point_rc_aff = json["rc_p_nb_point_rc_aff"].stringValue
        self.rc_p_nb_point_rc_cont_val = json["rc_p_nb_point_rc_cont_val"].stringValue
        self.rc_p_nb_point_rc_val = json["rc_p_nb_point_rc_val"].stringValue
        self.rc_p_pension = json["rc_p_pension"].stringValue
        self.rc_p_tx_ant_aj = json["rc_p_tx_ant_aj"].stringValue
        self.rc_p_val_p = json["rc_p_val_p"].stringValue
        self.rg_p_annee_aff = json["rg_p_annee_aff"].stringValue
        self.rg_p_annee_rach = json["rg_p_annee_rach"].stringValue
        self.rg_p_annee_val = json["rg_p_annee_val"].stringValue
        self.rg_p_d1 = json["rg_p_d1"].stringValue
        self.rg_p_d1_annee = json["rg_p_d1_annee"].stringValue
        self.rg_p_d1_mois = json["rg_p_d1_mois"].stringValue
        self.rg_p_date_limite_age = json["rg_p_date_limite_age"].stringValue
        self.rg_p_mois_aff = json["rg_p_mois_aff"].stringValue
        self.rg_p_mois_rach = json["rg_p_mois_rach"].stringValue
        self.rg_p_mois_val = json["rg_p_mois_val"].stringValue
        self.rg_p_p1_revalorise = json["rg_p_p1_revalorise"].stringValue
        self.rg_p_p2 = json["rg_p_p2"].stringValue
        self.rg_p_pension = json["rg_p_pension"].stringValue
        self.rg_p_samcr = json["rg_p_samcr"].stringValue
        self.rg_p_tx_ant_aj = json["rg_p_tx_ant_aj"].stringValue
        self.rg_p_tx_pension = json["rg_p_tx_pension"].stringValue
        self.total = json["total"].stringValue
        
        

    }
    
   
    
}
