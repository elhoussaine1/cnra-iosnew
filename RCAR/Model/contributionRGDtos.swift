//
//  contributionRGDtos.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 06/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON

class contributionRGDtos {
    
    // MARK: Properties
    var annee : String!
    var mois : String!
    var jours : String!
    var trimestre : String!
    var cotisation : String!
    
    init(fromJSON json: JSON){
        
        self.annee = json["annee"].stringValue
        self.mois = json["mois"].stringValue
        self.jours = json["jours"].stringValue
        self.trimestre = json["trimestre"].stringValue
        self.cotisation = json["cotisation"].stringValue
    }
    
    
}
