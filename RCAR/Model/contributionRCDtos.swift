//
//  contributionRCDtos.swift
//  RCAR
//
//  Created by El houssaine El gamouz  on 06/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON

class contributionRCDtos {
    
    
    // MARK: Properties
    
    var annee : String!
    var cotisationAffiliation : String!
    var cotisationValidation : String!
    var nbrPointAcquis : String!
    
    init(fromJSON json: JSON){
        
        self.annee = json["annee"].stringValue
        self.cotisationAffiliation = json["cotisationAffiliation"].stringValue
        self.cotisationValidation = json["cotisationValidation"].stringValue
        self.nbrPointAcquis = json["nbrPointAcquis"].stringValue
    }
    
    
}
