//
//  SituationDetail.swift
//  RCAR
//
//  Created by El houssaine  El gamouz on 17/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON

class SituationDetail {
    
    
    // MARK: Properties
    var rang: String!
    var rubrique: String!
    var montant: String!
    var libRubriquePaie: String!
    var payer: String!
 
    
    init(fromJSON json: JSON){
     
        self.rang = json["rang"].stringValue
        self.rubrique = json["rubrique"].stringValue
        self.montant = json["montant"].stringValue
        self.libRubriquePaie = json["libRubriquePaie"].stringValue
        self.payer = json["payer"].stringValue
    }
    
    
}
