//
//  Folder.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 05/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON

class Folder {
    
    
    // MARK: Properties
    
    var canal: String!
    var client: String!
    var dateAccepte: String!
    var dateEncours: String!
    var dateEngagement: String!
    var dateNotification: String!
    var dateRecu: String!
    var dateRejet: String!
    var dateSituation: String!
    var dateTraite: String!
    var descNotification: String!
    var description: String!
    var engagement: String!
    var etatAccepte: String!
    var etatEncours: String!
    var etatRecu: String!
    var etatRejete: String!
    var etatSituation: String!
    var etatTraite: String!
    var motifRejet: String!
    var numero: String!
    var produit: String!
    var reference: String!
    var type: String!
    
    init(fromJSON json: JSON){
        
        self.canal = json["canal"].stringValue
        self.client = json["client"].stringValue
        self.dateAccepte = json["dateAccepte"].stringValue
        self.dateEncours = json["dateEncours"].stringValue
        self.dateEngagement = json["dateEngagement"].stringValue
        self.dateNotification = json["dateNotification"].stringValue
        self.dateRecu = json["dateRecu"].stringValue
        self.dateRejet = json["dateRejet"].stringValue
        self.dateSituation = json["dateSituation"].stringValue
        self.dateTraite = json["dateTraite"].stringValue
        self.descNotification = json["descNotification"].stringValue
        self.description = json["description"].stringValue
        self.engagement = json["engagement"].stringValue
        self.etatAccepte = json["etatAccepte"].stringValue
        self.etatEncours = json["etatEncours"].stringValue
        self.etatRecu = json["etatRecu"].stringValue
        self.etatRejete = json["etatRejete"].stringValue
        self.etatSituation = json["etatSituation"].stringValue
        self.etatTraite = json["etatTraite"].stringValue
        self.motifRejet = json["motifRejet"].stringValue
        self.numero = json["numero"].stringValue
        self.produit = json["produit"].stringValue
        self.reference = json["reference"].stringValue
        self.type = json["type"].stringValue

    }
    
    
}
