//
//  Beneficiary.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 03/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON

class AffiliatedBeneficiaryCor : NSObject {
    
    // MARK: Properties
    var nom: String!
    var prenom: String!
    var mail: String!
    var adresse: String!
    var ville: String!
    var pays: String!
    var telephone: String!
    var gsm: String!
    var reclamation: String!
    var identifiant: String!
    var produit: String!
    var type: String!
    var profil: String!
    var canal: String!
    var desc: String!
    var codePostal      : String!
    var email           : String!
    var lieuResidence   : String!
    var localite        : String!
    var sms             : String!
    var telGsm          : String!
    var voie            : String!
    var telFix          : String!
    var telFix1         : String!
    var telFix2         : String!
    var telFix3         : String!
    
    override init() {
        super.init()
    }
    
    init(fromJSON json: JSON){
    
        self.nom = json["nom"].stringValue
        self.prenom = json["prenom"].stringValue
        self.mail = json["mail"].stringValue
        self.adresse = json["adresse"].stringValue
        self.ville = json["ville"].stringValue
        self.pays = json["pays"].stringValue
        self.telephone = json["telephone"].stringValue
        self.gsm = json["gsm"].stringValue
        self.reclamation = json["reclamation"].stringValue
        self.identifiant = json["identifiant"].stringValue
        self.produit = json["produit"].stringValue
        self.type = json["type"].stringValue
        self.profil = json["profil"].stringValue
        self.canal = json["canal"].stringValue
        self.desc = json["description"].stringValue
        self.codePostal = json["codePostal"].stringValue
        self.email = json["email"].stringValue
        self.lieuResidence = json["lieuResidence"].stringValue
        self.localite = json["localite"].stringValue
        self.sms = json["sms"].stringValue
        self.telGsm = json["telGsm"].stringValue
        self.voie = json["voie"].stringValue
        self.telFix = json["telFix"].stringValue
        self.telFix1 = json["telFix1"].stringValue
        self.telFix2 = json["telFix2"].stringValue
        self.telFix3 = json["telFix3"].stringValue
    }
    
   
    
}
