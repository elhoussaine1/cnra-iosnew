//
//  AffiliatedCoordinate .swift
//  RCAR
//
//  Created by El houssaine El gamouz on 03/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON


class AffiliatedCoordinate : NSObject , NSCoding {
    
    var bporlieuDit     : String!
    var cin             : String!
    var codePostal      : String!
    var dirOrSer        : String!
    var email           : String!
    var entreeImm       : String!
    var fax             : String!
    var identifiant     : String!
    var interlocuteur   : String!
    var lieuResidence   : String!
    var lieu_dit2       : String!
    var localite        : String!
    var nomPrenom       : String!
    var okMail          : String!
    var pays            : String!
    var siteWeb         : String!
    var sms             : String!
    var telEco          : String!
    var telFix          : String!
    var telFix1         : String!
    var telFix2         : String!
    var telFix3         : String!
    var telGsm          : String!
    var voie            : String!

    
    override init() {
        super.init()
    }
    
    init(fromJSON json: JSON){
        
        self.bporlieuDit              = json["bporlieuDit"].stringValue
        self.cin                      = json["cin"].stringValue
        self.codePostal               = json["codePostal"].stringValue
        self.dirOrSer                 = json["dirOrSer"].stringValue
        self.email                    = json["email"].stringValue
        self.entreeImm                = json["entreeImm"].stringValue
        self.fax                      = json["fax"].stringValue
        self.identifiant              = json["identifiant"].stringValue
        self.interlocuteur            = json["interlocuteur"].stringValue
        self.lieuResidence            = json["lieuResidence"].stringValue
        self.lieu_dit2                = json["lieu_dit2"].stringValue
        self.localite                 = json["localite"].stringValue
        self.nomPrenom                = json["nomPrenom"].stringValue
        self.okMail                   = json["okMail"].stringValue
        self.pays                     = json["pays"].stringValue
        self.siteWeb                  = json["siteWeb"].stringValue
        self.sms                      = json["sms"].stringValue
        self.telEco                   = json["telEco"].stringValue
        self.telFix                   = json["telFix"].stringValue
        self.telFix1                  = json["telFix1"].stringValue
        self.telFix2                  = json["telFix2"].stringValue
        self.telFix3                  = json["telFix3"].stringValue
        self.telGsm                   = json["telGsm"].stringValue
        self.voie                     = json["voie"].stringValue
    
    }
    
    required init?(coder aDecoder: NSCoder) {

        self.bporlieuDit              = aDecoder.decodeObject(forKey: "bporlieuDit") as! String
        self.cin                      = aDecoder.decodeObject(forKey: "cin") as! String
        self.codePostal               = aDecoder.decodeObject(forKey: "codePostal") as! String
        self.dirOrSer                 = aDecoder.decodeObject(forKey: "dirOrSer") as! String
        self.email                    = aDecoder.decodeObject(forKey: "email") as! String
        self.entreeImm                = aDecoder.decodeObject(forKey: "entreeImm") as! String
        self.fax                      = aDecoder.decodeObject(forKey: "fax") as! String
        self.identifiant              = aDecoder.decodeObject(forKey: "identifiant") as! String
        self.interlocuteur            = aDecoder.decodeObject(forKey: "interlocuteur") as! String
        self.lieuResidence            = aDecoder.decodeObject(forKey: "lieuResidence") as! String
        self.lieu_dit2                = aDecoder.decodeObject(forKey: "lieu_dit2") as! String
        self.localite                 = aDecoder.decodeObject(forKey: "localite") as! String
        self.nomPrenom                = aDecoder.decodeObject(forKey: "nomPrenom") as! String
        self.okMail                   = aDecoder.decodeObject(forKey: "okMail") as! String
        self.pays                     = aDecoder.decodeObject(forKey: "pays") as! String
        self.siteWeb                  = aDecoder.decodeObject(forKey: "siteWeb") as! String
        self.sms                      = aDecoder.decodeObject(forKey: "sms") as! String
        self.telEco                   = aDecoder.decodeObject(forKey: "telEco") as! String
        self.telFix                   = aDecoder.decodeObject(forKey: "telFix") as! String
        self.telFix1                  = aDecoder.decodeObject(forKey: "telFix1") as! String
        self.telFix2                  = aDecoder.decodeObject(forKey: "telFix2") as! String
        self.telFix3                  = aDecoder.decodeObject(forKey: "telFix3") as! String
        self.telGsm                   = aDecoder.decodeObject(forKey: "telGsm") as! String
        self.voie                     = aDecoder.decodeObject(forKey: "voie") as! String
        
    }
    internal  func encode(with encoder: NSCoder) {
        
        encoder.encode(self.bporlieuDit, forKey: "bporlieuDit")
        encoder.encode(self.cin, forKey: "cin")
        encoder.encode(self.codePostal, forKey: "codePostal")
        encoder.encode(self.dirOrSer, forKey: "dirOrSer")
        encoder.encode(self.email, forKey: "email")
        encoder.encode(self.entreeImm, forKey: "entreeImm")
        encoder.encode(self.fax, forKey: "fax")
        encoder.encode(self.identifiant, forKey: "identifiant")
        encoder.encode(self.interlocuteur, forKey: "interlocuteur")
        encoder.encode(self.lieuResidence, forKey: "lieuResidence")
        encoder.encode(self.lieu_dit2, forKey: "lieu_dit2")
        encoder.encode(self.fax, forKey: "localite")
        encoder.encode(self.identifiant, forKey: "nomPrenom")
        encoder.encode(self.interlocuteur, forKey: "okMail")
        encoder.encode(self.lieuResidence, forKey: "pays")
        encoder.encode(self.lieu_dit2, forKey: "siteWeb")
        encoder.encode(self.fax, forKey: "sms")
        encoder.encode(self.identifiant, forKey: "telEco")
        encoder.encode(self.interlocuteur, forKey: "telFix")
        encoder.encode(self.lieuResidence, forKey: "telFix1")
        encoder.encode(self.lieu_dit2, forKey: "telFix2")
        encoder.encode(self.fax, forKey: "telFix3")
        encoder.encode(self.identifiant, forKey: "telGsm")
        encoder.encode(self.interlocuteur, forKey: "voie")
        
    }
    

    static func currentAffiliated() -> AffiliatedCoordinate {
        if(KeychainWrapper.hasValueForKey(keyName: "AffiliatedCoordinate")) {
            let affiliatedCoordinate: AffiliatedCoordinate = KeychainWrapper.objectForKey(keyName: "AffiliatedCoordinate") as! AffiliatedCoordinate
            return affiliatedCoordinate
        }
        
        return AffiliatedCoordinate()
    }
    
    static func isLoggedIn() -> Bool {
        return KeychainWrapper.hasValueForKey(keyName: "AffiliatedCoordinate")
    }
    
    static func logout() {
        KeychainWrapper.removeObjectForKey(keyName: "AffiliatedCoordinate")
    }
    
    func save() {
        KeychainWrapper.setObject(value: self, forKey: "AffiliatedCoordinate")
}
}

