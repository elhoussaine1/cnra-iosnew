//
//  ApiError.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 03/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//


import SwiftyJSON
import Alamofire

class ApiError {
    var response: HTTPURLResponse?
    var json: JSON?
    
    init(response: DataResponse<Any>) {
        self.response = response.response
        
        if let data = response.data {
            self.json = JSON(data: data)
        }
    }
    
    func errorMessage() -> String {
        var errorMessage = ""
        
      
        
        if let errors = json?["errors"] , errors.count != 0 {
            for (_, messages):(String, JSON) in errors {
                //let key = field.titleize
              
                
                for _ in messages.arrayValue {
                   // errorMessage += "\(key) \(message.stringValue)\n"
                }
            }
        } else if let message = json?["message"] , !message.isEmpty {
            errorMessage = message.stringValue
        } else {
            errorMessage = "Some error occurred. Please try again."
        }
        
        return errorMessage
    }
    
}
