//
//  childs.swift
//  RCAR
//
//  Created by ab on 16/09/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON

class Childs {
    
    
    // MARK: Properties
    var nom: String!
    var dateNaissance: String!
    var codeSuspension: String!
    var type: String!
    
    init(fromJSON json: JSON){
        
        self.nom = json["nom"].stringValue
        self.dateNaissance = json["dateNaissance"].stringValue
        self.codeSuspension = json["codeSuspension"].stringValue
        self.type = json["type"].stringValue
    }
    
    
}
