//
//  Complaint.swift
//  RCAR
//
//  Created by El houssaine El gamouz on 16/08/2018.
//  Copyright © 2018 Aramobile. All rights reserved.
//

import UIKit
import SwiftyJSON

class Complaint {
    
    
    // MARK: Properties

    var canal: String!
    var dateReclamation: String!
    var dateReponse: String!
    var idd: String!
    var idtDocument: String!
    var statut: String!
    
    
    init(fromJSON json: JSON){
        
        self.canal = json["canal"].stringValue
        self.dateReclamation = json["dateReclamation"].stringValue
        self.dateReponse = json["dateReponse"].stringValue
        self.idd = json["idd"].stringValue
        self.idtDocument = json["idtDocument"].stringValue
        self.statut = json["statut"].stringValue

    }
}
