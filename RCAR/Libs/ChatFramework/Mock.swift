extension NetworkClient {
    static let mock = NetworkClient { text -> Effect<(Data?, URLResponse?, Error?)> in
        return Effect<(Data?, URLResponse?, Error?)> { callback in
            let response = mockResponses[Int.random(in: 0...2)]
            callback((try! JSONEncoder().encode(response), nil,  nil))
        }
    }
}

let mockResponses = [NetworkResponse.quickReply, .normal, .audio]

extension NetworkResponse {
    static let quickReply = NetworkResponse(
        id: 1,
        text: "hello",
        createdAt: "2020-06-29T13:24:55.882Z",
        quickReplies: ["Salam", "Hello world !", "I am here to help you", "I am here to help you", "I am here to help you", "I am here to help you", "I am here to help you", "I am here to help you"].map { NetworkResponse.QuickReply.init(title: $0, value: $0) },
        action: "",
        audioURL: nil
    )
    
    static let normal = NetworkResponse(
        id: 2,
        text: "Yes",
        createdAt: "2020-06-29T13:24:55.882Z",
        action: ""
    )
    
    static let audio = NetworkResponse(
        id: 1,
        text: "an audio need to play",
        createdAt: "2020-06-29T13:24:55.882Z",
        action: "",
        audioURL: "https://www.text2speech.org/FW/getfile.php?file=eb8630453f93b49c98eca3b72d740876%2Fspeech.mp3"
    )
    
}

