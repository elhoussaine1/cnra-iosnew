public struct User {
    let id: String
    let name: String
    let type: String
    let token: String
    public init(
        id: String,
        name: String,
        type: String,
        token: String
    ) {
        self.id = id
        self.name = name
        self.type = type
        self.token = token
    }
}

//(identifiant : String, dateEffet : String, success: @escaping (Simulation) -> Void, failure: @escaping (String) -> Void )

//let simulation: (String, String, (Int) -> Void, (String) -> Void) -> Void

import Foundation
public struct AppEnvironment {
    public var locale: Locale = Locale(identifier: "ar_MA")
    public var date: () -> Date = Date.init
    public var partialResults: Bool = true
    public var user: User
    public var apiClient: NetworkClient
    public let speech: SpeechRecognizer
    public let player = AudioPlayer()
    public let config: Config
    
    // simulation api
    let simulation: (String, String,  @escaping (Simulation) -> Void,  @escaping (String) -> Void ) -> Void
    
    init(
        user: User,
        apiClient: NetworkClient,
        speech: SpeechRecognizer = SpeechRecognizer(locale: Locale(identifier: "ar_MA")),
        config: Config,
        simulation: @escaping (String, String,  @escaping (Simulation) -> Void,  @escaping (String) -> Void ) -> Void
    ) {
        self.user = user
        self.apiClient = apiClient
        self.speech = speech
        self.config = config
        self.simulation = simulation
    }
}


public struct Config {
    let headerColor: String
    let botBubbleColor: String
    let botBubbleTextColor: String
    let userBubbleColor: String
    let userBubbleTextColor: String
    public init(
        headerColor: String,
        botBubbleColor: String,
        botBubbleTextColor: String,
        userBubbleColor: String,
        userBubbleTextColor: String
    ) {
        self.headerColor = headerColor
        self.botBubbleTextColor = botBubbleTextColor
        self.botBubbleColor = botBubbleColor
        self.userBubbleColor = userBubbleColor
        self.userBubbleTextColor = userBubbleTextColor
    }
}

public var Current: AppEnvironment!

extension User {
    static var blob = User(
        id: "1",
        name: "Blob",
        type: "Affilate",
        token: "token"
    )
}

extension AppEnvironment {
     static func live(
        config: Config = .´default´,
        user: User,
        simulation:  @escaping (String, String,  @escaping (Simulation) -> Void,  @escaping (String) -> Void ) -> Void
    ) -> AppEnvironment {
        AppEnvironment(
            user: user,
            apiClient: .live,
            speech: SpeechRecognizer(
                locale: Locale(identifier: "ar_MA")),
            config: config,
            simulation: simulation
        )
    }
}


extension Config {
    public static let ´default´ = Config(
        headerColor: "#E7F3E9",
        botBubbleColor: "#F7F7F7",
        botBubbleTextColor: "#1F1F24",
        userBubbleColor: "#4D8231",
        userBubbleTextColor: "#FFFFFF"
    )
}

extension AppEnvironment {
    public static let mock = AppEnvironment(
        user: .blob,
        apiClient: .mock,
        config: .´default´,
        simulation: { _,_,_,_ in () }
    )
}

let apiURL = URL(string: "https://www.rcar.ma/essidik/data/ar")!

func sendMessage(_ text: String) -> Effect<String?> {
    return dataTask(with: apiURL).decode(as: String.self)
}

public var bot = User(
    id: "2",
    name: "bot",
    type: "bot",
    token: ""
)
