public struct NetworkClient {
    public let send: (NetworkRequest) -> Effect<(Data?, URLResponse?, Error?)>
}

extension NetworkClient {
    func sendText(_ text: String) -> Effect<(Data?, URLResponse?, Error?)> {
        return send(
            NetworkRequest(
                id: Current.user.id,
                text: text,
                usertype: Current.user.type,
                token: Current.user.token
        ))
    }
}

extension NetworkClient {
    static let live = NetworkClient { request -> Effect<(Data?, URLResponse?, Error?)> in
        return dataTask(with: networkRequest(request))
    }
}

public func map<A, B>(_ f: @escaping (A) -> B) -> ([A]) -> [B] {
    return { $0.map(f) }
}

let networkRequest: (NetworkRequest) -> URLRequest = {
    var request = URLRequest(url: apiURL)
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    request.httpMethod = "post"
    
    let jsonDict = [
        "id": $0.id,
        "text": $0.text,
        "usertype": $0.usertype,
        "token": "Bearer \($0.token)"
        ] as [String : Any]
    
    let data = try! JSONSerialization.data(withJSONObject: jsonDict, options: [])
    
    request.httpBody = data
    request.httpBody = try? JSONEncoder().encode($0)
    return request
}


public struct NetworkRequest: Encodable {
    let id: String
    let text: String
    let usertype: String
    let token: String
}

let atomDateFormatter: DateFormatter = {
    let atomDateFormatter = DateFormatter()
    atomDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
    atomDateFormatter.locale = Locale.current
    atomDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
    return atomDateFormatter
}()



public enum Action {
    case endActivity
    case sendAppointement(NetworkResponse.ResponseData?)
    case requestCertificate(NetworkResponse.ResponseData?)
    case sendClaim(NetworkResponse.ResponseData?)
    case simulation(NetworkResponse.ResponseData?)
}

public struct NetworkResponse: Codable {
    let id: Int
    let text: String
    let createdAt: String
    let action: String?
    let data: ResponseData?
    var quickReplies: [QuickReply]?
    let audioURL: String?
    
    public struct ResponseData: Codable {
        let identifiant: String?
        let nomPrenom: String?
        let jour: String?
        let heure: String?
        let adresseMail: String?
        let objet: String?
        let tel: String?
        let fax: String?
        let email: String?
        let adresse: String?
        let idTypeAttestation: String?
        let gsm: String?
        let ville, pays, reclamation, age: String?
    }
    
    
    public init(
        id: Int,
        text: String,
        createdAt: String,
        quickReplies: [QuickReply]? = nil,
        action: String? = nil,
        audioURL: String? = nil,
        data: ResponseData? = nil
        ) {
        self.id = id
        self.text = text
        self.createdAt = createdAt
        self.quickReplies = quickReplies
        self.audioURL = audioURL
        self.action = action
        self.data = data
    }
    
    var date: Date? {
        return atomDateFormatter.date(from: createdAt)
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case text
        case createdAt
        case quickReplies
        case action
        case data
        case audioURL = "audiolink"
    }
    
    public struct QuickReply: Codable {
        let title: String
        let value: String
    }
    
}
