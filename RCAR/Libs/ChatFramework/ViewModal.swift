public struct Effect<A> {
    public let run: (@escaping (A) -> Void) -> Void
    
    public init(run: @escaping (@escaping (A) -> Void) -> Void) {
        self.run = run
    }
    
    public func map<B>(_ f: @escaping (A) -> B) -> Effect<B> {
        return Effect<B> { callback in self.run { a in callback(f(a)) }
        }
    }
}

extension Effect {
  func receive(on queue: DispatchQueue) -> Effect {
    return Effect { callback in
      self.run { a in
        queue.async { callback(a) }
      }
    }
  }
}

func dataTask(with request: URL) -> Effect<(Data?, URLResponse?, Error?)> {
  return Effect { callback in
    URLSession.shared.dataTask(with: request) { data, response, error in
      callback((data, response, error))
    }
    .resume()
  }
}

func dataTask(with request: URLRequest) -> Effect<(Data?, URLResponse?, Error?)> {
  return Effect { callback in
    URLSession.shared.dataTask(with: request) { data, response, error in
      callback((data, response, error))
    }
    .resume()
  }
}

extension Effect where A == (Data?, URLResponse?, Error?) {
  func decode<B: Decodable>(as type: B.Type) -> Effect<B?> {
    return self.map { data, _, _ in
      data
        .flatMap { try? JSONDecoder().decode(B.self, from: $0) }
    }
  }
}

// app state

struct State {
    var messages: [Message]
    var isRecording: Bool
    var text: String
    var recordedText: String
    var isSpeechAuthorized: Bool
}

protocol ViewModalInputs {
    func recordButtonTapped()
    func sendButtonTapped()
    func sendQuickReply(_ reply: String)
    func textChanged(_ value: String)
    func viewDidLoad()
}

protocol ViewModalType {
    var inputs: ViewModalInputs { get }
    var state: State { get }
}

class ViewModal {
    
    var state: State = State(
            messages: [],
            isRecording: false,
            text: String(),
            recordedText: String(),
            isSpeechAuthorized: false
        ) {
        didSet {
            callback(state)
        }
    }
    
    var callback:((State) -> Void)
    var actions: (Action) -> Void
    var speech: SpeechRecognizer
    
    init(
        callback: @escaping (State) -> Void,
        actions: @escaping (Action) -> Void,
        speech: SpeechRecognizer) {
        self.speech = speech
        self.callback = callback
        self.actions = actions
        self.callback(state)
    }
    
    var inputs: ViewModalInputs { self }
}


let numberFormatter: () -> NumberFormatter = {
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    return formatter
}


extension ViewModal: ViewModalType, ViewModalInputs {
    
    func sendQuickReply(_ reply: String) {
        state.text = reply
        guard var last = state.messages.last else { return }
        if state.messages.count > 1 {
            last.info?.quickReplies = nil
            state.messages.removeLast()
            state.messages.append(last)
        }
        send(reply)
    }
    
    func sendButtonTapped() {
        send(state.text)
    }

    func textChanged(_ value: String) {
        guard state.text != value else { return }
        self.state.text = value
    }
    
    func recordButtonTapped() {
        if self.speech.audioEngine.isRunning {
            endRecording()
        } else {
            try? startRecording(&self.speech).run { message in
                self.state.recordedText = message
                    debounce(time: .now() + 1.0) { [weak self] in
                    self?.sendRecordedText()
                    self?.endRecording()
                }
            }
            self.state.isRecording = true
        }
    }
    
    private func sendRecordedText() {
        send(state.recordedText)
        state.recordedText = String()
    }
        
    private func endRecording() {
        stopRecording(&self.speech)
        self.state.isRecording = false
    }
    
    func viewDidLoad() {
                       
        askPermission().run { self.state.isSpeechAuthorized = $0 }
                
        Current
            .apiClient
            .sendText("welcomemessage")
            .decode(as: [NetworkResponse].self)
            .run(appendIncomingMessage)
        
    }
                       
    private func appendIncomingMessage(_ responses: [NetworkResponse]?) -> Void {
        
        guard let responses = responses else { return  }
        responses.forEach {
            let incoming: Message = Message(sender: bot |> asSender, info: $0)
            state.messages.append(incoming)
            guard let audioURL = incoming.info?.audioURL,
                  let url = URL(string: audioURL) else { return }
            
            let mp3URL = url.deletingPathExtension().appendingPathExtension("mp3")
                        
            Current.player.playAudio(from: mp3URL, id: 0)
        }
        
        
        responses.forEach { (response) in
            guard let action = response.action else { return }
            
            switch action {
            case "finish_activity":
                actions(.endActivity)
            case "result_post_rv":
                actions(.sendAppointement(response.data))
            case "post_demande_attestation":
                actions(.requestCertificate(response.data))
            case "result_POST_reclamation":
                actions(.sendClaim(response.data))
            case "getSimulation":
                
                guard let object = jsonObject(response.data), let age = object["age"] as? String else { return }
                
                Current.simulation(Current.user.id, age, { [weak self] simulation in
                              
                    guard let self = self else { return }
                    guard var result = simulation.total else { return }
                    result.removeAll(where: { $0 == ","})
                    guard let double = Double(result),
                          let total = numberFormatter().string(from: NSNumber(value: double / 12))  else { return }
                                          
                    self.state.messages.append(contentsOf: [
                        Message.incoming("المبلغ الشهري الخام ديال الخلصة ديالكم غادي يكون :"+total+" درهم "),
                        Message.incoming("هاد المبلغ راه تقريبي وتحسب على أساس واحد التقدير معين ديال السنوات الجاية. وما تيلزمناش بالأداء ديالو حيت المعاش غايتحسب على أساس الوضعية كيف ما هيا ف سن التقاعد"
                        )
                    ])
                    
                }, { error in
                    
                })
                
            default: return
            }
        }
    }
    
}

extension Message {
    
    static func outgoing(_ text: String) -> Message {
        return Message(
            sender: currentSender,
            messageId: String.id,
            sentDate: Current.date(),
            kind: .text(text)
        )
    }
    
    static func incoming(_ text: String) -> Message {
        return Message(
            sender: bot |> asSender,
            messageId: String.id,
            sentDate: Current.date(),
            kind: .text(text)
        )
    }
    
}

extension ViewModal {
    private func send(_ text: String) {
        let message: Message = .outgoing(text)
        state.messages.append(message)
        Current.apiClient.sendText(
            text
        )
        .decode(as: [NetworkResponse].self)
        .run(appendIncomingMessage)
        
        state.text = String()
        state.recordedText = String()
        endRecording()
    }
}

import Foundation
extension String {
    static var id: String { UUID().uuidString }
}

func debounce(
    time delay: DispatchTime = .now() + .milliseconds(250),
    action: @escaping () -> Void
) {
    pendingRequestWorkItem?.cancel()
    let workItem = DispatchWorkItem { action() }
    pendingRequestWorkItem = workItem
    DispatchQueue.main.asyncAfter(
        deadline: delay,
        execute: workItem
    )
}
private var pendingRequestWorkItem: DispatchWorkItem?


