import AVKit

public struct AudioPlayer {
    
    public init() {}
    
    func playAudio(from url: URL, id: Int) {
        let item = AVPlayerItem(url: url)
        let player = AVPlayer(playerItem: item)
        player.play()
        players[id] = player
    }
    
    func stopAudio(id: Int) {
        players[id]?.pause()
    }
}

var players: [Int: AVPlayer] = [:]


