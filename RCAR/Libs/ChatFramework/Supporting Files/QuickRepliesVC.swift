import UIKit
extension UIView {
    public func constrainEdges(to view: UIView, priority: UILayoutPriority = .required, insets: UIEdgeInsets = .zero) {
        self.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            self.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: insets.left),
            self.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -insets.right),
            self.topAnchor.constraint(equalTo: view.topAnchor, constant: insets.top),
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -insets.bottom)
        ]
        
        constraints.forEach { $0.priority = priority }
        NSLayoutConstraint.activate(constraints)
    }
}



let flowLayout: () -> LeftAlignedCollectionViewFlowLayout = {
    let flowLayout = LeftAlignedCollectionViewFlowLayout()
    flowLayout.estimatedItemSize = CGSize(width: 100, height: 100)
    flowLayout.scrollDirection = .vertical
    flowLayout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    flowLayout.minimumInteritemSpacing = 4
    flowLayout.minimumLineSpacing = 8
    return flowLayout
}

public func roundedStyle(_ cornerRadius: CGFloat) -> (UIView) -> Void {
    return {
        $0.layer.cornerRadius = cornerRadius
        if #available(iOS 13.0, *) {
            $0.layer.cornerCurve = .continuous
        }
        $0.clipsToBounds = true
    }
}


func shadowStyle() -> (UIView) -> Void {
    return {
        $0.layer.shadowColor = UIColor.black.cgColor
        $0.layer.shadowOpacity = 1.0
        $0.layer.shadowOffset = CGSize(width: -1, height: 1)
        $0.layer.shadowRadius = 5
        $0.layer.shadowPath = UIBezierPath(rect: $0.bounds).cgPath
    }
}

let quickReplyCellStyle: (UIView) -> Void =
    roundedStyle(12)
  <> {
    $0.backgroundColor = UIColor(white: 0.96, alpha: 1.0)
  }

import MessageKit

protocol QuickRepliesFooterDelegate: class {
    func quickReplyFooterDidSendReply(_ reply: String)
}

public class QuickRepliesFooter: MessageReusableView, UICollectionViewDataSource {
            
    static let reuseIdentifier: String = "QuickRepliesFooter"
    
    var collectionView: UICollectionView!
    var delegate: QuickRepliesFooterDelegate?
               
    var replies: [NetworkResponse.QuickReply] = [] {
        willSet {
            collectionView.reloadData()
        }
    }
        
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupHierarchy()
    }
    
    func setupHierarchy() {
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout())
        self.addSubview(collectionView)
        collectionView.register(QuickReplyCell.self, forCellWithReuseIdentifier: QuickReplyCell.reuseIdentifier)
        collectionView.isUserInteractionEnabled = true
        collectionView.backgroundColor = .white
        collectionView.dataSource = self
        collectionView.showsVerticalScrollIndicator = false
        collectionView.constrainEdges(to: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return replies.count
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: QuickReplyCell.reuseIdentifier, for: indexPath) as! QuickReplyCell
        cell.label.text = replies[indexPath.row].value
        cell.addGestureRecognizer(UITapGestureRecognizer(
            target: self,
            action: #selector(handleTapGesture))
        )
        return cell
    }
                
    @objc func handleTapGesture(_ gesture: UIGestureRecognizer) {
        guard let cell = gesture.view as? QuickReplyCell,
            let reply = cell.label.text else { return }
        delegate?.quickReplyFooterDidSendReply(reply)
    }
        
}

extension QuickRepliesFooter {
    
    class QuickReplyCell: UICollectionViewCell {
        static var reuseIdentifier: String = "QuickReplyCell"
        let label = UILabel()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            setup()
        }
        
        required init?(coder: NSCoder) {
            fatalError()
        }
        
        func setup() {
            label.font = UIFont.systemFont(ofSize: 16)
            self.addSubview(label)
            label.numberOfLines = 0
            let insets = UIEdgeInsets(top: 4, left: 8, bottom: 4, right: 8)
            label.constrainEdges(to: self, insets: insets)
            quickReplyCellStyle(self)
        }
    }
    
}


class LeftAlignedCollectionViewFlowLayout: UICollectionViewFlowLayout {

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributes = super.layoutAttributesForElements(in: rect)

        var leftMargin = sectionInset.left
        var maxY: CGFloat = -1.0
        attributes?.forEach { layoutAttribute in
            if layoutAttribute.frame.origin.y >= maxY {
                leftMargin = sectionInset.left
            }

            layoutAttribute.frame.origin.x = leftMargin

            leftMargin += layoutAttribute.frame.width + minimumInteritemSpacing
            maxY = max(layoutAttribute.frame.maxY , maxY)
        }

        return attributes
    }
}
