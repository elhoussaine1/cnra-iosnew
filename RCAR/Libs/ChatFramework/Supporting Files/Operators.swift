public func first<A, B, C>(
    _ f: @escaping (A) -> C
    ) -> ((A,B)) -> (C, B) {
    return { pair in (f(pair.0), pair.1) }
}

public func second<A, B, C>(
    _ f: @escaping (B) -> C
    ) -> ((A,B)) -> (A, C) {
    return { pair in (pair.0, f(pair.1)) }
}


precedencegroup ForwardApplication {
    associativity: left
}

infix operator |>: ForwardApplication

public func |> <A, B>(_ a: A, f: (A) -> B) -> B { return f(a) }
public func |> <A>(_ a: inout A, f: (inout A) -> Void) { f(&a) }

precedencegroup ForwardComposition {
    associativity: left
    higherThan: ForwardApplication, EffectfulComposition
}

infix operator >>>: ForwardComposition

public func >>><A, B, C>(_ f: @escaping (A) -> B, _ g: @escaping (B) -> C) -> (A) -> C {
return { a in g(f(a)) }
}

precedencegroup EffectfulComposition {
    associativity: left
    higherThan: ForwardApplication
}

infix operator >=>: EffectfulComposition

public func >=> <A, B, C>(
    _ f: @escaping (A) -> B?,
    _ g: @escaping (B) -> C?
    ) -> ((A) -> C?) {
    return { a in
        f(a).flatMap(g)
    }
}

public func >=> <A, B, C>(
    _ f: @escaping (A) -> [B],
    _ g: @escaping (B) -> [C]
    ) -> ((A) -> [C]) {
    return { a in
        f(a).flatMap(g)
    }
}

precedencegroup SingleTypeComposition {
    associativity: left
    higherThan: ForwardApplication
}


infix operator <>: SingleTypeComposition

public func <> <A>(
    _ f: @escaping (A) -> A,
    _ g: @escaping (A) -> A
    ) -> (A) -> A {
    return f >>> g
}

public func <> <A: AnyObject>(
    _ f: @escaping (inout A) -> Void,
    _ g: @escaping (inout A) -> Void
    ) -> (inout A) -> Void {
     return { a in
        f(&a)
        g(&a) }
}

public func <> <A: AnyObject>( // any object that all referenecs types
    _ f: @escaping (A) -> Void,
    _ g: @escaping (A) -> Void
    ) -> (inout A) -> Void {
    return { a in
        f(a)
        g(a) }
}

public func <> <A: AnyObject>(f: @escaping (A) -> Void, g: @escaping (A) -> Void) -> (A) -> Void {
    return { a in
        f(a)
        g(a)
    }
}

precedencegroup BackwardComposition {
    associativity: left
}

infix operator <<<: BackwardComposition

public func <<< <A,B,C>(
    _ g: @escaping (B) -> C,
    _ f: @escaping (A) -> B
    ) -> (A) -> C {
    return { g(f($0)) }
}

public func filter<A>(
    _ p: @escaping (A) -> Bool
    ) -> ([A]) -> [A] {
    return { $0.filter(p) }
}



public func zip<A, B>(_ a: A?, _ b: B?) -> (A, B)? {
    guard let a = a, let b = b else { return nil }
    return (a, b)
}
public func with<A, B>(_ a: A, _ f: (A) throws -> B) rethrows -> B {
    return try f(a)
}
public func over<S, T, A, B>(
    _ setter: (@escaping (A) -> B) -> (S) -> T,
    _ f: @escaping (A) -> B
    )
    -> (S) -> T {
        return setter(f)
}
public func set<S, T, A, B>(
    _ setter: (@escaping (A) -> B) -> (S) -> T,
    _ value: B
    )
    -> (S) -> T {
        return over(setter) { _ in value }
}
public func get<Root, Value>(_ keyPath: KeyPath<Root, Value>) -> (Root) -> Value {
    return { root in root[keyPath: keyPath] }
}
public func prop<Root, Value>(
    _ keyPath: WritableKeyPath<Root, Value>
    )
    -> (@escaping (Value) -> Value)
    -> (Root) -> Root {
        return { update in
            { root in
                var copy = root
                copy[keyPath: keyPath] = update(copy[keyPath: keyPath])
                return copy
            }
        }
}
public func over<Root, Value>(
    _ keyPath: WritableKeyPath<Root, Value>,
    _ update: @escaping (Value) -> Value
    )
    -> (Root) -> Root {
        return prop(keyPath)(update)
}
public func set<Root, Value>(
    _ keyPath: WritableKeyPath<Root, Value>,
    _ value: Value
    )
    -> (Root) -> Root {
        return over(keyPath) { _ in value }
}


public func mver<S, A>(
    _ setter: (@escaping (inout A) -> Void) -> (inout S) -> Void,
    _ f: @escaping (inout A) -> Void
    )
    -> (inout S) -> Void {
        
        return setter(f)
}

public func mver<S, A>(
    _ setter: (@escaping (inout A) -> Void) -> (S) -> Void,
    _ f: @escaping (inout A) -> Void
    )
    -> (S) -> Void
    where S: AnyObject {
        
        return setter(f)
}

public func mver<S, A>(
    _ setter: (@escaping (A) -> Void) -> (S) -> Void,
    _ f: @escaping (A) -> Void
    )
    -> (S) -> Void
    where S: AnyObject, A: AnyObject {
        
        return setter(f)
}

public func mut<S, A>(
    _ setter: (@escaping (inout A) -> Void) -> (inout S) -> Void,
    _ value: A
    )
    -> (inout S) -> Void {
        
        return mver(setter) { $0 = value }
}

public func mut<S, A>(
    _ setter: (@escaping (inout A) -> Void) -> (S) -> Void,
    _ value: A
    )
    -> (S) -> Void
    where S: AnyObject {
        
        return mver(setter) { $0 = value }
}
