public func jsonObject<Object: Encodable>(_ object: Object) -> [String: Any]?  {
    guard let data = try? JSONEncoder().encode(object) else { return nil }
    guard let object = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else { return nil }
    return object
}
