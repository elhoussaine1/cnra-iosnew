import Speech
let askPermission: () -> Effect<Bool> = {
    return Effect<Bool> { callback in
        SFSpeechRecognizer.requestAuthorization { (state) in
            switch state {
            case .authorized:
                callback(true)
            default:
                callback(false)
            }
        }
    }
}

public class SpeechRecognizer {
    
    let audioEngine: AVAudioEngine
    let speechRecognizer: SFSpeechRecognizer!
    var request: SFSpeechAudioBufferRecognitionRequest?
    var recognitionTask: SFSpeechRecognitionTask?
    
    public init(
        audioEngine: AVAudioEngine = AVAudioEngine(),
        locale: Locale
        ) {
        self.audioEngine = audioEngine
        self.speechRecognizer = SFSpeechRecognizer(locale: locale)
    }
}

func startRecording(_ speech: inout SpeechRecognizer) throws -> Effect<String> {
    
//    let audioSession = AVAudioSession.sharedInstance()
//    try audioSession.setCategory(.record, mode: .measurement, options: .duckOthers)
//    try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
    
    let audioSession = AVAudioSession.sharedInstance()
    do {
        try audioSession.setCategory(AVAudioSession.Category.record)
        try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
    } catch {
        print("audioSession properties weren't set because of an error.")
    }
    let inputNode = speech.audioEngine.inputNode
    
    speech.request = SFSpeechAudioBufferRecognitionRequest()
    guard let recognitionRequest = speech.request else { fatalError("Unable to create a SFSpeechAudioBufferRecognitionRequest object") }
    recognitionRequest.shouldReportPartialResults = true
    
    let recordingFormat = inputNode.outputFormat(forBus: 0)
    inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { [speech] (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
        speech.request?.append(buffer)
    }
    
    speech.audioEngine.prepare()
    try speech.audioEngine.start()
    
    return Effect<String> { [speech] callback in
        speech.recognitionTask = speech.speechRecognizer.recognitionTask(with: recognitionRequest) { [speech]  result, error in
            var isFinal = false
            
            if let result = result {
                isFinal = result.isFinal
                callback(result.bestTranscription.formattedString)
            }
            
            if error != nil || isFinal {
                speech.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                speech.request = nil
                speech.recognitionTask = nil
                
                print("task cancelled")
            }
        }
    }
    
}

let stopRecording: (inout SpeechRecognizer) -> Void = { speech in
    speech.audioEngine.stop()
    speech.request?.endAudio()
    speech.recognitionTask?.cancel()
}

let cancelRecording: (inout SpeechRecognizer) -> Void = { speech in
    speech.audioEngine.stop()
    speech.recognitionTask?.cancel()
}


