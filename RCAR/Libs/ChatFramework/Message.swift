import MessageKit

public struct Message: MessageType {
    
    public var sender: SenderType
    
    public var messageId: String
    
    public var sentDate: Date
    
    public var kind: MessageKind
    
    public var info: NetworkResponse?
            
    public init(
        sender: SenderType,
        messageId: String = Int.random(in: 0...1000) |> String.init,
        sentDate: Date = Date(),
        kind: MessageKind
    ) {
        self.sender = sender
        self.messageId =  messageId
        self.sentDate = sentDate
        self.kind = kind
    }
    
    public init(
        sender: SenderType,
        messageId: String = Int.random(in: 0...1000) |> String.init,
        sentDate: Date = Date(),
        info: NetworkResponse
    ) {
        self.sender = sender
        self.messageId =  messageId
        self.sentDate = info.date ?? sentDate
        self.kind = .text(info.text)
        self.info = info
    }
    
}

extension Message {
    var quickReplies: [NetworkResponse.QuickReply]? {
        return info?.quickReplies
    }
}

let asSender: (User) -> AppSender = {
    AppSender(
        senderId: $0.id,
        displayName: $0.name
    )    
}

public let currentSender = AppSender(
    senderId: Current.user.id,
    displayName: Current.user.name
)


public struct AppSender: SenderType {
    public var senderId: String
    public var displayName: String
}
