import MessageKit
import InputBarAccessoryView
import Speech
public class ChatViewController: MessagesViewController {
                    
    var viewModal: ViewModalType!
            
    lazy var microphoneButton: InputBarButtonItem = {
        return InputBarButtonItem()
            .configure {
                $0.setSize(CGSize(width: .cm_grid(8), height: .cm_grid(8)), animated: false)
                $0.isEnabled = true
                $0 |> roundedStyle(cornerRadius: .cm_grid(4))
                $0.image = .micro
                $0.onTouchUpInside { [weak self ] _ in self?.viewModal.inputs.recordButtonTapped() }
        }
    }()
                               
    public var actions: ((Action) -> Void)?
    public init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.becomeFirstResponder()
    }
           
    public override func viewDidLoad() {
        super.viewDidLoad()
                                
        viewModal = ViewModal(callback: { [weak self] (state) in
            DispatchQueue.main.async {
                self?.microphoneButton.tintColor = state.isRecording ? .systemRed: UIColor.init(hex: Current.config.userBubbleColor)
                self?.reload()
                if state.isRecording {
                    self?.messageInputBar.inputTextView.text = state.text
                }
                
                self?.microphoneButton.isEnabled = state.isSpeechAuthorized
            }
        }, actions: actions!,
        speech: Current.speech
        )
                
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messageInputBar.delegate = self
                        
        messagesCollectionView.register(QuickRepliesFooter.self,
                forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
                withReuseIdentifier: QuickRepliesFooter.reuseIdentifier)
                            
        if let layout = messagesCollectionView.collectionViewLayout
            as? MessagesCollectionViewFlowLayout {
            layout.textMessageSizeCalculator.outgoingAvatarSize = .zero
            layout.textMessageSizeCalculator.incomingAvatarSize = CGSize(width: 60, height: 60)
        }
                                       
        self.messageInputBar
            |> inputBarStyle
            <> setupRightStack(microphoneButton)
            <> sendButtonStyle

        configureHierarchy()
        
        viewModal.inputs.viewDidLoad()
    }
    
    func configureHierarchy() {
        
        self.messagesCollectionView.removeFromSuperview()
        self.view.backgroundColor = UIColor.init(hex: Current.config.headerColor)
        
        let button = UIButton()
        if #available(iOS 13.0, *) {
            let image = UIImage(systemName: "xmark", withConfiguration: UIImage.SymbolConfiguration(pointSize: 22, weight: .bold))
            button.setImage(image?.withTintColor(UIColor.darkGray), for: .normal)
            button.tintColor = .darkGray
        } else {
            button.setImage(.xmark, for: .normal)
            button.tintColor = .darkGray
        }
        
        button.addTarget(self, action: #selector(dismissTapped), for: .touchUpInside)
         
        let header = UIImageView(image: .logo)
        header.heightAnchor.constraint(equalToConstant: 67).isActive = true
        let headerView = UIStackView(arrangedSubviews: [button, header])
        headerView.isLayoutMarginsRelativeArrangement = true
        if #available(iOS 11.0, *) {
            headerView.directionalLayoutMargins = NSDirectionalEdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16)
        } else {
            
        }
        headerView.alignment = .center
        headerView.spacing = .cm_grid(4)
        headerView.distribution = .fill
        
        let stackview = UIStackView(arrangedSubviews: [headerView, messagesCollectionView])
        stackview.spacing = .cm_grid(2)
        stackview.axis = .vertical
        self.view.addSubview(stackview)
        stackview.constrainEdges(to: self.view)
    }
                 
    @objc private func dismissTapped() {
        self.dismiss(animated: true)
    }
    
    private func reload() {
        self.messagesCollectionView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if self.isLastSectionVisible(self.viewModal.state.messages) {
                self.messagesCollectionView.scrollToBottom(animated: true)
            }
        }
    }
            
}

extension ChatViewController: MessagesDataSource {

    public func currentSender() -> SenderType {
        return Current.user |> asSender
    }

    public func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return viewModal.state.messages.count
    }

    public func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return viewModal.state.messages[indexPath.section]
    }
            
}

extension ChatViewController: InputBarAccessoryViewDelegate {
    
    public func inputBar(
        _ inputBar: InputBarAccessoryView,
        didPressSendButtonWith text: String) {
        viewModal.inputs.sendButtonTapped()
        inputBar.inputTextView.text = String()
    }
        
    public func inputBar(
        _ inputBar: InputBarAccessoryView,
        textViewTextDidChangeTo text: String) {
        viewModal.inputs.textChanged(text)
    }
    
}

extension ChatViewController {

    private func insertMessage(_ message: Message, messages: [Message]) {
        messagesCollectionView.performBatchUpdates({
            messagesCollectionView.insertSections([messages.count - 1])
            if messages.count >= 2 {
                messagesCollectionView.reloadSections([messages.count - 2])
            }
        }, completion: { [weak self] _ in
            if self?.isLastSectionVisible(messages) == true {
                self?.messagesCollectionView.scrollToBottom(animated: true)
            }
        })
    }

    private func isLastSectionVisible(_ messages: [Message]) -> Bool {
        guard !messages.isEmpty else { return false }
        let lastIndexPath = IndexPath(item: 0, section: messages.count - 1)
        return messagesCollectionView.indexPathsForVisibleItems.contains(lastIndexPath)
    }

}

extension ChatViewController: MessagesLayoutDelegate {
                           
    public func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        if !isFromCurrentSender(message: message) {
            avatarView.image = .avatar
            avatarView.contentMode = .scaleAspectFill
            avatarView.backgroundColor = .clear
        }
    }

}

extension ChatViewController: MessagesDisplayDelegate {
    
    public func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message)
            ? UIColor(hex:Current.config.userBubbleColor)
            : UIColor(hex:Current.config.botBubbleColor)
    }
    
    public func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message)
        ? UIColor(hex:Current.config.userBubbleTextColor)
        : UIColor(hex:Current.config.botBubbleTextColor)
    }
                        
}

extension ChatViewController: QuickRepliesFooterDelegate {
    
    func quickReplyFooterDidSendReply(_ reply: String) {
        viewModal.inputs.sendQuickReply(reply)
    }
    
    public func footerViewSize(for section: Int, in messagesCollectionView: MessagesCollectionView) -> CGSize {
        let message = viewModal.state.messages[section]
        guard !isFromCurrentSender(message: message), message.info?.quickReplies != nil else { return .zero }
        return CGSize(width: view.bounds.width, height: .cm_grid(28))
    }

    public func messageFooterView(for indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageReusableView {
        let message = viewModal.state.messages[indexPath.section]
        guard !isFromCurrentSender(message: message) else { return MessageReusableView() }
        let footer = messagesCollectionView.dequeueReusableFooterView(QuickRepliesFooter.self, for: indexPath) as QuickRepliesFooter
        footer.delegate = self
        footer.replies = message.quickReplies ?? []
        return footer
    }
 
}

func roundedStyle(cornerRadius: CGFloat) -> (UIView) -> Void  {
    return {
        $0.layer.cornerRadius = cornerRadius
        $0.layer.masksToBounds = true
    }
}

extension CGFloat {
    public static func cm_grid(_ n: Int) -> CGFloat { 4 * CGFloat(n) }
}

let inputBarStyle: (inout InputBarAccessoryView) -> Void = {
    $0.inputTextView.placeholder = "Type a Message ..."
    $0.setRightStackViewWidthConstant(to: .cm_grid(23), animated: true)
    $0.rightStackView.alignment = .center
    $0.rightStackView.spacing = .cm_grid(5)
    $0.inputTextView.textColor = UIColor.init(white: 0.2, alpha: 1)
    $0.inputTextView.backgroundColor = UIColor.init(white: 0.95, alpha: 1)
    $0.inputTextView.layer.borderColor = UIColor.clear.cgColor
    $0.inputTextView.layer.borderWidth = 1.0
    $0.inputTextView.layer.cornerRadius = 16.0
    $0.inputTextView.layer.masksToBounds = true
    $0.inputTextView.textContainerInset = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: .cm_grid(23 / 2))
    $0.inputTextView.placeholderLabelInsets = UIEdgeInsets(top: 8, left: 20, bottom: 8, right: .cm_grid(23 / 2))
    $0.middleContentViewPadding.right = -42
    $0.separatorLine.isHidden = true
    $0.isTranslucent = true
}

let sendButtonStyle: (inout InputBarAccessoryView) -> Void = {
    $0.sendButton.setSize(CGSize(width: .cm_grid(6), height: .cm_grid(6)), animated: true)
    $0.sendButton.setTitle(nil, for: .normal)
    $0.sendButton.tintColor =  UIColor.init(hex: Current.config.userBubbleColor)
    $0.sendButton.setImage(UIImage.send, for: .normal)
}

func setupRightStack(_ microphoneButton: InputBarButtonItem) -> (inout InputBarAccessoryView) -> Void {
    return {
        $0.sendButton.removeFromSuperview()
        $0.setStackViewItems([$0.sendButton, microphoneButton], forStack: .right, animated: true)
    }
}

func formatter(_ dateFormat: String) -> (Date) -> String  {{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = dateFormat
    return dateFormatter.string(from: $0)
}}

let dateFormatter = formatter("hh:mm a")


