import UIKit

extension UIImage {
    static let logo = UIImage(named: "logo", in: .chat, compatibleWith: nil)
    static let micro = UIImage(named: "microphone", in: .chat, compatibleWith: nil)
    static let send = UIImage(named: "send", in: .chat, compatibleWith: nil)
    static let avatar = UIImage(named: "avatar", in: .chat, compatibleWith: nil)
    static let xmark = UIImage(named: "xmark", in: .chat, compatibleWith: nil)
}

extension UIColor {
    @available(iOS 11.0, *)
    static let cdgColor = UIColor(named: "green Color", in: .chat, compatibleWith: nil)
}

extension Bundle {
    static let chat = Bundle(identifier: "com.majid.ChatFramework")
}



